package com.food.quibilrestaurant.network_layer;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.food.quibilrestaurant.data_layer.CurrentRestaurant;
import com.food.quibilrestaurant.interfaces.CanPinBeMadeInterface;
import com.food.quibilrestaurant.interfaces.DeleteObjectInDatabaseInterface;
import com.food.quibilrestaurant.interfaces.FetchBraintreeCustomerIdInterface;
import com.food.quibilrestaurant.interfaces.FetchPastSessionsInterface;
import com.food.quibilrestaurant.interfaces.FetchUserInterface;
import com.food.quibilrestaurant.interfaces.FindBraintreeCustomerInterface;
import com.food.quibilrestaurant.interfaces.OnClientChargedInterface;
import com.food.quibilrestaurant.interfaces.OnPaymentTokenCreatedInterface;
import com.food.quibilrestaurant.interfaces.UpdateLockScreenInterface;
import com.food.quibilrestaurant.interfaces.UpdateUserInterface;
import com.food.quibilrestaurant.models.BraintreeCustomer;
import com.food.quibilrestaurant.models.LockscreenCredential;
import com.food.quibilrestaurant.models.Session;
import com.food.quibilrestaurant.models.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by sultankhan on 8/1/17.
 */

public class UserService {
    private String TAG = "UserService";

    private DatabaseReference mDatabase;
    private Context context;
    private SharedPreferences prefs;
    private static UserService instance;

    public static UserService getInstance(Context context) {

        if (instance == null) {
            instance = new UserService(context);
        }
        return instance;
    }

    private UserService(Context context) {
        this.mDatabase = FirebaseDatabase.getInstance().getReference();
        this.context = context;
        this.prefs = context.getSharedPreferences("current_user", MODE_PRIVATE);
    }

    public void getUserFromDatabase(final String uid, final FetchUserInterface fetchUserInterface) {
        DocumentReference docRef = FirebaseFirestore.getInstance().collection("users").document(uid);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d(TAG, "DocumentSnapshot data: " + document.getData());

                        User currentUser = document.toObject(User.class);

                        fetchUserInterface.onDataFetched(currentUser);
                    } else {
                        Log.d(TAG, "No such document");
                        fetchUserInterface.onUserDataNotFound();
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                    fetchUserInterface.onUserDataNotFound();
                }
            }
        });

    }

    public void updateUser(final User user,final UpdateUserInterface updateUserInterface){
        int socketTimeout = 30000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        Log.i(TAG, "API Call: updateProfile");

        String postURL = "https://us-central1-quibil-b5335.cloudfunctions.net/updateUser";

        StringRequest postRequest = new StringRequest(Request.Method.POST, postURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.i(TAG, "JSON response: " +  response);

                            JSONObject jsonResponse = new JSONObject(response);

                            // If SUCCESS
                            if (jsonResponse.getInt("status") == 200) {
                                updateUserInterface.onUpdated();
                            } else {
                                Log.e("Error JSON", jsonResponse.getString("message"));
                                updateUserInterface.onError();
                                return;
                            }

                        } catch (Exception e) {
                            Log.e(TAG, "onResponse: exception");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "VolleyError:onErrorResponse");
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<>();
                params.put("id", user.id);
                params.put("name", user.name);
                params.put("ownsRestaurant", user.ownsRestaurant);
                params.put("profileTypeTitle", user.profileTypeTitle);
                params.put("profilePictureURL", user.profilePictureURL);
                params.put("profileType", user.profileType);

                return params;
            }
        };

        postRequest.setRetryPolicy(policy);
        Volley.newRequestQueue(context).add(postRequest);
    }

    public void updateLockscreenCredentials(LockscreenCredential credential, String restaurantId, String pin, final UpdateLockScreenInterface updateLockScreenInterface){
        // Save pin to database
        FirebaseDatabase.getInstance().getReference()
                .child("restaurant_pin_user_relation")
                .child(restaurantId)
                .child(pin).setValue(credential).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                updateLockScreenInterface.onUpdated();
            }
        });
    }

    public void deleteLockscreenCredentials(String restaurantId, String pin, final DeleteObjectInDatabaseInterface deleteObjectInDatabaseInterface){
        // Save pin to database
        FirebaseDatabase.getInstance().getReference()
                .child("restaurant_pin_user_relation")
                .child(restaurantId)
                .child(pin).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                deleteObjectInDatabaseInterface.onDeleted();
            }
        });
    }

    public void checkIfPinHasNotBeenChosenByPreviousUsers(String restaurantId, String pin, final CanPinBeMadeInterface canPinBeMadeInterface){
        FirebaseDatabase.getInstance().getReference()
                .child("restaurant_pin_user_relation")
                .child(restaurantId)
                .child(pin).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                LockscreenCredential credential = dataSnapshot.getValue(LockscreenCredential.class);

                // If no id found, meaning pin is available
                if(credential == null){
                    canPinBeMadeInterface.pinAvailable();
                }
                // Else if id found
                else {
                    canPinBeMadeInterface.pinTaken();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());


            }
        });
    }

    public void getOlderSessions(final FetchPastSessionsInterface fetchPastSessionsInterface) {
        FirebaseFirestore.getInstance()
                .collection("restaurantSessionHistory").document(CurrentRestaurant.getInstance(context).getCache().id)
                .collection("closedOutSessions").get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                if(queryDocumentSnapshots.getDocuments().size() == 0){
                    fetchPastSessionsInterface.onClearList();
                } else {
                    for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                        Session session = documentSnapshot.toObject(Session.class);
                        fetchPastSessionsInterface.onDataFetched(session);
                    }
                }
            }
        });
    }




    public void fetchBraintreeCustomerId(final String clientId, final FetchBraintreeCustomerIdInterface fetchBraintreeCustomerIdInterface) {
        FirebaseFirestore.getInstance()
                .collection("braintree").document(clientId)
                .get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                BraintreeCustomer braintreeCustomer = documentSnapshot.toObject(BraintreeCustomer.class);
                fetchBraintreeCustomerIdInterface.onFetched(braintreeCustomer);
            }
        });
    }

    public void testBTFindCustomer(final String customerId,final FindBraintreeCustomerInterface findBraintreeCustomerInterface){
        int socketTimeout = 30000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        Log.i(TAG, "API Call: testBTFindCustomer");

        String postURL = "https://us-central1-quibil-b5335.cloudfunctions.net/testBTFindCustomer";

        StringRequest postRequest = new StringRequest(Request.Method.POST, postURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.i(TAG, "JSON response: " +  response);

                            JSONObject jsonResponse = new JSONObject(response);

                            // If SUCCESScheckoutUser
                            if (jsonResponse.getInt("status") == 200) {

                                JSONArray paymentMethods = jsonResponse.getJSONObject("customer")
                                        .getJSONArray("paymentMethods");
                                String paymentToken = paymentMethods.getJSONObject(0).getString("token");
                                findBraintreeCustomerInterface.onFetched(paymentToken);
                            } else {
                                Log.e("Error JSON", jsonResponse.getString("message"));
                                findBraintreeCustomerInterface.onError();
                                return;
                            }

                        } catch (Exception e) {
                            Log.e(TAG, "onResponse: exception");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "VolleyError:onErrorResponse");
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<>();
                params.put("customerId", customerId);
                return params;
            }
        };

        postRequest.setRetryPolicy(policy);
        Volley.newRequestQueue(context).add(postRequest);
    }

    public void chargeClient(final String nonceFromTheClient, final String paymentAmount, final OnClientChargedInterface onClientChargedInterface){
        int socketTimeout = 30000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        Log.i(TAG, "API Call: chargeClient");

        String postURL = "https://us-central1-quibil-b5335.cloudfunctions.net/chargeClient";

        StringRequest postRequest = new StringRequest(Request.Method.POST, postURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.i(TAG, "JSON response: " +  response);

                            JSONObject jsonResponse = new JSONObject(response);

                            // If SUCCESS
                            if (jsonResponse.getInt("status") == 200) {
                                onClientChargedInterface.onSuccess();
                            } else {
                                Log.e("Error JSON", jsonResponse.getString("message"));
                                onClientChargedInterface.onError();
                                return;
                            }

                        } catch (Exception e) {
                            Log.e(TAG, "onResponse: exception");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "VolleyError:onErrorResponse");
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<>();
                params.put("nonceFromTheClient", nonceFromTheClient);
                params.put("amount", paymentAmount);
                return params;
            }
        };

        postRequest.setRetryPolicy(policy);
        Volley.newRequestQueue(context).add(postRequest);
    }

    public void getPaymentToken(final String paymentToken, final OnPaymentTokenCreatedInterface onPaymentTokenCreatedInterface){
        int socketTimeout = 30000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        Log.i(TAG, "API Call: getPaymentToken");

        String postURL = "https://us-central1-quibil-b5335.cloudfunctions.net/getPaymentToken";

        StringRequest postRequest = new StringRequest(Request.Method.POST, postURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.i(TAG, "JSON response: " +  response);

                            JSONObject jsonResponse = new JSONObject(response);

                            // If SUCCESS
                            if (jsonResponse.getInt("status") == 200) {
                                onPaymentTokenCreatedInterface.onNoneReceived(jsonResponse.getString("paymentMethodNonce"));
                            } else {
                                Log.e("Error JSON", jsonResponse.getString("message"));
                                onPaymentTokenCreatedInterface.onError();
                                return;
                            }

                        } catch (Exception e) {
                            Log.e(TAG, "onResponse: exception");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "VolleyError:onErrorResponse");
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<>();
                params.put("paymentToken", paymentToken);
                return params;
            }
        };

        postRequest.setRetryPolicy(policy);
        Volley.newRequestQueue(context).add(postRequest);
    }


}
