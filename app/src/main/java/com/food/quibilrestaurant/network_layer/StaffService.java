package com.food.quibilrestaurant.network_layer;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.food.quibilrestaurant.interfaces.CreateMenuCategoryInterface;
import com.food.quibilrestaurant.interfaces.CreateMenuItemInterface;
import com.food.quibilrestaurant.interfaces.CreateStaffWorkerInterface;
import com.food.quibilrestaurant.interfaces.DeleteObjectInDatabaseInterface;
import com.food.quibilrestaurant.interfaces.DeleteStaffWorkerInterface;
import com.food.quibilrestaurant.interfaces.FetchRestaurantMenuCategoriesInterface;
import com.food.quibilrestaurant.interfaces.FetchRestaurantStaffInterface;
import com.food.quibilrestaurant.interfaces.UpdateLockScreenInterface;
import com.food.quibilrestaurant.interfaces.UpdateMenuItemInterface;
import com.food.quibilrestaurant.interfaces.UpdateStaffWorkerInterface;
import com.food.quibilrestaurant.models.LockscreenCredential;
import com.food.quibilrestaurant.models.MenuItem;
import com.food.quibilrestaurant.models.StaffWorker;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by sultankhan on 8/1/17.
 */

public class StaffService {

    String TAG = "Network Layer: StaffService";

    private DatabaseReference mDatabase;
    private Context context;
    private SharedPreferences prefs;
    private static StaffService instance;

    public static StaffService getInstance(Context context) {

        if (instance == null) {
            instance = new StaffService(context);
        }
        return instance;
    }

    private StaffService(Context context) {
        this.mDatabase = FirebaseDatabase.getInstance().getReference();
        this.context = context;
        this.prefs = context.getSharedPreferences("current_user", MODE_PRIVATE);
    }


    public void getRestaurantStaffWorkers(final String restaurantId, final FetchRestaurantStaffInterface fetchRestaurantStaffInterface) {
        FirebaseFirestore.getInstance()
                .collection("restaurants").document(restaurantId)
                .collection("staff").get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                    StaffWorker staffWorker = documentSnapshot.toObject(StaffWorker.class);
                    fetchRestaurantStaffInterface.onDataFetched(staffWorker);
                }
            }
        });
    }



    public void addNewStaffWorker(final String restaurantId, final StaffWorker staffWorker, final CreateStaffWorkerInterface createStaffWorkerInterface) {
        FirebaseFirestore.getInstance()
                .collection("restaurants").document(restaurantId)
                .collection("staff").add(staffWorker)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        staffWorker.id = documentReference.getId();
                        staffWorker.worksAtRestaurantId = restaurantId;
                        documentReference.set(staffWorker);


                        // Update lockscreen credentials
                        LockscreenCredential credential = new LockscreenCredential();
                        credential.id = staffWorker.id;
                        credential.profileType = staffWorker.profileType;
                        credential.name = staffWorker.name;
                        UserService.getInstance(context).updateLockscreenCredentials(credential, restaurantId, staffWorker.pin, new UpdateLockScreenInterface() {
                            @Override
                            public void onUpdated() {
                                createStaffWorkerInterface.onCreated();
                            }

                            @Override
                            public void onError() {

                            }
                        });


                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                createStaffWorkerInterface.onError();
            }
        });

    }

    public void deleteStaffWorker(final String restaurantId, final StaffWorker staffWorker, final DeleteObjectInDatabaseInterface deleteStaffWorkerInterface) {
        FirebaseFirestore.getInstance()
                .collection("restaurants").document(restaurantId)
                .collection("staff").document(staffWorker.id).delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                        // Delete credentials
                        UserService.getInstance(context).deleteLockscreenCredentials(restaurantId, staffWorker.pin, new DeleteObjectInDatabaseInterface() {

                            @Override
                            public void onDeleted() {
                                deleteStaffWorkerInterface.onDeleted();
                            }

                            @Override
                            public void onError() {

                            }
                        });


                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                deleteStaffWorkerInterface.onError();
            }
        });

    }


    public void updateStaffWorker(final String restaurantId, final StaffWorker staffWorker, final UpdateStaffWorkerInterface updateStaffWorkerInterface) {
        FirebaseFirestore.getInstance()
                .collection("restaurants").document(restaurantId)
                .collection("staff").document(staffWorker.id).set(staffWorker)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                        // Update lockscreen credentials
                        LockscreenCredential credential = new LockscreenCredential();
                        credential.id = staffWorker.id;
                        credential.profileType = staffWorker.profileType;
                        credential.name = staffWorker.name;
                        UserService.getInstance(context).updateLockscreenCredentials(credential, restaurantId, staffWorker.pin, new UpdateLockScreenInterface() {
                            @Override
                            public void onUpdated() {
                                updateStaffWorkerInterface.onUpdated();
                            }

                            @Override
                            public void onError() {

                            }
                        });


                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                updateStaffWorkerInterface.onError();
            }
        });


    }

}
