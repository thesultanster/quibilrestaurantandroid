package com.food.quibilrestaurant.network_layer;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.food.quibilrestaurant.interfaces.CreateMenuCategoryInterface;
import com.food.quibilrestaurant.interfaces.CreateRestaurantInterface;
import com.food.quibilrestaurant.interfaces.FetchCheckedInUsersInterface;
import com.food.quibilrestaurant.interfaces.FetchRestaurantInterface;
import com.food.quibilrestaurant.interfaces.FetchUserInterface;
import com.food.quibilrestaurant.interfaces.FetchUsersOrderDeliverednterface;
import com.food.quibilrestaurant.interfaces.FetchUsersWaitingForOrderInterface;
import com.food.quibilrestaurant.interfaces.UpdateRestaurantInterface;
import com.food.quibilrestaurant.models.CheckedInUser;
import com.food.quibilrestaurant.models.MenuItem;
import com.food.quibilrestaurant.models.OrderDelivered;
import com.food.quibilrestaurant.models.OrderedItem;
import com.food.quibilrestaurant.models.Restaurant;
import com.food.quibilrestaurant.models.Session;
import com.food.quibilrestaurant.models.User;
import com.food.quibilrestaurant.models.WaitingForOrder;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.functions.FirebaseFunctions;
import com.google.firebase.functions.HttpsCallableResult;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by sultankhan on 8/1/17.
 */

public class RestaurantService {

    String TAG = "RestaurantService";

    private DatabaseReference mDatabase;
    private Context context;
    private SharedPreferences prefs;
    private static RestaurantService instance;
    private ValueEventListener checkedInUsersListener;
    private ValueEventListener usersWaitingForOrderListener;
    private ValueEventListener userOrderDeliveredListner;

    public static RestaurantService getInstance(Context context) {

        if (instance == null) {
            instance = new RestaurantService(context);
        }
        return instance;
    }

    private RestaurantService(Context context) {
        this.mDatabase = FirebaseDatabase.getInstance().getReference();
        this.context = context;
        this.prefs = context.getSharedPreferences("current_restaurant", MODE_PRIVATE);
    }

    public void getRestaurantFromDatabase(final String id, final FetchRestaurantInterface fetchRestaurantInterface) {

        int socketTimeout = 30000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        Log.i(TAG, "API Call: getRestaurant");
        String postURL = "https://us-central1-quibil-b5335.cloudfunctions.net/getRestaurant";
        StringRequest postRequest = new StringRequest(Request.Method.POST, postURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.i(TAG, "JSON response: " + response);

                            JSONObject jsonResponse = new JSONObject(response);

                            // If SUCCESS
                            if (jsonResponse.getInt("status") == 200) {

                                // Convert json to restaurant object
                                Gson gson = new Gson();
                                String json = jsonResponse.getString("restaurant");
                                Restaurant restaurant = gson.fromJson(json, Restaurant.class);

                                fetchRestaurantInterface.onDataFetched(restaurant);
                            } else {
                                Log.e("Error JSON", jsonResponse.getString("message"));
                                fetchRestaurantInterface.onError();
                                return;
                            }


                        } catch (Exception e) {
                            Log.e(TAG, "onResponse: exception", e);
                            fetchRestaurantInterface.onError();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "VolleyError:onErrorResponse");
                        fetchRestaurantInterface.onError();
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<>();
                params.put("restaurantId", id);
                return params;
            }
        };

        postRequest.setRetryPolicy(policy);
        Volley.newRequestQueue(context).add(postRequest);
    }

    public void updateRetaurantToDatabase(final Restaurant restaurant, final UpdateRestaurantInterface updateRestaurantInterface) {
        int socketTimeout = 30000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        Log.i(TAG, "API Call: updateRestaurant");
        String postURL = "https://us-central1-quibil-b5335.cloudfunctions.net/updateRestaurant";
        StringRequest postRequest = new StringRequest(Request.Method.POST, postURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.i(TAG, "JSON response: " + response);

                            JSONObject jsonResponse = new JSONObject(response);

                            // If SUCCESS
                            if (jsonResponse.getInt("status") == 200) {
                                updateRestaurantInterface.onUpdated();
                            } else {
                                Log.e("Error JSON", jsonResponse.getString("message"));
                                updateRestaurantInterface.onError();
                                return;
                            }


                        } catch (Exception e) {
                            Log.e(TAG, "onResponse: exception");
                            updateRestaurantInterface.onError();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "VolleyError:onErrorResponse");
                        updateRestaurantInterface.onError();
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<>();
                params.put("id", restaurant.id);
                params.put("name", restaurant.name);
                params.put("address", restaurant.address);
                params.put("phoneNumber", restaurant.phoneNumber);
                params.put("profilePictureUrl", restaurant.profilePictureUrl);
                return params;
            }
        };

        postRequest.setRetryPolicy(policy);
        Volley.newRequestQueue(context).add(postRequest);
    }

    public void createRetaurantFromDatabase(final CreateRestaurantInterface createRestaurantInterface) {
        int socketTimeout = 30000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        Log.i(TAG, "API Call: createRestaurant");
        String postURL = "https://us-central1-quibil-b5335.cloudfunctions.net/createRestaurant";
        StringRequest postRequest = new StringRequest(Request.Method.POST, postURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.i(TAG, "JSON response: " + response);

                            JSONObject jsonResponse = new JSONObject(response);

                            // If SUCCESS
                            if (jsonResponse.getInt("status") == 200) {
                                createRestaurantInterface.onCreated(jsonResponse.getString("restaurantId"));
                            } else {
                                Log.e("Error JSON", jsonResponse.getString("message"));
                                createRestaurantInterface.onError();
                                return;
                            }


                        } catch (Exception e) {
                            Log.e(TAG, "onResponse: exception");
                            createRestaurantInterface.onError();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "VolleyError:onErrorResponse");
                        createRestaurantInterface.onError();
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<>();
                return params;
            }
        };

        postRequest.setRetryPolicy(policy);
        Volley.newRequestQueue(context).add(postRequest);
    }


    public void getCheckedInUsers(String restaurantId, final FetchCheckedInUsersInterface fetchCheckedInUsersInterface) {


        checkedInUsersListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // If results are null meaning there is no such restaurant in the tree, that means no one has checked in
                if (dataSnapshot.getValue() == null) {
                    fetchCheckedInUsersInterface.onNoOneCheckedIn();
                } else {
                    fetchCheckedInUsersInterface.clearCurrentUsers();
                    for (DataSnapshot stateSnap : dataSnapshot.getChildren()) {
                        // Find pendingFoodOrder
                        if (stateSnap.getKey().equals("checkedIn")) {
                            for (DataSnapshot sessionSnap : stateSnap.getChildren()) {
                                fetchCheckedInUsersInterface.onDataFetched(sessionSnap.getValue(Session.class));
                            }
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "getCheckedInUsers:onCancelled", databaseError.toException());
                fetchCheckedInUsersInterface.onError();

            }
        };

        FirebaseDatabase.getInstance().getReference()
                .child("restaurant_sessions")
                .child(restaurantId)
                .addValueEventListener(checkedInUsersListener);
    }


    public void getUsersWaitingForOrderUsers(String restaurantId, final FetchUsersWaitingForOrderInterface fetchUsersWaitingForOrderInterface) {

        usersWaitingForOrderListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // If results are null meaning there is no such restaurant in the tree, that means no one has checked in
                if (dataSnapshot.getValue() == null) {
                    fetchUsersWaitingForOrderInterface.noPendingOrders();
                } else {

                    fetchUsersWaitingForOrderInterface.clearCurrentOrders();
                    for (DataSnapshot stateSnap : dataSnapshot.getChildren()) {
                        // Find pendingFoodOrder
                        if (stateSnap.getKey().equals("pendingFoodOrder")) {
                            for (DataSnapshot sessionSnap : stateSnap.getChildren()) {
                                fetchUsersWaitingForOrderInterface.onDataFetched(sessionSnap.getValue(Session.class));
                            }
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "getUsersWaitingForOrderUsers:onCancelled", databaseError.toException());
                fetchUsersWaitingForOrderInterface.onError();

            }
        };

        FirebaseDatabase.getInstance().getReference()
                .child("restaurant_sessions")
                .child(restaurantId)
                .addValueEventListener(usersWaitingForOrderListener);

    }

    public void getUsersOrderedDelivered(String restaurantId, final FetchUsersOrderDeliverednterface orderDeliverednterface) {

        userOrderDeliveredListner = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                // If results are null meaning there is no such restaurant in the tree, that means no one has checked in
                if (dataSnapshot.getValue() == null) {
                    orderDeliverednterface.noPendingOrders();
                } else {
                    orderDeliverednterface.clearDeliveredOrders();

                    for (DataSnapshot stateSnap : dataSnapshot.getChildren()) {
                        // Find pendingFoodOrder
                        if (stateSnap.getKey().equals("orderDelivered")) {
                            for (DataSnapshot sessionSnap : stateSnap.getChildren()) {
                                orderDeliverednterface.onDataFetched(sessionSnap.getValue(Session.class));
                            }
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "getUsersOrderedDelivered:onCancelled", databaseError.toException());
                orderDeliverednterface.onError();

            }
        };

        FirebaseDatabase.getInstance()
                .getReference()
                .child("restaurant_sessions")
                .child(restaurantId)
                .addValueEventListener(userOrderDeliveredListner);

    }

    public void userOrderDelivered(final WaitingForOrder waitingForOrder) {
        int socketTimeout = 30000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        Log.i(TAG, "API Call: userOrderDelivered");
        String postURL = "https://us-central1-quibil-b5335.cloudfunctions.net/userOrderDelivered";
        StringRequest postRequest = new StringRequest(Request.Method.POST, postURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.i(TAG, "JSON response: " + response);

                            JSONObject jsonResponse = new JSONObject(response);

                            // If SUCCESS
                            if (jsonResponse.getInt("status") == 200) {
                                //createRestaurantInterface.onCreated(jsonResponse.getString("restaurantId"));
                            } else {
                                Log.e("Error JSON", jsonResponse.getString("message"));
                                //createRestaurantInterface.onError();
                                return;
                            }


                        } catch (Exception e) {
                            Log.e(TAG, "onResponse: exception");
                            //createRestaurantInterface.onError();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "VolleyError:onErrorResponse");
                        //createRestaurantInterface.onError();
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<>();
                params.put("sessionId", waitingForOrder.id);
                return params;
            }
        };

        postRequest.setRetryPolicy(policy);
        Volley.newRequestQueue(context).add(postRequest);
    }

    public Task<String> checkoutUser(Session session) {
        // Create the arguments to the callable function.
        ObjectMapper m = new ObjectMapper();
        Map<String, Object> data = m.convertValue(session, Map.class);

        return FirebaseFunctions.getInstance()
                .getHttpsCallable("businessCloseOut")
                .call(data)
                .continueWith(new Continuation<HttpsCallableResult, String>() {
                    @Override
                    public String then(@NonNull Task<HttpsCallableResult> task) throws Exception {
                        // This continuation runs on either success or failure, but if the task
                        // has failed then getResult() will throw an Exception which will be
                        // propagated down.
                        String result = (String) task.getResult().getData();
                        return result;
                    }
                });
    }

    public void setMenuItemAsPrinted(final OrderedItem orderedItem, final String sessionId, final OrderPrintedInterface orderPrintedInterface) {
        Log.d(TAG, "setMenuItemAsPrinted()");

        // Reflect changes or order_sessions tree
        FirebaseDatabase.getInstance().getReference()
                .child("order_sessions")
                .child(sessionId)
                .child("orders")
                .child(orderedItem.index + "")
                .child("isPrinted")
                .setValue(true)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "setValue orderDelivered isPrinted");

                        FirebaseDatabase.getInstance().getReference()
                                .child("order_sessions")
                                .child(sessionId)
                                .child("orders")
                                .child(orderedItem.index + "")
                                .child("state")
                                .setValue("PRINTED")
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Log.d(TAG, "setValue orderDelivered state");
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.e(TAG, "error saving state for orderDelivered", e);
                                    }
                                });
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG, "error saving isPrinted for orderDelivered ", e);
                        orderPrintedInterface.onError();
                    }
                });

        // Reflect changes under the "orderDelivered" tree
        FirebaseDatabase.getInstance().getReference()
                .child("restaurant_sessions")
                .child(orderedItem.restaurantId)
                .child("orderDelivered")
                .child(sessionId)
                .child("orders")
                .child(orderedItem.index + "")
                .child("isPrinted")
                .setValue(true)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "setValue orderDelivered isPrinted");

                        FirebaseDatabase.getInstance().getReference()
                                .child("restaurant_sessions")
                                .child(orderedItem.restaurantId)
                                .child("orderDelivered")
                                .child(sessionId)
                                .child("orders")
                                .child(orderedItem.index + "")
                                .child("state")
                                .setValue("PRINTED")
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Log.d(TAG, "setValue orderDelivered state");
                                        }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.e(TAG, "error saving state for orderDelivered", e);
                                    }
                                });
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG, "error saving isPrinted for orderDelivered ", e);
                        orderPrintedInterface.onError();
                    }
                });

        // Reflect same changes under the "pendingFoodOrder" tree
        FirebaseDatabase.getInstance().getReference()
                .child("restaurant_sessions")
                .child(orderedItem.restaurantId)
                .child("pendingFoodOrder")
                .child(sessionId)
                .child("orders")
                .child(orderedItem.index + "")
                .child("isPrinted")
                .setValue(true)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "setValue pendingFoodOrder isPrinted");

                        FirebaseDatabase.getInstance().getReference()
                                .child("restaurant_sessions")
                                .child(orderedItem.restaurantId)
                                .child("pendingFoodOrder")
                                .child(sessionId)
                                .child("orders")
                                .child(orderedItem.index + "")
                                .child("state")
                                .setValue("PRINTED")
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Log.d(TAG, "setValue pendingFoodOrder state");

                                        orderPrintedInterface.onStatuSuccesfullyChanged();
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.e(TAG, "error saving state for pendingFoodOrder", e);
                                        orderPrintedInterface.onError();
                                    }
                                });
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG, "error saving isPrinted for pendingFoodOrder", e);
                        orderPrintedInterface.onError();
                    }
                });


    }

    public void removeListeners() {
        FirebaseDatabase.getInstance().getReference().removeEventListener(checkedInUsersListener);
        FirebaseDatabase.getInstance().getReference().removeEventListener(usersWaitingForOrderListener);
        FirebaseDatabase.getInstance().getReference().removeEventListener(userOrderDeliveredListner);
    }


}
