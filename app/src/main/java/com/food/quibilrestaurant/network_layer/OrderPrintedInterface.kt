package com.food.quibilrestaurant.network_layer

interface OrderPrintedInterface {
    fun onStatuSuccesfullyChanged()
    fun onError()
}