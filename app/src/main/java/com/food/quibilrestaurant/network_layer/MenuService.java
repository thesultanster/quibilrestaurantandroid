package com.food.quibilrestaurant.network_layer;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.food.quibilrestaurant.dialogs.UpdateMenuCategoryDialog;
import com.food.quibilrestaurant.interfaces.CreateAddonItemInterface;
import com.food.quibilrestaurant.interfaces.CreateMenuCategoryInterface;
import com.food.quibilrestaurant.interfaces.CreateMenuItemInterface;
import com.food.quibilrestaurant.interfaces.DeleteObjectInDatabaseInterface;
import com.food.quibilrestaurant.interfaces.FetchRestaurantItemAddOnsInterface;
import com.food.quibilrestaurant.interfaces.FetchRestaurantMenuCategoriesInterface;
import com.food.quibilrestaurant.interfaces.FetchRestaurantMenuItemsInterface;
import com.food.quibilrestaurant.interfaces.FetchRestaurantStaffInterface;
import com.food.quibilrestaurant.interfaces.UpdateAddonItemInterface;
import com.food.quibilrestaurant.interfaces.UpdateMenuCategoryInterface;
import com.food.quibilrestaurant.interfaces.UpdateMenuItemInterface;
import com.food.quibilrestaurant.models.AddOnItem;
import com.food.quibilrestaurant.models.MenuCategory;
import com.food.quibilrestaurant.models.MenuItem;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by sultankhan on 8/1/17.
 */

public class MenuService {

    String TAG = "Network Layer: MenuService";

    private DatabaseReference mDatabase;
    private Context context;
    private SharedPreferences prefs;
    private static MenuService instance;

    public static MenuService getInstance(Context context) {

        if (instance == null) {
            instance = new MenuService(context);
        }
        return instance;
    }

    private MenuService(Context context) {
        this.mDatabase = FirebaseDatabase.getInstance().getReference();
        this.context = context;
        this.prefs = context.getSharedPreferences("current_user", MODE_PRIVATE);
    }


    public void getMenuCategories(final String restaurantId, final FetchRestaurantMenuCategoriesInterface fetchRestaurantMenuInterface) {
        FirebaseFirestore.getInstance()
                .collection("restaurants").document(restaurantId)
                .collection("menu").get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                    MenuCategory menuCategory = documentSnapshot.toObject(MenuCategory.class);
                    fetchRestaurantMenuInterface.onDataFetched(menuCategory);
                }
            }
        });
    }

    public void getMenuItems(final String restaurantId, final String categoryId, final FetchRestaurantMenuItemsInterface fetchRestaurantMenuItemsInterface) {
        FirebaseFirestore.getInstance()
                .collection("restaurants").document(restaurantId)
                .collection("menu").document(categoryId)
                .collection("items").get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                    MenuItem menuItem = documentSnapshot.toObject(MenuItem.class);
                    fetchRestaurantMenuItemsInterface.onDataFetched(menuItem);
                }
            }
        });
    }

    public void getItemAddOns(final String restaurantId, final String categoryId, final String itemId, final FetchRestaurantItemAddOnsInterface fetchRestaurantItemAddOnsInterface) {
        FirebaseFirestore.getInstance()
                .collection("restaurants").document(restaurantId)
                .collection("menu").document(categoryId)
                .collection("items").document(itemId)
                .collection("addOns").get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                    AddOnItem addOnItem = documentSnapshot.toObject(AddOnItem.class);
                    fetchRestaurantItemAddOnsInterface.onDataFetched(addOnItem);
                }
            }
        });
    }

    public void addCategoryToMenu(final String restaurantId, final String name, final CreateMenuCategoryInterface createMenuCategoryInterface) {
        int socketTimeout = 30000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        Log.i(TAG, "API Call: addCategoryToMenu");
        String postURL = "https://us-central1-quibil-b5335.cloudfunctions.net/addCategoryToMenu";
        StringRequest postRequest = new StringRequest(Request.Method.POST, postURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.i(TAG, "JSON response: " + response);

                            JSONObject jsonResponse = new JSONObject(response);

                            // If SUCCESS
                            if (jsonResponse.getInt("status") == 200) {
                                createMenuCategoryInterface.onCreated();
                            } else {
                                Log.e("Error JSON", jsonResponse.getString("message"));
                                createMenuCategoryInterface.onError();
                                return;
                            }


                        } catch (Exception e) {
                            Log.e(TAG, "onResponse: exception");
                            createMenuCategoryInterface.onError();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "VolleyError:onErrorResponse");
                        createMenuCategoryInterface.onError();
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<>();
                params.put("restaurantId", restaurantId);
                params.put("name", name);
                return params;
            }
        };

        postRequest.setRetryPolicy(policy);
        Volley.newRequestQueue(context).add(postRequest);
    }

    public void addItemToMenu(final MenuItem menuItem, final CreateMenuItemInterface createMenuItemInterface) {
        FirebaseFirestore.getInstance()
                .collection("restaurants").document(menuItem.restaurantId)
                .collection("menu").document(menuItem.categoryId)
                .collection("items").add(menuItem)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        menuItem.id = documentReference.getId();
                        documentReference.set(menuItem);
                        createMenuItemInterface.onCreated();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                createMenuItemInterface.onError();
            }
        });


    }

    public void addAddonItemToMenu(String restaurantId, String categoryId, final AddOnItem addOnItem, final CreateAddonItemInterface createAddonItemInterface) {
        FirebaseFirestore.getInstance()
                .collection("restaurants").document(restaurantId)
                .collection("menu").document(categoryId)
                .collection("items").document(addOnItem.menuItemId)
                .collection("addOns").add(addOnItem)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        addOnItem.id = documentReference.getId();
                        documentReference.set(addOnItem);
                        createAddonItemInterface.onCreated();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(TAG, e.getMessage());
                createAddonItemInterface.onError();
            }
        });


    }

    public void updateAddonItemToMenu(String restaurantId, String categoryId, final AddOnItem addOnItem, final UpdateAddonItemInterface updateAddonItemInterface) {
        FirebaseFirestore.getInstance()
                .collection("restaurants").document(restaurantId)
                .collection("menu").document(categoryId)
                .collection("items").document(addOnItem.menuItemId)
                .collection("addOns").document(addOnItem.id).set(addOnItem)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        updateAddonItemInterface.onUpdated();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(TAG, e.getMessage());
                updateAddonItemInterface.onError();
            }
        });


    }

    public void updateMenuCategory(final MenuCategory menuCategory, final UpdateMenuCategoryInterface updateMenuCategoryInterface) {
        FirebaseFirestore.getInstance()
                .collection("restaurants").document(menuCategory.restaurantId)
                .collection("menu").document(menuCategory.id)
                .set(menuCategory)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        updateMenuCategoryInterface.onUpdated();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                updateMenuCategoryInterface.onError();
            }
        });


    }

    public void updateMenuItem(final MenuItem menuItem, final UpdateMenuItemInterface updateMenuItemInterface) {
        FirebaseFirestore.getInstance()
                .collection("restaurants").document(menuItem.restaurantId)
                .collection("menu").document(menuItem.categoryId)
                .collection("items").document(menuItem.id)
                .set(menuItem)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        updateMenuItemInterface.onUpdated();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                updateMenuItemInterface.onError();
            }
        });


    }


    public void deleteItemFromMenu(final MenuItem menuItem, final DeleteObjectInDatabaseInterface deleteObjectInDatabaseInterface) {
        FirebaseFirestore.getInstance()
                .collection("restaurants").document(menuItem.restaurantId)
                .collection("menu").document(menuItem.categoryId)
                .collection("items").document(menuItem.id).delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        deleteObjectInDatabaseInterface.onDeleted();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                deleteObjectInDatabaseInterface.onError();
            }
        });
    }

}
