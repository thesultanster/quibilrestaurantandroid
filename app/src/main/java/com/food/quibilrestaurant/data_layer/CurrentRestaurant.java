package com.food.quibilrestaurant.data_layer;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.food.quibilrestaurant.interfaces.FetchRestaurantInterface;
import com.food.quibilrestaurant.interfaces.UpdateRestaurantInterface;
import com.food.quibilrestaurant.models.Restaurant;
import com.food.quibilrestaurant.models.User;
import com.food.quibilrestaurant.network_layer.RestaurantService;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 *
 */

public class CurrentRestaurant {
    private String TAG = "CurrentRestaurant";

    Context context;
    FirebaseDatabase database;
    SharedPreferences prefs;
    static CurrentRestaurant instance = null;

    // Normal Initialization gets user from cache
    protected CurrentRestaurant(Context context) {
        initializeRestaurant(context);
    }

    public static CurrentRestaurant getInstance(Context context) {
        if(instance == null) {
            instance = new CurrentRestaurant(context);
        }
        return instance;
    }



    private void initializeRestaurant(Context context){
        this.context = context;
        this.database = FirebaseDatabase.getInstance();
        prefs = context.getSharedPreferences("current_restaurant", MODE_PRIVATE);

        cacheRestaurant(new Restaurant());
    }

    public void fetchAndCache(final FetchRestaurantInterface fetchRestaurantInterface){
        RestaurantService.getInstance(context).getRestaurantFromDatabase(getCache().id, new FetchRestaurantInterface() {
            @Override
            public void onDataFetched(Restaurant restaurant) {
                cacheRestaurant(restaurant);
                fetchRestaurantInterface.onDataFetched(restaurant);
            }

            @Override
            public void onError() {
                fetchRestaurantInterface.onError();
            }
        });
    }

    public void updateRestaurantToDatabase(final Restaurant restaurant, UpdateRestaurantInterface updateRestaurantInterface){
       RestaurantService.getInstance(context).updateRetaurantToDatabase(restaurant,updateRestaurantInterface);
    }


    public void cacheRestaurant(Restaurant restaurant) {
        SharedPreferences.Editor prefsEditor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(restaurant);
        prefsEditor.putString("restaurant", json);
        prefsEditor.commit();
    }


    public Restaurant getCache() {
        Gson gson = new Gson();
        String json = prefs.getString("restaurant", "");
        return gson.fromJson(json, Restaurant.class);
    }





}
