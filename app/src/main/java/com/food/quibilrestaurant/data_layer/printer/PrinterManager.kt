package com.food.quibilrestaurant.data_layer.printer

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.util.Log
import com.food.quibilrestaurant.domain.ordermanager.IndividualOrderedItem
import com.food.quibilrestaurant.models.Session
import com.food.quibilrestaurant.utils.DateUtils
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*

class PrinterManager {

    lateinit var prefs: SharedPreferences
    var blockOperation = false

    fun init(context: Context?) {
        blockOperation = false

        if (context != null) {
            prefs = context!!.getSharedPreferences("app_state", MODE_PRIVATE)
        } else {
            blockOperation = true
        }
        //val btAdapter = BluetoothAdapter.getDefaultAdapter()
        //val mBtDevice = btAdapter.bondedDevices.iterator().next()   // Get first paired device

//        val mPrinter = BluetoothPrinter(mBtDevice)
//        mPrinter.connectPrinter(object: (BluetoothPrinter.PrinterConnectListener) {
//
//            override fun onConnected() {
//                mPrinter.setAlign(BluetoothPrinter.ALIGN_CENTER)
//                mPrinter.printText("Hello World!")
//                mPrinter.addNewLine()
//
//                mPrinter.finish()
//            }
//
//            override fun onFailed() {
//                Log.d("BluetoothPrinter", "Connection failed")
//            }
//
//        })
    }

    fun blockOperation() {
        blockOperation = true
    }

    fun getConnectedPrinter(): BluetoothDevice? {
        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        val pairedDevices = mBluetoothAdapter.bondedDevices

        if(blockOperation){
            return null
        }

        for (bluetoothDevice in pairedDevices) {
            if (getPrinterName() == bluetoothDevice.name) {
                return bluetoothDevice
            }
        }

        return null
    }

    fun printTestData(printerManagerInterface: PrinterManagerInterface) {
        var printer = getConnectedPrinter()
        if (printer == null) {
            printerManagerInterface.cannotFindPrinterInListOfBluetoothDevices()
        } else {
            val mPrinter = BluetoothPrinter(printer)
            mPrinter.connectPrinter(object : (BluetoothPrinter.PrinterConnectListener) {

                override fun onConnected() {
                    mPrinter.setAlign(BluetoothPrinter.ALIGN_CENTER)
                    mPrinter.addNewLine()
                    mPrinter.printText("***** Quibil Test *****")
                    mPrinter.addNewLine()
                    mPrinter.printText("TODO: This is a test")
                    mPrinter.addNewLine()
                    mPrinter.addNewLine()
                    mPrinter.setAlign(BluetoothPrinter.ALIGN_LEFT)
                    mPrinter.printText("ITEM: EXAMPLE ITEM")
                    mPrinter.addNewLine()

                    mPrinter.printText("TIME: 12:00AM")
                    mPrinter.addNewLine()
                    mPrinter.printText("TABLE: Dinner Table")
                    mPrinter.addNewLine()
                    mPrinter.printText("NAME: Boris Yeltson")
                    mPrinter.addNewLine()
                    mPrinter.addNewLine()
                    mPrinter.addNewLine()
                    mPrinter.printText("----------------------")
                    mPrinter.finish()

                    printerManagerInterface.onPrintSuccess()
                }

                override fun onFailed() {
                    Log.d("BluetoothPrinter", "Connection failed")
                    if (!blockOperation) {
                        printerManagerInterface.onPrintError()
                    }
                }

            })
        }

    }

    fun printOrder(individualItem: IndividualOrderedItem, printerManagerInterface: PrinterManagerInterface) {

        var itemName = individualItem.item.name
        var addOnItems = individualItem.item.addOnItems
        var timeOrdered = DateUtils.convertMillisecondsToTimeStamp(individualItem.orderedAt)
        var tableOrdered = individualItem.tableId
        var nameOrdered = individualItem.userFullName

        var printer = getConnectedPrinter()
        if (printer == null) {
            printerManagerInterface.cannotFindPrinterInListOfBluetoothDevices()
        } else {
            val mPrinter = BluetoothPrinter(getConnectedPrinter())
            mPrinter.connectPrinter(object : (BluetoothPrinter.PrinterConnectListener) {

                override fun onConnected() {
                    mPrinter.setAlign(BluetoothPrinter.ALIGN_CENTER)
                    mPrinter.addNewLine()
                    mPrinter.printText("***** Quibil Order *****")
                    mPrinter.addNewLine()
                    mPrinter.printText("TODO: MAKE AND DELIVER")
                    mPrinter.addNewLine()
                    mPrinter.addNewLine()
                    mPrinter.setAlign(BluetoothPrinter.ALIGN_LEFT)
                    mPrinter.printText("ITEM: $itemName")
                    mPrinter.addNewLine()

                    if (addOnItems.size > 0) {
                        mPrinter.printText("ADD-ONS:")
                        mPrinter.addNewLine()
                        for (item in addOnItems) {
                            mPrinter.printText("         ${item.name}")
                            mPrinter.addNewLine()
                        }
                    }
                    mPrinter.printText("TIME: $timeOrdered")
                    mPrinter.addNewLine()
                    mPrinter.printText("TABLE: $tableOrdered")
                    mPrinter.addNewLine()
                    mPrinter.printText("NAME: $nameOrdered")
                    mPrinter.addNewLine()
                    mPrinter.addNewLine()
                    mPrinter.addNewLine()
                    mPrinter.printText("----------------------")
                    mPrinter.finish()

                    printerManagerInterface.onPrintSuccess()
                }

                override fun onFailed() {
                    Log.d("BluetoothPrinter", "Connection failed")
                    mPrinter.finish()
                    if (!blockOperation) {
                        printerManagerInterface.onPrintError()
                    }
                }

            })
        }
    }

    fun printReceipt(printerName: String, session: Session, printerManagerInterface: PrinterManagerInterface) {

        val formatter = SimpleDateFormat("hh:mm a", Locale.US)
        formatter.timeZone = TimeZone.getTimeZone("UTC")
        val timeStamp = formatter.format(session.updatedAt)


        var printer = getConnectedPrinter()
        if (printer == null) {
            printerManagerInterface.cannotFindPrinterInListOfBluetoothDevices()
        } else {
            val mPrinter = BluetoothPrinter(getConnectedPrinter())
            mPrinter.connectPrinter(object : (BluetoothPrinter.PrinterConnectListener) {

                override fun onConnected() {
                    mPrinter.setAlign(BluetoothPrinter.ALIGN_CENTER)
                    mPrinter.addNewLine()
                    mPrinter.printText("***** RECEIPT *****")
                    mPrinter.addNewLine()
                    mPrinter.addNewLine()
                    mPrinter.setAlign(BluetoothPrinter.ALIGN_LEFT)
                    for (item in session.orders) {

                        val numberFormatter = NumberFormat.getCurrencyInstance(Locale.US)
                        val itemPrice = numberFormatter.format(item.price / 100.0)

                        mPrinter.printText("$itemPrice   ${item.name}")
                        mPrinter.addNewLine()

                        if (item.addOnItems.size > 0) {
                            mPrinter.printText("ADD-ONS:")
                            mPrinter.addNewLine()
                            for (addOns in item.addOnItems) {
                                val addonPrice = numberFormatter.format(addOns.price / 100.0)
                                mPrinter.printText("   $addonPrice   ${addOns.name}")
                                mPrinter.addNewLine()
                            }
                        }
                    }


                    mPrinter.addNewLine()
                    mPrinter.printText("NAME: ${session.userFullName}")
                    mPrinter.addNewLine()
                    mPrinter.printText("TIME: $timeStamp")
                    mPrinter.addNewLine()
                    mPrinter.printText("TABLE: ${session.tableId}")
                    mPrinter.addNewLine()
                    mPrinter.finish()

                    printerManagerInterface.onPrintSuccess()
                }

                override fun onFailed() {
                    Log.d("BluetoothPrinter", "Connection failed")

                    if (!blockOperation) {
                        printerManagerInterface.onPrintError()
                    }
                }

            })
        }

    }

    fun isConnected(prirnterConnectionnterface: PrinterConnectionnterface) {
        var printer = getConnectedPrinter()
        if (printer == null) {
            prirnterConnectionnterface.cannotFindPrinterInListOfBluetoothDevices()
        } else {
            val mPrinter = BluetoothPrinter(getConnectedPrinter())
            mPrinter.connectPrinter(object : (BluetoothPrinter.PrinterConnectListener) {

                override fun onConnected() {

                    prirnterConnectionnterface.isConnected()
                }

                override fun onFailed() {
                    Log.d("BluetoothPrinter", "Connection failed")
                    prirnterConnectionnterface.isDisconnected()
                }

            })
        }
    }


    fun cachePrinterAddress(printerAddress: String) {
        val prefsEditor = prefs.edit()
        prefsEditor.putString("printerAddress", printerAddress)
        prefsEditor.commit()
    }

    fun getPrinterAddress(): String? {
        return prefs.getString("printerAddress", "")
    }

    fun cachePrinterName(printerName: String) {
        val prefsEditor = prefs.edit()
        prefsEditor.putString("printerName", printerName)
        prefsEditor.commit()
    }

    fun getPrinterName(): String? {
        return prefs.getString("printerName", "")
    }


}