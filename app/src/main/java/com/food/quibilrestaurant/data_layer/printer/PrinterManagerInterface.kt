package com.food.quibilrestaurant.data_layer.printer

interface PrinterManagerInterface {
    fun cannotFindPrinterInListOfBluetoothDevices()
    fun onPrintSuccess()
    fun onPrintError()
}