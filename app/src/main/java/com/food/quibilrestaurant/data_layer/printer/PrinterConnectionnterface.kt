package com.food.quibilrestaurant.data_layer.printer

interface PrinterConnectionnterface {
    fun cannotFindPrinterInListOfBluetoothDevices()
    fun isConnected()
    fun isDisconnected()
}