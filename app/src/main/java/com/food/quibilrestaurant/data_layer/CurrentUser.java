package com.food.quibilrestaurant.data_layer;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;

import com.food.quibilrestaurant.interfaces.FetchUserInterface;
import com.food.quibilrestaurant.interfaces.UpdateUserInterface;
import com.food.quibilrestaurant.models.Restaurant;
import com.food.quibilrestaurant.models.User;
import com.food.quibilrestaurant.network_layer.UserService;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by sultankhan on 7/22/17.
 */

public class CurrentUser {
    private String TAG = "CurrentUser";

    public User data;
    private Context context;
    private FirebaseAuth mAuth;
    FirebaseDatabase database;
    SharedPreferences prefs;
    private static CurrentUser instance = null;

    // Normal Initialization gets user from cache
    protected CurrentUser(Context context) {
        initializeUser(context);
    }

    public static CurrentUser getInstance(Context context) {
        if(instance == null) {
            instance = new CurrentUser(context);
        }
        return instance;
    }


    private void initializeUser(Context context){
        this.context = context;
        this.database = FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();
        data = new User();
        prefs = context.getSharedPreferences("current_user", MODE_PRIVATE);
    }

    public void fetch(final FetchUserInterface fetchUserInterface){
        UserService.getInstance(context).getUserFromDatabase(getUid(), new FetchUserInterface() {
            @Override
            public void onDataFetched(User user) {
                data = user;
                cacheUser(user);
                fetchUserInterface.onDataFetched(user);
            }

            @Override
            public void onUserDataNotFound() {
                data = new User();
                fetchUserInterface.onUserDataNotFound();
            }
        });
    }

    public void persistAndCache(final User currentUser, final UpdateUserInterface updateUserInterface){

       UserService.getInstance(context).updateUser(currentUser, new UpdateUserInterface() {
           @Override
           public void onUpdated() {
               cacheUser(currentUser);
               updateUserInterface.onUpdated();
           }

           @Override
           public void onError() {
               updateUserInterface.onError();
           }
       });

    }

    public String getUid(){
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }


    public void cacheUser(User user) {
        SharedPreferences.Editor prefsEditor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(user);
        prefsEditor.putString("user", json);
        prefsEditor.commit();
    }


    public User getCache() {
        Gson gson = new Gson();
        String json = prefs.getString("user", "");
        return gson.fromJson(json, User.class);
    }








}
