package com.food.quibilrestaurant.data_layer;

import android.content.Context;
import android.content.SharedPreferences;

import com.food.quibilrestaurant.interfaces.FetchUserInterface;
import com.food.quibilrestaurant.interfaces.UpdateUserInterface;
import com.food.quibilrestaurant.models.LockscreenCredential;
import com.food.quibilrestaurant.models.Restaurant;
import com.food.quibilrestaurant.models.User;
import com.food.quibilrestaurant.network_layer.UserService;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by sultankhan on 7/22/17.
 */

public class AppState {
    private String TAG = "AppState";

    public User data;
    FirebaseDatabase database;
    SharedPreferences prefs;
    private static AppState instance = null;

    // Normal Initialization gets user from cache
    protected AppState(Context context) {
        initializeUser(context);
    }

    public static AppState getInstance(Context context) {
        if(instance == null) {
            instance = new AppState(context);
        }
        return instance;
    }


    private void initializeUser(Context context){
        this.database = FirebaseDatabase.getInstance();
        data = new User();
        prefs = context.getSharedPreferences("app_state", MODE_PRIVATE);
    }


    public void cacheAppMode(String mode){
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putString("mode", mode);
        prefsEditor.commit();
    }

    public String getAppMode(){
        return prefs.getString("mode","MANAGER");
    }

    public void cacheVipAuthStatus(String status){
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putString("vipAuthStatus", status);
        prefsEditor.commit();
    }

    public String getVipAuthStatus(){
        return prefs.getString("vipAuthStatus","");
    }


    public void cacheIsManagerStaffAccountCreated(Boolean isManagerStaffAccountCreated){
        prefs.edit().putBoolean("isManagerStaffAccountCreated", isManagerStaffAccountCreated).commit();
    }

    public Boolean getIsManagerStaffAccountCreated(){
        return prefs.getBoolean("isManagerStaffAccountCreated", false);
    }

    public void cacheLockscreenCredential(LockscreenCredential lockscreenCredential) {
        SharedPreferences.Editor prefsEditor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(lockscreenCredential);
        prefsEditor.putString("lockscreenCredential", json);
        prefsEditor.commit();
    }


    public LockscreenCredential getLockscreenCredential() {
        Gson gson = new Gson();
        String json = prefs.getString("lockscreenCredential", "");
        return gson.fromJson(json, LockscreenCredential.class);
    }


}
