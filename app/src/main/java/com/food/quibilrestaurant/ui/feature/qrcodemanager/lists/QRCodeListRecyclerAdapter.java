package com.food.quibilrestaurant.ui.feature.qrcodemanager.lists;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.food.quibilrestaurant.R;
import com.food.quibilrestaurant.models.QRCode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class QRCodeListRecyclerAdapter extends RecyclerView.Adapter<QRCodeListRecyclerAdapter.MyViewHolder> {

    // emptyList takes care of null pointer exception
    List<QRCode> data = Collections.emptyList();
    LayoutInflater inflator;
    Context context;
    OnQRCodeListSelectedInterface onQRCodeListSelectedInterface;


    public QRCodeListRecyclerAdapter(Context context, List<QRCode> data, OnQRCodeListSelectedInterface onQRCodeListSelectedInterface) {
        this.context = context;
        inflator = LayoutInflater.from(context);
        this.data = data;
        this.onQRCodeListSelectedInterface = onQRCodeListSelectedInterface;
    }

    public void addRow(QRCode qrCode) {
        data.add(qrCode);
        notifyDataSetChanged();
    }

    public void clear() {
        data = new ArrayList<>();
        notifyDataSetChanged();
    }

    // Called when the recycler view needs to create a new row
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        final View view = inflator.inflate(R.layout.row_qr_code, parent, false);
        final MyViewHolder holder = new MyViewHolder(view, context, new MyViewHolder.MyViewHolderClicks() {

            public void rowClick(View caller, int position) {
                Log.d("rowClick", "rowClicks");

            }

            @Override
            public void editQrCOde(View caller, int position) {
                Log.d("rowClick", "editQrCOde");
                onQRCodeListSelectedInterface.onEditQrCode(position, data.get(position));

            }
        });
        return holder;
    }

    // Setting up the data for each row
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        QRCode current = data.get(position);
        holder.tvTableId.setText(current.getTableId());
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    // Created my custom view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvTableId;
        ImageView ivEdit;
        Context context;


        public MyViewHolderClicks mListener;

        // itemView will be my own custom layout View of the row
        public MyViewHolder(View itemView, Context context, MyViewHolderClicks listener) {
            super(itemView);
            mListener = listener;
            this.context = context;

            tvTableId = itemView.findViewById(R.id.tvTableId);
            ivEdit = itemView.findViewById(R.id.ivEdit);

            ivEdit.setOnClickListener(this);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivEdit:
                    mListener.editQrCOde(v, getAdapterPosition());
                    break;
                default:
                    mListener.rowClick(v, getAdapterPosition());
                    break;
            }
        }

        public interface MyViewHolderClicks {
            void rowClick(View caller, int position);
            void editQrCOde(View caller, int position);
        }
    }


}