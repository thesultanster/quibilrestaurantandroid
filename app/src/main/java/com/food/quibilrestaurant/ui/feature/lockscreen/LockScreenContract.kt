package com.food.quibilrestaurant.ui.feature.lockscreen

import android.content.Context

interface LockScreenContract {
    interface View {
        fun hideProgressBar()
        fun showSnackbar(title: String)
        fun goToMainActivity()
    }

    interface Presenter {
        fun getCredentials(restaurantId: String, pin: String, context: Context)
    }
}