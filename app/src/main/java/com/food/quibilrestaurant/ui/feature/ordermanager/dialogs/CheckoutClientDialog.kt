package com.food.quibilrestaurant.ui.feature.ordermanager.dialogs

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.widget.Button

import com.food.quibilrestaurant.R

import com.food.quibilrestaurant.models.Session
import kotlinx.android.synthetic.main.dialog_checkout_client.view.*

class CheckoutClientDialog : DialogFragment() {

    lateinit var session: Session
    private var checkoutClientInterface: CheckoutClientInterface? = null

    // This is a workaround for the strange behavior of onCreateView (which doesn't show dialog's layout)
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialogBuilder = AlertDialog.Builder(context!!)
        val inflater = LayoutInflater.from(context)
        val dialogView = inflater.inflate(R.layout.dialog_checkout_client, null)

        session = arguments!!.getSerializable("session") as Session

        initSubViews(dialogView)

        // Set dialog to not be cancelable
        isCancelable = false

        dialogBuilder.setView(dialogView)
        return dialogBuilder.create()
    }


    private fun initSubViews(dialogView: View) {

        dialogView.btnCheckoutOnly.setOnClickListener {
            checkoutClientInterface!!.checkoutUser()
            dismiss()
        }

        dialogView.btnCancel.setOnClickListener { dialog.dismiss() }

    }

    companion object {
        fun newInstance(session: Session, checkoutClientInterface: CheckoutClientInterface): CheckoutClientDialog {
            val frag = CheckoutClientDialog()
            frag.checkoutClientInterface = checkoutClientInterface
            val args = Bundle()
            args.putSerializable("session", session)
            frag.arguments = args
            return frag
        }
    }
}
