package com.food.quibilrestaurant.ui.feature.menueditor;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.food.quibilrestaurant.R;
import com.food.quibilrestaurant.adapters.MenuCategoriesRecyclerAdapter;
import com.food.quibilrestaurant.interfaces.DeleteObjectInDatabaseInterface;
import com.food.quibilrestaurant.ui.feature.menueditor.dialogs.DeleteMenuItemDialog;
import com.food.quibilrestaurant.ui.feature.menueditor.dialogs.DeleteMenuItemInterface;
import com.food.quibilrestaurant.ui.feature.menueditor.dialogs.DeleteStaffInterface;
import com.food.quibilrestaurant.ui.feature.menueditor.lists.MenuItemsRecyclerAdapter;
import com.food.quibilrestaurant.data_layer.CurrentRestaurant;
import com.food.quibilrestaurant.dialogs.AddMenuCategoryDialog;
import com.food.quibilrestaurant.dialogs.AddMenuItemDialog;
import com.food.quibilrestaurant.dialogs.UpdateMenuCategoryDialog;
import com.food.quibilrestaurant.interfaces.CreateMenuCategoryInterface;
import com.food.quibilrestaurant.interfaces.CreateMenuItemInterface;
import com.food.quibilrestaurant.interfaces.FetchRestaurantMenuCategoriesInterface;
import com.food.quibilrestaurant.interfaces.FetchRestaurantMenuItemsInterface;
import com.food.quibilrestaurant.interfaces.OnMenuCategorySelectedInterface;
import com.food.quibilrestaurant.interfaces.OnMenuItemSelectedInterface;
import com.food.quibilrestaurant.interfaces.OnStartDragListener;
import com.food.quibilrestaurant.interfaces.UpdateMenuCategoryInterface;
import com.food.quibilrestaurant.interfaces.UserEnteredNewMenuCategoryInterface;
import com.food.quibilrestaurant.interfaces.UserEnteredNewMenuItemInterface;
import com.food.quibilrestaurant.interfaces.UserUpdatedMenuCategoryInterface;
import com.food.quibilrestaurant.models.MenuCategory;
import com.food.quibilrestaurant.models.MenuItem;
import com.food.quibilrestaurant.models.Restaurant;
import com.food.quibilrestaurant.network_layer.MenuService;
import com.food.quibilrestaurant.utils.SimpleItemTouchHelperCallback;

import java.util.ArrayList;


public class MenuEditorFragment extends Fragment implements OnStartDragListener{

    RecyclerView rvMenuCategories;
    RecyclerView rvMenuItems;
    NestedScrollView nsvItems;

    MenuCategoriesRecyclerAdapter menuCategoriesRecyclerAdapter;
    MenuItemsRecyclerAdapter menuItemsRecyclerAdapter;
    ItemTouchHelper touchHelper;

    Typeface openFace;
    TextView tvAddCategory;
    TextView tvAddItem;

    Restaurant currentRestaurant;
    MenuCategory currentSelectedCategory;
    MenuItem currentSelectedItem;


    public MenuEditorFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        openFace = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Light.ttf");
        menuCategoriesRecyclerAdapter = new MenuCategoriesRecyclerAdapter(getContext(), new ArrayList<MenuCategory>(), this, new OnMenuCategorySelectedInterface() {
            @Override
            public void onCategorySelected(int position, MenuCategory menuCategory) {
                currentSelectedCategory = menuCategory;
                nsvItems.setVisibility(View.VISIBLE);

                refreshCategoryItemsList();
            }

            @Override
            public void onEdit(final int position, final MenuCategory menuCategory) {
                // Show dialog with interface when they select a title for the new category.
                DialogFragment newFragment = UpdateMenuCategoryDialog.newInstance(menuCategory, new UserUpdatedMenuCategoryInterface() {


                    @Override
                    public void userUpdatedMenuCategory(final MenuCategory menuCategory) {
                        MenuService.getInstance(getContext()).updateMenuCategory(menuCategory, new UpdateMenuCategoryInterface() {

                            @Override
                            public void onUpdated() {
                                menuCategoriesRecyclerAdapter.updateRow(menuCategory, position);
                            }

                            @Override
                            public void onError() {

                            }
                        });
                    }
                });
                newFragment.show(getFragmentManager(), "dialog");
            }
        });

        menuItemsRecyclerAdapter = new MenuItemsRecyclerAdapter(getContext(), new ArrayList<MenuItem>(), new OnMenuItemSelectedInterface(){

            @Override
            public void onItemSelected(int position, MenuItem menuItem) {
                currentSelectedItem = menuItem;

                // Create a new fragment and specify the fragment to show based on nav item clicked
                MenuItemFragment fragment = MenuItemFragment.newInstance(menuItem);
                // Insert the fragment by replacing any existing fragment
                FragmentManager fragmentManager = getChildFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.flMenuItemDetail, fragment, "flMenuItemDetail").commit();
            }

            @Override
            public void onUserWantsToDeleteItem(int position, final MenuItem menuItem) {
                // Show dialog with interface when they select a title for the new category.
                DialogFragment newFragment = DeleteMenuItemDialog.Companion.newInstance(new DeleteMenuItemInterface() {
                    @Override
                    public void userWantsToDeleteMenuItem() {
                        MenuService.getInstance(getContext()).deleteItemFromMenu(menuItem, new DeleteObjectInDatabaseInterface() {
                            @Override
                            public void onDeleted() {

                                    refreshCategoryItemsList();
                            }

                            @Override
                            public void onError() {

                            }
                        });
                    }
                });
                newFragment.show(getFragmentManager(), "dialog");
            }
        });

        currentRestaurant = CurrentRestaurant.getInstance(getContext()).getCache();

    }

    void refreshCategoryItemsList(){
        menuItemsRecyclerAdapter.clearRows();
        MenuService.getInstance(getContext()).getMenuItems(currentSelectedCategory.restaurantId, currentSelectedCategory.id, new FetchRestaurantMenuItemsInterface() {
            @Override
            public void onDataFetched(MenuItem menuItem) {
                menuItemsRecyclerAdapter.addRow(menuItem);
            }

            @Override
            public void onError() {

            }
        });

        FragmentManager fragmentManager = getChildFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.flMenuItemDetail);
        if(fragment != null) {
            fragmentManager.beginTransaction().remove(fragment).commit();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_menu_editor, container, false);

        rvMenuCategories = (RecyclerView) view.findViewById(R.id.rvMenuCategories);
        rvMenuItems = (RecyclerView) view.findViewById(R.id.rvMenuItems);
        tvAddCategory = view.findViewById(R.id.tvAddCategory);
        tvAddItem = view.findViewById(R.id.tvAddItem);
        nsvItems = view.findViewById(R.id.nsvItems);

        nsvItems.setVisibility(View.INVISIBLE);

        rvMenuCategories.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvMenuCategories.setAdapter(menuCategoriesRecyclerAdapter);

        ItemTouchHelper.Callback callback =
                new SimpleItemTouchHelperCallback(menuCategoriesRecyclerAdapter);
        touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(rvMenuCategories);

        rvMenuItems.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvMenuItems.setAdapter(menuItemsRecyclerAdapter);

        MenuItem menuItem = new MenuItem();
        menuItem.name = "adfasffffffsd";
        menuItemsRecyclerAdapter.addRow(menuItem);


        // Fetch all categories and populate list
        MenuService.getInstance(getContext()).getMenuCategories(currentRestaurant.id, new FetchRestaurantMenuCategoriesInterface() {
            @Override
            public void onDataFetched(MenuCategory menuCategory) {
                menuCategoriesRecyclerAdapter.addRow(menuCategory);
            }

            @Override
            public void onError() {

            }
        });


        // User selects add category buttonso we ope up the dialog
        tvAddCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Show dialog with interface when they select a title for the new category.
                DialogFragment newFragment = AddMenuCategoryDialog.newInstance("New Category", new UserEnteredNewMenuCategoryInterface() {
                    @Override
                    public void userEnteredNewMenuCategory(final String menuCategoryTitle) {
                        MenuService.getInstance(getContext()).addCategoryToMenu(currentRestaurant.id, menuCategoryTitle, new CreateMenuCategoryInterface() {
                            @Override
                            public void onCreated() {

                                // Add category to list adapter
                                MenuCategory menuCategory = new MenuCategory();
                                menuCategory.name = menuCategoryTitle;
                                menuCategory.index = menuCategoriesRecyclerAdapter.getItemCount();
                                menuCategoriesRecyclerAdapter.addRow(menuCategory);
                            }

                            @Override
                            public void onError() {

                            }
                        });
                    }

                });
                newFragment.show(getFragmentManager(), "dialog");
            }
        });


        // User selects add item button so we ope up the dialog
        tvAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Show dialog with interface when they select a title for the new category.
                DialogFragment newFragment = AddMenuItemDialog.newInstance(currentSelectedCategory, new UserEnteredNewMenuItemInterface() {
                    @Override
                    public void userEnteredNewMenuItem(final MenuItem menuItem) {
                        MenuService.getInstance(getContext()).addItemToMenu(menuItem, new CreateMenuItemInterface() {
                            @Override
                            public void onCreated() {
                                menuItemsRecyclerAdapter.addRow(menuItem);
                            }

                            @Override
                            public void onError() {

                            }
                        });
                    }

                });
                newFragment.show(getFragmentManager(), "dialog");
            }
        });







        return view;
    }


    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        touchHelper.startDrag(viewHolder);
    }
}
