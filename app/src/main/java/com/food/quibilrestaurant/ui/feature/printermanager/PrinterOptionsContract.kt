package com.food.quibilrestaurant.ui.feature.printermanager

import android.bluetooth.BluetoothDevice
import com.food.quibilrestaurant.data_layer.printer.PrinterManager

interface PrinterOptionsContract {

    interface View {
        fun onBluetoothDisabled()
        fun onBluetoothEnabled()
        fun addAvailableBluetoothDevice(bluetoothDevice: BluetoothDevice)
        fun selectPrinterAsDefault(device: BluetoothDevice)
        fun rescanBluetooth()
        fun onScanComplete()
        fun onDeviceConnected(device: BluetoothDevice?)
        fun onDeviceDisconnected()
        fun onDeviceMaybeConnecting()
    }

    interface Presenter {
        fun onCreate()
        fun onStart(printerManager: PrinterManager)
        fun onViewCreated()
        fun onDestroy(printerManager: PrinterManager)
        fun populateDeviceList()
    }
}