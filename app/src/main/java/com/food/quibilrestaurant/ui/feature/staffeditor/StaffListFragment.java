package com.food.quibilrestaurant.ui.feature.staffeditor;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.food.quibilrestaurant.R;
import com.food.quibilrestaurant.dialogs.UpdateMenuCategoryDialog;
import com.food.quibilrestaurant.interfaces.DeleteObjectInDatabaseInterface;
import com.food.quibilrestaurant.interfaces.UpdateMenuCategoryInterface;
import com.food.quibilrestaurant.interfaces.UserUpdatedMenuCategoryInterface;
import com.food.quibilrestaurant.models.MenuCategory;
import com.food.quibilrestaurant.network_layer.MenuService;
import com.food.quibilrestaurant.ui.feature.menueditor.dialogs.DeleteStaffInterface;
import com.food.quibilrestaurant.ui.feature.staffeditor.dialogs.DeleteStaffDialog;
import com.food.quibilrestaurant.ui.feature.staffeditor.lists.StaffWorkersRecyclerAdapter;
import com.food.quibilrestaurant.data_layer.CurrentRestaurant;
import com.food.quibilrestaurant.dialogs.AddStaffWorkerDialog;
import com.food.quibilrestaurant.interfaces.CreateStaffWorkerInterface;
import com.food.quibilrestaurant.interfaces.FetchRestaurantStaffInterface;
import com.food.quibilrestaurant.interfaces.OnStaffWorkerSelectedInterface;
import com.food.quibilrestaurant.interfaces.UserEnteredNewStaffWorkerInterface;
import com.food.quibilrestaurant.models.Restaurant;
import com.food.quibilrestaurant.models.StaffWorker;
import com.food.quibilrestaurant.network_layer.StaffService;

import java.util.ArrayList;


public class StaffListFragment extends Fragment {

    RecyclerView rvStaffWorkers;

    StaffWorkersRecyclerAdapter staffWorkersRecyclerAdapter;

    Typeface openFace;
    TextView tvAddStaffWorker;

    Restaurant currentRestaurant;
    StaffWorker currentSelectedStaffWorker;


    public StaffListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        openFace = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Light.ttf");
        staffWorkersRecyclerAdapter = new StaffWorkersRecyclerAdapter(getContext(), new ArrayList<StaffWorker>(), new OnStaffWorkerSelectedInterface() {
            @Override
            public void onCategorySelected(int position, StaffWorker staffWorker) {
                currentSelectedStaffWorker = staffWorker;

                // Create a new fragment and specify the fragment to show based on nav item clicked
                StaffWorkerViewerAndEditorFragment fragment = StaffWorkerViewerAndEditorFragment.newInstance(staffWorker);
                // Insert the fragment by replacing any existing fragment
                FragmentManager fragmentManager = getChildFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.flMenuItemDetail, fragment).commit();

            }

            @Override
            public void onUserWantsToDeleteStaff(int position, final StaffWorker staffWorker) {
                // Show dialog with interface when they select a title for the new category.
                DialogFragment newFragment = DeleteStaffDialog.Companion.newInstance( new DeleteStaffInterface() {

                    @Override
                    public void userWantsToDeleteStaff() {
                        StaffService.getInstance(getContext()).deleteStaffWorker(
                                CurrentRestaurant.getInstance(getContext()).getCache().id,
                                staffWorker,
                                new DeleteObjectInDatabaseInterface() {
                                    @Override
                                    public void onDeleted() {

                                        fetchStaffWorkers();

                                    }

                                    @Override
                                    public void onError() {

                                    }
                                }
                        );
                    }
                });
                newFragment.show(getFragmentManager(), "dialog");
            }
        });


        currentRestaurant = CurrentRestaurant.getInstance(getContext()).getCache();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_staff_list, container, false);

        rvStaffWorkers = (RecyclerView) view.findViewById(R.id.rvStaffWorkers);
        tvAddStaffWorker = view.findViewById(R.id.tvAddStaffWorker);

        rvStaffWorkers.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvStaffWorkers.setAdapter(staffWorkersRecyclerAdapter);


        fetchStaffWorkers();

        // User selects add category buttonso we ope up the dialog
        tvAddStaffWorker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Show dialog with interface when they select a title for the new category.
                DialogFragment newFragment = AddStaffWorkerDialog.newInstance(currentRestaurant.id, new UserEnteredNewStaffWorkerInterface() {
                    @Override
                    public void userEnteredNewStaffWorkerCategory(final StaffWorker staffWorker) {

                        // Hard code staff since we can only add staff workers here
                        staffWorker.profileType = "STAFF";
                        staffWorker.worksAtRestaurantId = currentRestaurant.id;
                        StaffService.getInstance(getContext()).addNewStaffWorker(currentRestaurant.id, staffWorker, new CreateStaffWorkerInterface() {
                            @Override
                            public void onCreated() {

                                // Add category to list adapter
                                staffWorkersRecyclerAdapter.addRow(staffWorker);
                            }

                            @Override
                            public void onError() {

                            }
                        });
                    }

                });
                newFragment.show(getFragmentManager(), "dialog");
            }
        });



        return view;
    }

    public void fetchStaffWorkers(){

        staffWorkersRecyclerAdapter.clear();
        // Fetch all staff and populate list
        StaffService.getInstance(getContext()).getRestaurantStaffWorkers(currentRestaurant.id, new FetchRestaurantStaffInterface() {
            @Override
            public void onDataFetched(StaffWorker staffWorker) {
                staffWorkersRecyclerAdapter.addRow(staffWorker);
            }

            @Override
            public void onError() {

            }
        });

        FragmentManager fragmentManager = getChildFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.flMenuItemDetail);
        if(fragment != null) {
            fragmentManager.beginTransaction().remove(fragment).commit();
        }
    }


}
