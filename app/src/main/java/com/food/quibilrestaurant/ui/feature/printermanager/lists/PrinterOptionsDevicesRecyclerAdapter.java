package com.food.quibilrestaurant.ui.feature.printermanager.lists;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.food.quibilrestaurant.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PrinterOptionsDevicesRecyclerAdapter extends RecyclerView.Adapter<PrinterOptionsDevicesRecyclerAdapter.MyViewHolder> {

    // emptyList takes care of null pointer exception
    List<BluetoothDevice> data = Collections.emptyList();
    LayoutInflater inflator;
    Context context;
    OnBluetoothDeviceSelectedInterface onBluetoothDeviceSelectedInterface;
    int rowSelected = -1;

    public PrinterOptionsDevicesRecyclerAdapter(Context context, List<BluetoothDevice> data, OnBluetoothDeviceSelectedInterface onBluetoothDeviceSelectedInterface) {
        this.context = context;
        inflator = LayoutInflater.from(context);
        this.data = data;
        this.onBluetoothDeviceSelectedInterface = onBluetoothDeviceSelectedInterface;
    }

    public void addRow(BluetoothDevice printer) {
        data.add(printer);
        notifyDataSetChanged();
    }



    public void clear() {
        data = new ArrayList<>();
        notifyDataSetChanged();
    }

    // Called when the recycler view needs to create a new row
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        final View view = inflator.inflate(R.layout.row_printer_device, parent, false);
        final MyViewHolder holder = new MyViewHolder(view, context, new MyViewHolder.MyViewHolderClicks() {

            public void rowClick(View caller, int position) {
                Log.d("rowClick", "rowClicks");

                rowSelected = position;

                // Pass selected item down the interface
                onBluetoothDeviceSelectedInterface.onSelected(position, data.get(position));

                notifyDataSetChanged();

            }

            @Override
            public void printOrder(View caller, int position) {

            }
        });
        return holder;
    }

    // Setting up the data for each row
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        BluetoothDevice current = data.get(position);
        holder.tvDeviceMAC.setText(current.getAddress());
        holder.tvDeviceTitle.setText(current.getName());

        if(rowSelected == position){
            holder.ivCheck.setVisibility(View.VISIBLE);
        }
        else {
            holder.ivCheck.setVisibility(View.INVISIBLE);
        }
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    // Created my custom view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvDeviceTitle;
        TextView tvDeviceMAC;
        ImageView ivCheck;
        Context context;


        public MyViewHolderClicks mListener;

        // itemView will be my own custom layout View of the row
        public MyViewHolder(View itemView, Context context, MyViewHolderClicks listener) {
            super(itemView);
            mListener = listener;
            this.context = context;

            ivCheck = itemView.findViewById(R.id.ivCheck);
            tvDeviceTitle = itemView.findViewById(R.id.tvDeviceTitle);
            tvDeviceMAC = itemView.findViewById(R.id.tvDeviceMAC);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btnPrintOrder:
                    mListener.printOrder(v, getAdapterPosition());
                    break;
                default:
                    mListener.rowClick(v, getAdapterPosition());
                    break;
            }
        }

        public interface MyViewHolderClicks {
            void rowClick(View caller, int position);

            void printOrder(View caller, int position);
        }
    }


}