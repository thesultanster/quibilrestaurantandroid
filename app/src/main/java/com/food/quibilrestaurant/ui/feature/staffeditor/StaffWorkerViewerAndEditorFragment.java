package com.food.quibilrestaurant.ui.feature.staffeditor;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.food.quibilrestaurant.R;
import com.food.quibilrestaurant.activities.BaseFragment;
import com.food.quibilrestaurant.interfaces.CanPinBeMadeInterface;
import com.food.quibilrestaurant.interfaces.UpdateStaffWorkerInterface;
import com.food.quibilrestaurant.models.StaffWorker;
import com.food.quibilrestaurant.network_layer.StaffService;
import com.food.quibilrestaurant.network_layer.UserService;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.rey.material.widget.EditText;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.net.URL;

import de.hdodenhof.circleimageview.CircleImageView;


public class StaffWorkerViewerAndEditorFragment extends BaseFragment {
    String TAG = "StaffWorkerViewerAndEditorFragment";

    TextView tvName;
    TextView tvProfileTypeTitle;
    TextView tvPin;
    TextView tvChangeProfilePhoto;
    EditText edtName;
    EditText edtProfileTypeTitle;
    EditText edtPin;
    TextView tvEdit;
    CircleImageView civProfilePicture;

    boolean isEditOn = false;

    Typeface openFace;
    StaffWorker staffWorker;


    public StaffWorkerViewerAndEditorFragment() {
        // Required empty public constructor
    }

    public static StaffWorkerViewerAndEditorFragment newInstance(StaffWorker staffWorker) {
        StaffWorkerViewerAndEditorFragment frag = new StaffWorkerViewerAndEditorFragment();
        Bundle args = new Bundle();
        args.putSerializable("staffWorker", staffWorker);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        openFace = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Light.ttf");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_profile_viewer_editor, container, false);
        staffWorker = (StaffWorker) getArguments().getSerializable("staffWorker");

        tvName = (TextView) view.findViewById(R.id.tvName);
        tvProfileTypeTitle = (TextView) view.findViewById(R.id.tvProfileTypeTitle);
        tvEdit = (TextView) view.findViewById(R.id.tvEdit);
        tvPin = (TextView) view.findViewById(R.id.tvPin);
        tvChangeProfilePhoto = (TextView) view.findViewById(R.id.tvChangeProfilePhoto);
        edtName = (EditText) view.findViewById(R.id.edtName);
        edtProfileTypeTitle = (EditText) view.findViewById(R.id.edtProfileTypeTitle);
        edtPin = (EditText) view.findViewById(R.id.edtPin);
        civProfilePicture = (CircleImageView) view.findViewById(R.id.civProfilePicture);

        tvChangeProfilePhoto.setVisibility(View.GONE);


        tvChangeProfilePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkCameraPermission()) {
                    takePhotoOrChoose();
                } else {
                    requestPermission();
                }
            }
        });
        // If picture url is not empty
        if(staffWorker.profilePictureURL != null && !staffWorker.profilePictureURL.equals("")) {
            Picasso.with(getContext()).load(staffWorker.profilePictureURL).into(civProfilePicture);
        }


        tvName.setText(staffWorker.name);
        tvProfileTypeTitle.setText(staffWorker.profileTypeTitle);
        tvPin.setText(staffWorker.pin);
        edtName.setText(staffWorker.name);
        edtProfileTypeTitle.setText(staffWorker.profileTypeTitle);
        edtPin.setText(staffWorker.pin);

        tvEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isEditOn) {
                    isEditOn = false;
                    tvEdit.setText("EDIT");


                    tvName.setText(edtName.getText().toString());
                    tvProfileTypeTitle.setText(edtProfileTypeTitle.getText().toString());
                    tvPin.setText(edtPin.getText().toString());

                    staffWorker.name = edtName.getText().toString();
                    staffWorker.profileTypeTitle = edtProfileTypeTitle.getText().toString();
                    staffWorker.pin = edtPin.getText().toString();
                    StaffService.getInstance(getContext()).updateStaffWorker(staffWorker.worksAtRestaurantId, staffWorker, new UpdateStaffWorkerInterface() {
                        @Override
                        public void onUpdated() {

                        }

                        @Override
                        public void onError() {

                        }
                    });

                    tvName.setVisibility(View.VISIBLE);
                    tvProfileTypeTitle.setVisibility(View.VISIBLE);
                    tvPin.setVisibility(View.VISIBLE);

                    edtName.setVisibility(View.GONE);
                    edtProfileTypeTitle.setVisibility(View.GONE);
                    edtPin.setVisibility(View.GONE);
                    tvChangeProfilePhoto.setVisibility(View.GONE);

                } else {
                    isEditOn = true;
                    tvEdit.setText("SAVE");

                    tvName.setVisibility(View.GONE);
                    tvProfileTypeTitle.setVisibility(View.GONE);
                    tvPin.setVisibility(View.GONE);

                    edtName.setVisibility(View.VISIBLE);
                    edtProfileTypeTitle.setVisibility(View.VISIBLE);
                    edtPin.setVisibility(View.VISIBLE);
                    tvChangeProfilePhoto.setVisibility(View.VISIBLE);

                }


            }
        });

        edtPin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length() >= 4){
                    UserService.getInstance(getContext()).checkIfPinHasNotBeenChosenByPreviousUsers(staffWorker.worksAtRestaurantId, editable.toString(), new CanPinBeMadeInterface() {
                        @Override
                        public void pinAvailable() {
                            Log.d(TAG,"Pin available");
                            tvEdit.setEnabled(true);
                        }

                        @Override
                        public void pinTaken() {
                            edtPin.setError("Pin taken by another user");
                            tvEdit.setEnabled(false);
                        }
                    });
                }
            }
        });


        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Detects request codes
        if ((requestCode == 1888 || requestCode == 1889) && resultCode == Activity.RESULT_OK) {

            // Set image to bitmap
            try {

                // Get image bitmap and set to images
                Bitmap bitmap;
                if (requestCode == 1889) {
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                } else {
                    bitmap = (Bitmap) data.getExtras().get("data");
                }

                civProfilePicture.setImageBitmap(bitmap);

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 80, baos);
                byte[] pictureData = baos.toByteArray();

                float aspectRatio = bitmap.getWidth() / (float) bitmap.getHeight();

                int height = 500;
                int width = Math.round(height * aspectRatio);

                bitmap = Bitmap.createScaledBitmap(
                        bitmap, width, height, false);

                final Handler handler = new Handler();
                final Bitmap finalBitmap = bitmap;
                Runnable runnable = new Runnable() {
                    public void run() {

                        URL img_value = null;
                        Bitmap mIcon1 = null;

                        try {
                            handler.post(new Runnable() {
                                public void run() {
                                    tvChangeProfilePhoto.setVisibility(View.GONE);
                                    civProfilePicture.setImageBitmap(finalBitmap);
                                }
                            });

                        } catch (Exception e) {
                            Log.e("IOException", "onActivityResult");
                            e.printStackTrace();
                        }


                    }
                };
                new Thread(runnable).start();


                FirebaseStorage storage = FirebaseStorage.getInstance();
                // Create a storage reference from our app
                StorageReference storageRef = storage.getReference();
                // Create a reference to profile picture
                final StorageReference photoRef = storageRef.child("images/user_profile_image/" +  staffWorker.id + "/profile_image.jpeg");

                // Upload photo to firebase
                UploadTask uploadTask = photoRef.putBytes(pictureData);
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        Log.e("onActivityResult", "Upload Faliure: " + exception.toString());
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.

                        photoRef.getDownloadUrl()
                                .addOnSuccessListener(new OnSuccessListener<Uri>() {
                                                          @Override
                                                          public void onSuccess(Uri uri) {
                                                              staffWorker.profilePictureURL = uri.toString();
                                                              StaffService.getInstance(getContext()).updateStaffWorker(staffWorker.worksAtRestaurantId, staffWorker, new UpdateStaffWorkerInterface() {
                                                                  @Override
                                                                  public void onUpdated() {

                                                                  }

                                                                  @Override
                                                                  public void onError() {

                                                                  }
                                                              });

                                                              Log.d("onActivityResult", "Upload SUCCESS: ");
                                                          }
                                                      }
                                );


                        Log.d("onActivityResult", "Upload SUCCESS: ");

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


}
