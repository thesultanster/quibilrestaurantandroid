package com.food.quibilrestaurant.ui.feature.qrcodemanager.dialogs;


import com.food.quibilrestaurant.models.QRCode;

/**
 * Created by sultankhan on 8/1/17.
 */

public interface AddNewQRCodeInterface {
    void userEnteredTableId(QRCode qrCode);
}
