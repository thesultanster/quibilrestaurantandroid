package com.food.quibilrestaurant.ui.feature.staffeditor.lists;

import android.content.Context;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.food.quibilrestaurant.R;
import com.food.quibilrestaurant.interfaces.OnStaffWorkerSelectedInterface;
import com.food.quibilrestaurant.models.StaffWorker;

import java.util.Collections;
import java.util.List;

public class StaffWorkersRecyclerAdapter extends RecyclerView.Adapter<StaffWorkersRecyclerAdapter.MyViewHolder> {

    // emptyList takes care of null pointer exception
    List<StaffWorker> staffWorkers = Collections.emptyList();
    LayoutInflater inflator;
    Context context;
    OnStaffWorkerSelectedInterface onStaffWorkerSelectedInterface;
    // Start with first item selected
    private int focusedItem = -1;


    public StaffWorkersRecyclerAdapter(Context context, List<StaffWorker> staffWorkers, OnStaffWorkerSelectedInterface onStaffWorkerSelectedInterface) {
        this.context = context;
        inflator = LayoutInflater.from(context);
        this.staffWorkers = staffWorkers;
        this.onStaffWorkerSelectedInterface = onStaffWorkerSelectedInterface;

        //onMenuCategorySelectedInterface.onCategorySelected(0,menuCategories.get(0));
    }

    public void addRow(StaffWorker staffWorker) {
        staffWorkers.add(staffWorker);
        notifyItemInserted(getItemCount() - 1);
    }

    public void clear() {
        focusedItem = -1;
        staffWorkers.clear();
        notifyDataSetChanged();
    }

    // Called when the recycler view needs to create a new row
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        final View view = inflator.inflate(R.layout.row_staff, parent, false);
        final MyViewHolder holder = new MyViewHolder(view, context, new MyViewHolder.MyViewHolderClicks() {

            public void rowClick(View caller, int position) {
                Log.d("rowClick", "rowClicks");
                // Set selected postition
                focusedItem = position;

                // Pass selected category down the interface
                onStaffWorkerSelectedInterface.onCategorySelected(position, staffWorkers.get(position));

                // Update rows
                Handler handler = new Handler();
                final Runnable r = new Runnable() {
                    public void run() {
                        notifyDataSetChanged(); // notify adapter
                    }
                };
                handler.post(r);

            }

            @Override
            public void deleteItem(View caller, int position) {
                onStaffWorkerSelectedInterface.onUserWantsToDeleteStaff(position, staffWorkers.get(position));
            }


        });
        return holder;
    }

    // Setting up the data for each row
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        if(focusedItem == position){
            holder.rlRow.setBackgroundColor(ContextCompat.getColor(context, R.color.md_grey_200));
        } else {
            holder.rlRow.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
        }

        StaffWorker current = staffWorkers.get(position);
        holder.tvCategoryTitle.setText(current.name);


    }


    @Override
    public int getItemCount() {
        return staffWorkers.size();
    }

    // Created my custom view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvCategoryTitle;
        ImageView ivDelete;
        RelativeLayout rlRow;
        Context context;


        public MyViewHolderClicks mListener;

        // itemView will be my own custom layout View of the row
        public MyViewHolder(View itemView, Context context, MyViewHolderClicks listener) {
            super(itemView);
            mListener = listener;
            this.context = context;

            tvCategoryTitle = (TextView) itemView.findViewById(R.id.tvCategoryTitle);
            ivDelete = (ImageView) itemView.findViewById(R.id.ivDelete);
            rlRow = (RelativeLayout) itemView.findViewById(R.id.rlRow);

            itemView.setOnClickListener(this);
            ivDelete.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivDelete:
                    rlRow.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                    mListener.deleteItem(v, getAdapterPosition());
                    break;
                default:
                    rlRow.setBackgroundColor(ContextCompat.getColor(context, R.color.md_grey_200));
                    mListener.rowClick(v, getAdapterPosition());
                    break;
            }
        }

        public interface MyViewHolderClicks {
            void rowClick(View caller, int position);
            void deleteItem(View caller, int position);
        }
    }


}