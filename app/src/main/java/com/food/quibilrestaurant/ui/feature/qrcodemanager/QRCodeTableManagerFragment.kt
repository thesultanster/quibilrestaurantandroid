package com.food.quibilrestaurant.ui.feature.qrcodemanager

import android.app.ProgressDialog
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.food.quibilrestaurant.R
import com.food.quibilrestaurant.activities.BaseFragment
import com.food.quibilrestaurant.data_layer.CurrentRestaurant
import com.food.quibilrestaurant.models.QRCode
import com.food.quibilrestaurant.ui.feature.qrcodemanager.dialogs.AddNewQRCodeInterface
import com.food.quibilrestaurant.ui.feature.qrcodemanager.dialogs.AddQRCodeDialog
import com.food.quibilrestaurant.ui.feature.qrcodemanager.dialogs.UpdateQRCodeDialog
import com.food.quibilrestaurant.ui.feature.qrcodemanager.dialogs.UpdateQRCodeInterface
import com.food.quibilrestaurant.ui.feature.qrcodemanager.lists.QRCodeListRecyclerAdapter
import com.google.firebase.firestore.FirebaseFirestore
import com.google.zxing.BarcodeFormat
import com.google.zxing.Result
import kotlinx.android.synthetic.main.fragment_qrcode_table_manager.*
import me.dm7.barcodescanner.zxing.ZXingScannerView
import java.util.ArrayList
import kotlinx.android.synthetic.main.fragment_qrcode_table_manager.view.*


internal class QRCodeTableManagerFragment : BaseFragment(), QRCodeTableContract.View, ZXingScannerView.ResultHandler {
    private val TAG: String = "QRTableManagerFragment"

    private var isCameraOn: Boolean = false

    private lateinit var qrCodeTableManagerPresenter: QRCodeTableManagerPresenter
    private lateinit var rvQrCodeListRecyclerAdapter: QRCodeListRecyclerAdapter
    private lateinit var mScannerView: ZXingScannerView
    private lateinit var btnStartCamera: Button
    private lateinit var btnStopCamera: Button
    private lateinit var progressDialog: ProgressDialog

    fun newInstance(): QRCodeTableManagerFragment {
        val fragment = QRCodeTableManagerFragment()
        val arguments = Bundle()
        fragment.arguments = arguments

        return fragment
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        qrCodeTableManagerPresenter = QRCodeTableManagerPresenter(this)
        qrCodeTableManagerPresenter.onCreate()

        rvQrCodeListRecyclerAdapter = QRCodeListRecyclerAdapter(activity, ArrayList<QRCode>(), qrCodeTableManagerPresenter)


        var currentRestaurant = CurrentRestaurant.getInstance(context).cache
        qrCodeTableManagerPresenter.fetchQRCodes(currentRestaurant.id)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_qrcode_table_manager, container, false)

        mScannerView = view.mScannerView
        btnStopCamera = view.btnStopCamera
        btnStartCamera = view.btnStartCamera

        // Setup QR Code Scanner
        val formats = ArrayList<BarcodeFormat>()
        formats.add(BarcodeFormat.QR_CODE)
        mScannerView.setFormats(formats)


        view.btnStartCamera.setOnClickListener {
            startCamera()
        }
        view.btnStopCamera.setOnClickListener {
            stopCamera()
        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Setup adapter and propagate click events to presenter
        rvQrCodes.layoutManager = LinearLayoutManager(activity)
        rvQrCodes.adapter = rvQrCodeListRecyclerAdapter

    }

    override fun onStart() {
        super.onStart()

        if (checkCameraPermission()) {
            startCamera()
        } else {

            // Permissions results will be propogated to our presenter
            requestCameraPermission(qrCodeTableManagerPresenter)
        }

        qrCodeTableManagerPresenter.onStart()
    }

    override fun startCamera() {

        if (!isCameraOn) {
            // Scanner results will be propagated to our presenter
            mScannerView.setResultHandler(this)
            mScannerView.startCamera()

            isCameraOn = true
        }

    }

    override fun stopCamera() {
        if (isCameraOn) {
            mScannerView.removeAllViews()
            mScannerView.stopCamera()

            isCameraOn = false
        }
    }

    override fun handleResult(result: Result) {
        val scanString = result.text
        val qrTAG = scanString.substring(0, 7)


        if (qrTAG != "QUIBIL:") run {
            Toast.makeText(context, "This QR Code is not acceptable", Toast.LENGTH_LONG).show()
            stopCamera()
        } else {

            val qrCodeId = scanString.substring(7).trim()

            //TODO: Create QR Code Repository you fuck
            FirebaseFirestore.getInstance()
                    .collection("qrcodes").document(qrCodeId).get()
                    .addOnSuccessListener { queryDocumentSnapshots ->
                        var qrCode: QRCode? = queryDocumentSnapshots.toObject(QRCode::class.java)
                        stopCamera()


                        // If the QR Code is unregistered
                        if (qrCode!!.restaurantId == "") {
                            Log.d(TAG, "QR Code is unregistered")
                            showAddQrCodeDialog(qrCode);
                        } else {

                            // If the QR Code is from a different restaurant
                            // Then let user know it can't be registered to us
                            if (qrCode.restaurantId != CurrentRestaurant.getInstance(context).cache.id) {
                                Log.d(TAG, "QR Code is from a different restaurant")
                                Toast.makeText(context, "QR Code Registered to a differnt Restaurant", Toast.LENGTH_LONG).show()
                            }

                            // If the QR Code is already registered to our restaurant
                            // Then edit the QR Code
                            if (qrCode.restaurantId == CurrentRestaurant.getInstance(context).cache.id) {
                                Log.d(TAG, "QR Code is already registered to our restaurant")
                                Toast.makeText(context, "QR Code is already registered to your Restaurant", Toast.LENGTH_LONG).show()
                            }
                        }


                    }
        }
    }

    override fun showAddQrCodeDialog(qrCode: QRCode){
        // Show dialog with interface when they select a title for the new category.
        val newFragment = AddQRCodeDialog.newInstance( qrCode,  object: AddNewQRCodeInterface {
            override fun userEnteredTableId(qrCode: QRCode) {
                Log.d(TAG, "User Entered TableId: ${qrCode.tableId}")

                qrCode.restaurantId = CurrentRestaurant.getInstance(context).cache.id

                showDialog("Registering QR Code")

                //TODO: Refactor this in its own repository you fuck
                FirebaseFirestore.getInstance()
                        .collection("qrcodes").document(qrCode.id).set(qrCode)
                        .addOnSuccessListener { queryDocumentSnapshots ->
                            Log.d(TAG, "qr register success")


                            FirebaseFirestore.getInstance()
                                    .collection("restaurants").document(qrCode.restaurantId)
                                    .collection("QRCodes").document(qrCode.id).set(qrCode)
                                    .addOnSuccessListener { queryDocumentSnapshots ->
                                        Log.d(TAG, "qr saved in restaurant success")

                                        addAQRCodeToList(qrCode)
                                        dismissDialog()
                                    }
                                    .addOnFailureListener( { exception ->
                                        Log.d(TAG, "qr saved in restaurant fail")
                                        dismissDialog()
                                    })



                        }
                        .addOnFailureListener( { exception ->
                            Log.d(TAG, "qr register fail")
                            dismissDialog()
                        })


            }


        })
        newFragment.show(fragmentManager!!, "dialog")
    }


    override fun showUpdateQrCodeDialog(qrCode: QRCode){
        // Show dialog with interface when they select a title for the new category.
        val newFragment = UpdateQRCodeDialog.newInstance( qrCode,  object: UpdateQRCodeInterface {
            override fun userEnteredTableId(qrCode: QRCode) {
                Log.d(TAG, "User Entered TableId: ${qrCode.tableId}")

                qrCode.restaurantId = CurrentRestaurant.getInstance(context).cache.id

                showDialog("Updating QR Code")

                //TODO: Refactor this in its own repository you fuck
                FirebaseFirestore.getInstance()
                        .collection("qrcodes").document(qrCode.id).set(qrCode)
                        .addOnSuccessListener { queryDocumentSnapshots ->
                            Log.d(TAG, "qr update success")


                            FirebaseFirestore.getInstance()
                                    .collection("restaurants").document(qrCode.restaurantId)
                                    .collection("QRCodes").document(qrCode.id).set(qrCode)
                                    .addOnSuccessListener { queryDocumentSnapshots ->
                                        Log.d(TAG, "qr saved in restaurant success")

                                        qrCodeTableManagerPresenter.fetchQRCodes(qrCode.restaurantId)
                                        dismissDialog()
                                    }
                                    .addOnFailureListener( { exception ->
                                        Log.e(TAG, "qr saved in restaurant fail", exception)
                                        dismissDialog()
                                    })



                        }
                        .addOnFailureListener( { exception ->
                            Log.e(TAG, "qr update fail", exception)
                            dismissDialog()
                        })


            }

            override fun userWantsToDeleteQRCode(qrCode: QRCode) {
                showDialog("Deleting QR Code")

                val restaurantId: String = qrCode.restaurantId
                qrCode.tableId = ""
                qrCode.restaurantId = ""


                FirebaseFirestore.getInstance()
                        .collection("qrcodes").document(qrCode.id).set(qrCode)
                        .addOnSuccessListener { queryDocumentSnapshots ->
                            Log.d(TAG, "qr delete success")


                            FirebaseFirestore.getInstance()
                                    .collection("restaurants").document(restaurantId)
                                    .collection("QRCodes").document(qrCode.id).delete()
                                    .addOnSuccessListener { queryDocumentSnapshots ->
                                        Log.d(TAG, "qr delete in restaurant success")

                                        qrCodeTableManagerPresenter.fetchQRCodes(restaurantId)
                                        dismissDialog()
                                    }
                                    .addOnFailureListener( { exception ->
                                        Log.e(TAG, "qr delete in restaurant fail", exception)
                                        dismissDialog()
                                    })



                        }
                        .addOnFailureListener( { exception ->
                            Log.e(TAG, "qr delete fail", exception)
                            dismissDialog()
                        })
            }
        })
        newFragment.show(fragmentManager!!, "dialog")
    }

    override fun showDialog(title: String){
        progressDialog = ProgressDialog(activity)
        progressDialog.setMessage(title)
        progressDialog.setCancelable(false)
        progressDialog.show()
    }

    override fun dismissDialog() {
        if (progressDialog != null && progressDialog.isShowing){
            progressDialog.dismiss()
        }
    }

    override fun addAQRCodeToList(qrCode: QRCode) {
        rvQrCodeListRecyclerAdapter.addRow(qrCode)
    }

    override fun clearQRList() {
        rvQrCodeListRecyclerAdapter.clear()
    }
}
