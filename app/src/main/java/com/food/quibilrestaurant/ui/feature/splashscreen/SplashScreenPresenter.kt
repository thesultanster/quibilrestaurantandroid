package com.food.quibilrestaurant.ui.feature.splashscreen

import android.content.Context
import android.util.Log
import com.food.quibilrestaurant.data_layer.AppState
import com.food.quibilrestaurant.data_layer.CurrentRestaurant
import com.food.quibilrestaurant.data_layer.CurrentUser
import com.food.quibilrestaurant.interfaces.FetchRestaurantInterface
import com.food.quibilrestaurant.interfaces.FetchUserInterface
import com.food.quibilrestaurant.models.Restaurant
import com.food.quibilrestaurant.models.User
import com.food.quibilrestaurant.network_layer.RestaurantService
import com.google.firebase.auth.FirebaseAuth

class SplashScreenPresenter(var splashScreenView: SplashScreenContract.View)
    : SplashScreenContract.Presenter {

    internal var TAG = "SplashScreen"
    var mAuth: FirebaseAuth = FirebaseAuth.getInstance();

    override fun checkAuthStatus(context: Context) {
        if (mAuth.currentUser == null) {
            Log.d(TAG, "No user logged in")
            splashScreenView.goToSignUpScreen()
        } else {
           fetchUser(context)
        }
    }

    private fun fetchUser(context: Context){
        // Get latest user and cache them
        CurrentUser.getInstance(context).fetch(object : FetchUserInterface {
            override fun onDataFetched(user: User) {
                Log.d(TAG, "CurrentUser.fetch:onDataFetched: " + user.toString())
                getRestaurant(context, user)
            }

            override fun onUserDataNotFound() {
                Log.d(TAG, "fetch:onUserDataNotFound")
                splashScreenView.goToMainActivity()
            }
        })
    }

    private fun getRestaurant(context: Context, user: User){
        RestaurantService.getInstance(context).getRestaurantFromDatabase(user.ownsRestaurant, object : FetchRestaurantInterface {
            override fun onDataFetched(restaurant: Restaurant) {
                Log.d(TAG, "getRestaurantFromDatabase:onDataFetched")
                // Cache new restaurant as current
                CurrentRestaurant.getInstance(context).cacheRestaurant(restaurant)

                // If manager is onboarded then go to main activity, otherwise go to onboarding
                if (AppState.getInstance(context).isManagerStaffAccountCreated) {
                    splashScreenView.goToMainActivity()
                } else {
                    splashScreenView.goToLockScreen()
                }

            }

            override fun onError() {
                Log.e(TAG, "getRestaurantFromDatabase:onError")
            }
        })
    }


}