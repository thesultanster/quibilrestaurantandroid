package com.food.quibilrestaurant.ui.feature.ordermanager.dialogs


interface ChargeClientInterface {
    fun chargeClient()
    fun printReceipt()
    fun printAndCharge()
}
