package com.food.quibilrestaurant.ui.feature.qrcodemanager

import com.food.quibilrestaurant.models.QRCode

interface QRCodeTableContract {

    interface View {
        fun startCamera()
        fun stopCamera()
        fun showAddQrCodeDialog(qrCode: QRCode)
        fun showUpdateQrCodeDialog(qrCode: QRCode)
        fun showDialog(title: String)
        fun dismissDialog()
        fun addAQRCodeToList(qrCode: QRCode)
        fun clearQRList()

    }

    interface Presenter {
        fun onCreate()
        fun onStart()
        fun fetchQRCodes(restaurantId: String)
    }
}