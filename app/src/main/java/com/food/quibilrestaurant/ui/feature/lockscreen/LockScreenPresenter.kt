package com.food.quibilrestaurant.ui.feature.lockscreen

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.Toast
import com.food.quibilrestaurant.activities.MainActivity
import com.food.quibilrestaurant.data_layer.AppState
import com.food.quibilrestaurant.data_layer.CurrentRestaurant
import com.food.quibilrestaurant.data_layer.CurrentUser
import com.food.quibilrestaurant.interfaces.FetchRestaurantInterface
import com.food.quibilrestaurant.interfaces.FetchUserInterface
import com.food.quibilrestaurant.models.LockscreenCredential
import com.food.quibilrestaurant.models.Restaurant
import com.food.quibilrestaurant.models.User
import com.food.quibilrestaurant.network_layer.RestaurantService
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthException
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class LockScreenPresenter(var lockscreenView: LockScreenContract.View)
    : LockScreenContract.Presenter {

    internal var TAG = "LockScreen"

    override fun getCredentials(restaurantId: String, pin: String, context: Context) {
        Log.d(TAG, "getCredentials: RestaurantId: " + restaurantId + " Pin: " + pin)

        //TODO: Move this method to Staff Service
        FirebaseDatabase.getInstance().reference
                .child("restaurant_pin_user_relation")
                .child(restaurantId)
                .child(pin).addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        val credential = dataSnapshot.getValue(LockscreenCredential::class.java)

                        lockscreenView.hideProgressBar()

                        // If no id found
                        if (credential == null) {
                            lockscreenView.showSnackbar("No user found for pin code")
                        } else {
                            Log.d(TAG, "User ID: " + credential.id + "")

                            // Cache mode and staff id
                            if (credential.profileType == "STAFF") {
                                AppState.getInstance(context).cacheAppMode(credential.profileType)
                                AppState.getInstance(context).cacheLockscreenCredential(credential)
                            } else {
                                AppState.getInstance(context).cacheAppMode(credential.profileType)
                                AppState.getInstance(context).cacheLockscreenCredential(credential)
                            }

                            // Although misleading variable name, this will also being used to see if the manager has logged their pin after Signing into the app
                            AppState.getInstance(context).cacheIsManagerStaffAccountCreated(true)
                            lockscreenView.goToMainActivity()

                        }
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                        Log.e(TAG, databaseError.message)
                        lockscreenView.showSnackbar("Database error")
                    }
                })
    }


}