package com.food.quibilrestaurant.ui.feature.login

import android.content.Context
import android.util.Log
import com.food.quibilrestaurant.data_layer.CurrentRestaurant
import com.food.quibilrestaurant.data_layer.CurrentUser
import com.food.quibilrestaurant.interfaces.FetchRestaurantInterface
import com.food.quibilrestaurant.interfaces.FetchUserInterface
import com.food.quibilrestaurant.models.Restaurant
import com.food.quibilrestaurant.models.User
import com.food.quibilrestaurant.network_layer.RestaurantService
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthException

class LoginPresenter(var loginView: LoginContract.View)
    : LoginContract.Presenter {

    internal var TAG = "Login"
    var mAuth: FirebaseAuth = FirebaseAuth.getInstance();

    override fun signIn(email: String, password: String, context: Context, activity: LoginActivity) {

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(activity) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInWithEmail:success")
                        val user = mAuth.currentUser

                        // Cache UID and Email right so the current user service can use a cached uid
                        val currentUser = User()
                        currentUser.id = user!!.uid
                        currentUser.email = user.email
                        CurrentUser.getInstance(context).cacheUser(currentUser)

                        fetchUser(context)

                    } else {
                        loginView.dismissProgressDialog()

                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "signInWithEmail:failure", task.exception)
                        if (task.exception != null) {
                            val errorCode = (task.exception as FirebaseAuthException).errorCode
                            loginView.consumeError(errorCode)
                        }

                    }
                }
    }

    private fun fetchUser(context: Context) {
        // Fetch user data and cache it
        CurrentUser.getInstance(context).fetch(object : FetchUserInterface {
            override fun onDataFetched(user: User) {
                Log.d(TAG, "fetchUser: " + user.toString())
                CurrentUser.getInstance(context).cacheUser(user)
                getRestaurant(context, user)
            }

            override fun onUserDataNotFound() {
                Log.d(TAG, "fetchUser: No User Data")
                loginView.dismissProgressDialog()
                loginView.goToSignUpScreen()
            }
        })
    }

    private fun getRestaurant(context: Context, user: User){
        RestaurantService.getInstance(context).getRestaurantFromDatabase(user.ownsRestaurant, object : FetchRestaurantInterface {
            override fun onDataFetched(restaurant: Restaurant) {
                Log.d(TAG, "getRestaurantFromDatabase:onDataFetched: " + restaurant.toString())
                // Cache new restaurant as current
                CurrentRestaurant.getInstance(context).cacheRestaurant(restaurant)
                loginView.dismissProgressDialog()
                loginView.goToLockScreen()

            }

            override fun onError() {
                Log.e(TAG, "getRestaurantFromDatabase:onError")
                loginView.dismissProgressDialog()
                loginView.goToSignUpScreen()
            }
        })
    }


}