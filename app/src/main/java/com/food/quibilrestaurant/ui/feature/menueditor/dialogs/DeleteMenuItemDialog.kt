package com.food.quibilrestaurant.ui.feature.menueditor.dialogs

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import com.food.quibilrestaurant.R
import kotlinx.android.synthetic.main.dialog_update_qr_code.view.*

/**
 * Created by sultankhan on 7/28/18.
 */

class DeleteMenuItemDialog : DialogFragment() {

    private lateinit var deleteMenuItemInterface: DeleteMenuItemInterface

    // This is a workaround for the strange behavior of onCreateView (which doesn't show dialog's layout)
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialogBuilder = AlertDialog.Builder(context!!)
        val inflater = LayoutInflater.from(context)
        val dialogView = inflater.inflate(R.layout.dialog_delete_menu_item, null)
        dialogBuilder.setView(dialogView)

        initSubViews(dialogView)

        populateSubViews()

        isCancelable = false

        return dialogBuilder.create()
    }


    private fun initSubViews(rootView: View) {



        rootView.btnCancel.setOnClickListener { dialog.dismiss() }

        rootView.tvDelete.setOnClickListener {
            deleteMenuItemInterface.userWantsToDeleteMenuItem()
            dialog.dismiss()
        }


    }

    private fun populateSubViews() {

    }

    companion object {


        fun newInstance( deleteMenuItemInterface: DeleteMenuItemInterface): DeleteMenuItemDialog {

            val frag = DeleteMenuItemDialog()
            var arguments = Bundle()
            frag.deleteMenuItemInterface = deleteMenuItemInterface
            frag.arguments = arguments
            return frag
        }
    }
}
