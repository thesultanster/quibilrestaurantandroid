package com.food.quibilrestaurant.ui.feature.ordermanager.lists;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.food.quibilrestaurant.R;
import com.food.quibilrestaurant.interfaces.OnOrderDeliveredSelectedInterface;
import com.food.quibilrestaurant.models.MenuItem;
import com.food.quibilrestaurant.models.OrderedItem;
import com.food.quibilrestaurant.models.Session;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class ItemsDeliveredRecyclerAdapter extends RecyclerView.Adapter<ItemsDeliveredRecyclerAdapter.MyViewHolder> {

    // emptyList takes care of null pointer exception
    List<Session> data = Collections.emptyList();
    LayoutInflater inflator;
    Context context;
    OnOrderDeliveredSelectedInterface onOrderDeliveredSelectedInterface;
    // Start with first item selected
    private int focusedItem = -1;


    public ItemsDeliveredRecyclerAdapter(Context context, List<Session> data, OnOrderDeliveredSelectedInterface onOrderDeliveredSelectedInterface) {
        this.context = context;
        inflator = LayoutInflater.from(context);
        this.data = data;
        this.onOrderDeliveredSelectedInterface = onOrderDeliveredSelectedInterface;
    }

    public void addRow(Session waitingForOrder) {
        data.add(waitingForOrder);
        notifyItemInserted(getItemCount() - 1);
    }

    public void clear(){
        data = new ArrayList<>();
        notifyDataSetChanged();
    }


    // Called when the recycler view needs to create a new row
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        final View view = inflator.inflate(R.layout.row_items_acknowledged, parent, false);
        final MyViewHolder holder = new MyViewHolder(view, context, new MyViewHolder.MyViewHolderClicks() {

            public void rowClick(View caller, int position) {
                Log.d("rowClick", "rowClicks");
                // Set selected postition
                focusedItem = position;

                // Pass selected item down the interface
                onOrderDeliveredSelectedInterface.onSelected(position, data.get(position));


            }

            @Override
            public void checkoutUserAndPrint(View caller, int position) {
                onOrderDeliveredSelectedInterface.onRestaurantWantsToChecksUserOut(position, data.get(position));
            }

        });
        return holder;
    }

    // Setting up the data for each row
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Session current = data.get(position);

        holder.tvName.setText(current.userFullName);
        holder.btnAction.setText("CHARGE");
        holder.tvTable.setText(current.tableId);

        DateFormat formatter = new SimpleDateFormat("hh:mm a", Locale.US);
        formatter.setTimeZone(TimeZone.getDefault());
        holder.tvTime.setText(formatter.format(current.updatedAt));

        if(current.orders != null) {
            String orders = "";
            for (OrderedItem orderedItem : current.orders) {
                orders += orderedItem.name + "\n";
            }
            holder.tvItemsOrdered.setText(orders);
        } else {
            holder.tvItemsOrdered.setText("No Items");
        }


        NumberFormat n = NumberFormat.getCurrencyInstance(Locale.US);
        String s = n.format(current.totalPaymentAmount / 100.0);
        holder.tvPrice.setText(s);

    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    // Created my custom view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvName;
        TextView tvTable;
        TextView tvTime;
        TextView tvItemsOrdered;
        TextView tvPrice;
        Button btnAction;
        Context context;


        public MyViewHolderClicks mListener;

        // itemView will be my own custom layout View of the row
        public MyViewHolder(View itemView, Context context, MyViewHolderClicks listener) {
            super(itemView);
            mListener = listener;
            this.context = context;

            tvName = itemView.findViewById(R.id.tvName);
            tvTable = itemView.findViewById(R.id.tvTable);
            tvTime = itemView.findViewById(R.id.tvTime);
            tvItemsOrdered = itemView.findViewById(R.id.tvItemsOrdered);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            btnAction = itemView.findViewById(R.id.btnAction);

            itemView.setOnClickListener(this);
            btnAction.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btnAction:
                    mListener.checkoutUserAndPrint(v, getAdapterPosition());
                    break;
                default:
                    mListener.rowClick(v, getAdapterPosition());
                    break;
            }
        }

        public interface MyViewHolderClicks {
            void rowClick(View caller, int position);
            void checkoutUserAndPrint(View caller, int position);
        }
    }


}