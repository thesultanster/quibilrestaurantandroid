package com.food.quibilrestaurant.ui.feature.menueditor.lists;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.food.quibilrestaurant.R;
import com.food.quibilrestaurant.interfaces.OnMenuItemSelectedInterface;
import com.food.quibilrestaurant.models.MenuItem;

import java.util.Collections;
import java.util.List;

public class MenuItemsRecyclerAdapter extends RecyclerView.Adapter<MenuItemsRecyclerAdapter.MyViewHolder> {

    // emptyList takes care of null pointer exception
    List<MenuItem> menuItems = Collections.emptyList();
    LayoutInflater inflator;
    Context context;
    View prevView = null;
    // Start with first item selected
    private int focusedItem = -1;

    OnMenuItemSelectedInterface onMenuItemSelectedInterface;


    public MenuItemsRecyclerAdapter(Context context, List<MenuItem> menuItems, OnMenuItemSelectedInterface onMenuItemSelectedInterface) {
        this.context = context;
        this.onMenuItemSelectedInterface = onMenuItemSelectedInterface;
        inflator = LayoutInflater.from(context);
        this.menuItems = menuItems;
    }

    public void addRow(MenuItem menuItem) {
        menuItems.add(menuItem);
        notifyItemInserted(getItemCount() - 1);
    }

    public void clearRows() {
        focusedItem = -1;
        menuItems.clear();
        notifyDataSetChanged();
    }

    public void updateRow(MenuItem menuItem, int position){
        menuItems.set(position, menuItem);
        notifyItemChanged(position);
    }

    // Called when the recycler view needs to create a new row
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        final View view = inflator.inflate(R.layout.row_menu_item, parent, false);
        final MyViewHolder holder = new MyViewHolder(view, context, new MyViewHolder.MyViewHolderClicks() {

            public void rowClick(View caller, int position) {
                Log.d("rowClick", "rowClicks");
                // Set selected postition
                focusedItem = position;

                // Pass selected category down the interface
                onMenuItemSelectedInterface.onItemSelected(position, menuItems.get(position));


                notifyDataSetChanged(); // notify adapter


            }

            @Override
            public void deleteItem(View caller, int position) {
                onMenuItemSelectedInterface.onUserWantsToDeleteItem(position, menuItems.get(position));
            }


        });
        return holder;
    }

    // Setting up the data for each row
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        MenuItem current = menuItems.get(position);

        if (focusedItem == position) {
            holder.rlRow.setBackgroundColor(ContextCompat.getColor(context, R.color.md_grey_200));
        } else {
            holder.rlRow.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
        }

        holder.tvCategoryTitle.setText(current.name);
    }


    @Override
    public int getItemCount() {
        return menuItems.size();
    }

    // Created my custom view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvCategoryTitle;
        ImageView ivDelete;
        RelativeLayout rlRow;
        Context context;

        public MyViewHolderClicks mListener;

        // itemView will be my own custom layout View of the row
        public MyViewHolder(View itemView, Context context, MyViewHolderClicks listener) {
            super(itemView);
            this.mListener = listener;
            this.context = context;

            tvCategoryTitle = (TextView) itemView.findViewById(R.id.tvCategoryTitle);
            ivDelete = (ImageView) itemView.findViewById(R.id.ivDelete);
            rlRow = (RelativeLayout) itemView.findViewById(R.id.rlRow);

            itemView.setOnClickListener(this);
            ivDelete.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivDelete:
                    rlRow.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                    mListener.deleteItem(v, getAdapterPosition());
                    break;
                default:
                    rlRow.setBackgroundColor(ContextCompat.getColor(context, R.color.md_grey_200));
                    mListener.rowClick(v, getAdapterPosition());
                    break;
            }
        }

        public interface MyViewHolderClicks {
            void rowClick(View caller, int position);
            void deleteItem(View caller, int position);
        }
    }


}