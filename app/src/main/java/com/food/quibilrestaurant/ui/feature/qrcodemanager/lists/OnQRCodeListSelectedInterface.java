package com.food.quibilrestaurant.ui.feature.qrcodemanager.lists;


import com.food.quibilrestaurant.models.QRCode;

public interface OnQRCodeListSelectedInterface {
    void onSelected(int position, QRCode qrCode);
    void onEditQrCode(int position, QRCode qrCode);
}
