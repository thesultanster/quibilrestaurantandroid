package com.food.quibilrestaurant.ui.feature.menueditor;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.food.quibilrestaurant.R;
import com.food.quibilrestaurant.adapters.AddOnItemsRecyclerAdapter;
import com.food.quibilrestaurant.data_layer.CurrentRestaurant;
import com.food.quibilrestaurant.dialogs.AddAddonItemDialog;
import com.food.quibilrestaurant.dialogs.UpdateAddonItemDialog;
import com.food.quibilrestaurant.interfaces.CreateAddonItemInterface;
import com.food.quibilrestaurant.interfaces.FetchRestaurantItemAddOnsInterface;
import com.food.quibilrestaurant.interfaces.OnAddOnItemSelectedInterface;
import com.food.quibilrestaurant.interfaces.UpdateAddonItemInterface;
import com.food.quibilrestaurant.interfaces.UpdateMenuItemInterface;
import com.food.quibilrestaurant.interfaces.UserEnteredNewAddonItemInterface;
import com.food.quibilrestaurant.models.AddOnItem;
import com.food.quibilrestaurant.models.MenuItem;
import com.food.quibilrestaurant.models.Restaurant;
import com.food.quibilrestaurant.network_layer.MenuService;
import com.food.quibilrestaurant.utils.NumberTextWatcher;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.rey.material.widget.EditText;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.net.URL;
import java.util.ArrayList;


public class MenuItemFragment extends Fragment {
    String TAG = "MenuItemFragment";

    TextView tvEdit;
    TextView tvItemName;
    TextView tvDescription;
    TextView tvPrice;
    TextView tvChangeProfilePhoto;
    TextView tvTax;
    TextView tvAddAddOnItem;
    EditText edtItemName;
    EditText edtDescription;
    android.widget.EditText edtPrice;
    EditText edtTax;
    ImageView ivItemPhoto;
    ImageView ivAddPhoto;

    byte[] pictureData;
    boolean isEditOn = false;
    RecyclerView rvAddOnItems;
    AddOnItemsRecyclerAdapter addOnItemsRecyclerAdapter;
    Typeface openFace;

    Restaurant restaurant;
    MenuItem currentItem;


    public MenuItemFragment() {
        // Required empty public constructor
    }

    public static MenuItemFragment newInstance(MenuItem currentItem) {
        MenuItemFragment frag = new MenuItemFragment();
        Bundle args = new Bundle();
        args.putSerializable("currentItem", currentItem);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        openFace = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Light.ttf");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_menu_item, container, false);
        restaurant = CurrentRestaurant.getInstance(getContext()).getCache();
        currentItem = (MenuItem) getArguments().getSerializable("currentItem");


        rvAddOnItems = view.findViewById(R.id.rvAddOnItems);
        tvAddAddOnItem = view.findViewById(R.id.tvAddAddOnItem);
        tvTax = view.findViewById(R.id.tvTax);
        tvItemName = (TextView) view.findViewById(R.id.tvItemName);
        tvDescription = (TextView) view.findViewById(R.id.tvDescription);
        tvEdit = (TextView) view.findViewById(R.id.tvEdit);
        tvPrice = (TextView) view.findViewById(R.id.tvPrice);
        tvChangeProfilePhoto = (TextView) view.findViewById(R.id.tvChangeProfilePhoto);
        edtTax = view.findViewById(R.id.edtTax);
        edtItemName = (EditText) view.findViewById(R.id.edtItemName);
        edtDescription = (EditText) view.findViewById(R.id.edtDescription);
        edtPrice = view.findViewById(R.id.edtPrice);
        ivAddPhoto = view.findViewById(R.id.ivAddPhoto);
        ivItemPhoto = view.findViewById(R.id.ivItemPhoto);


        edtPrice.addTextChangedListener(new NumberTextWatcher(edtPrice));

        tvChangeProfilePhoto.setVisibility(View.GONE);

        tvChangeProfilePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkPermission()) {
                    takePhotoOrChoose();
                } else {
                    requestPermission();
                }

            }
        });
        if (currentItem.itemPhotoUrl != null && !currentItem.itemPhotoUrl.equals("")) {
            Picasso.with(getContext()).load(currentItem.itemPhotoUrl).into(ivItemPhoto);
            ivAddPhoto.setVisibility(View.GONE);
        }




        tvItemName.setText(currentItem.name);
        tvDescription.setText(currentItem.description);
        tvPrice.setText("$" +   String.format("%.2f", currentItem.price / 100.0));
        tvTax.setText("" + currentItem.taxRate);
        edtItemName.setText(currentItem.name);
        edtDescription.setText(currentItem.description);
        edtPrice.setText("" + currentItem.price);
        edtTax.setText("" + currentItem.taxRate);

        tvEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isEditOn) {
                    isEditOn = false;
                    tvEdit.setText("EDIT");


                    tvItemName.setText(edtItemName.getText().toString());
                    tvDescription.setText(edtDescription.getText().toString());
                    tvPrice.setText(edtPrice.getText().toString());
                    tvTax.setText(edtTax.getText().toString());

                    currentItem.name = edtItemName.getText().toString();
                    currentItem.description = edtDescription.getText().toString();
                    currentItem.price = (int)(((Double.parseDouble(edtPrice.getText().toString().replace("$", "")))) * 100);
                    currentItem.taxRate = Double.valueOf(edtTax.getText().toString());

                    MenuService.getInstance(getContext()).updateMenuItem(currentItem, new UpdateMenuItemInterface() {
                        @Override
                        public void onUpdated() {
                            Log.d(TAG, "updateMenuItem:onUpdated");
                        }

                        @Override
                        public void onError() {
                            Log.e(TAG, "Error");
                        }
                    });


                    tvItemName.setVisibility(View.VISIBLE);
                    tvDescription.setVisibility(View.VISIBLE);
                    tvPrice.setVisibility(View.VISIBLE);
                    tvTax.setVisibility(View.VISIBLE);

                    edtItemName.setVisibility(View.GONE);
                    edtDescription.setVisibility(View.GONE);
                    edtPrice.setVisibility(View.GONE);
                    edtTax.setVisibility(View.GONE);
                    tvChangeProfilePhoto.setVisibility(View.GONE);

                } else {
                    isEditOn = true;
                    tvEdit.setText("SAVE");

                    tvItemName.setVisibility(View.GONE);
                    tvDescription.setVisibility(View.GONE);
                    tvPrice.setVisibility(View.GONE);
                    tvTax.setVisibility(View.GONE);

                    edtItemName.setVisibility(View.VISIBLE);
                    edtDescription.setVisibility(View.VISIBLE);
                    edtPrice.setVisibility(View.VISIBLE);
                    edtTax.setVisibility(View.VISIBLE);
                    tvChangeProfilePhoto.setVisibility(View.VISIBLE);
                }


            }
        });

        addOnItemsRecyclerAdapter = new AddOnItemsRecyclerAdapter(getContext(), new ArrayList<AddOnItem>(), new OnAddOnItemSelectedInterface() {

            @Override
            public void onEditItem(final int position, final AddOnItem addOnItem) {
                // Show dialog with interface when they select a title for the new category.
                DialogFragment newFragment = UpdateAddonItemDialog.newInstance(addOnItem, new UserEnteredNewAddonItemInterface() {
                    @Override
                    public void userEnteredNewAddonItem(final AddOnItem addOnItem) {
                        MenuService.getInstance(getContext()).updateAddonItemToMenu(currentItem.restaurantId, currentItem.categoryId, addOnItem, new UpdateAddonItemInterface() {
                            @Override
                            public void onUpdated() {

                                addOnItemsRecyclerAdapter.updateRow(addOnItem, position);
                            }

                            @Override
                            public void onError() {

                            }
                        });
                    }

                });
                newFragment.show(getFragmentManager(), "dialog");
            }
        });
        rvAddOnItems.setLayoutManager(new LinearLayoutManager(getContext()));
        rvAddOnItems.setAdapter(addOnItemsRecyclerAdapter);


        MenuService.getInstance(getContext()).getItemAddOns(currentItem.restaurantId, currentItem.categoryId, currentItem.id, new FetchRestaurantItemAddOnsInterface() {
            @Override
            public void onDataFetched(AddOnItem addOnItem) {
                addOnItemsRecyclerAdapter.addRow(addOnItem);
            }

            @Override
            public void onError() {

            }
        });


        // User selects add item button so we ope up the dialog
        tvAddAddOnItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Show dialog with interface when they select a title for the new category.
                DialogFragment newFragment = AddAddonItemDialog.newInstance(currentItem, new UserEnteredNewAddonItemInterface() {
                    @Override
                    public void userEnteredNewAddonItem(final AddOnItem addOnItem) {
                        MenuService.getInstance(getContext()).addAddonItemToMenu(currentItem.restaurantId, currentItem.categoryId, addOnItem, new CreateAddonItemInterface() {
                            @Override
                            public void onCreated() {
                                addOnItemsRecyclerAdapter.addRow(addOnItem);
                            }

                            @Override
                            public void onError() {

                            }
                        });
                    }

                });
                newFragment.show(getFragmentManager(), "dialog");
            }
        });


        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        // Detects request codes
        if ((requestCode == 1888 || requestCode == 1889) && resultCode == Activity.RESULT_OK) {

            // Set image to bitmap
            try {

                // Get image bitmap and set to images
                Bitmap bitmap;
                if(requestCode == 1889 ){
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                } else {
                    bitmap = (Bitmap) data.getExtras().get("data");
                }

                ivItemPhoto.setImageBitmap(bitmap);

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 80, baos);
                pictureData = baos.toByteArray();

                float aspectRatio = bitmap.getWidth() / (float) bitmap.getHeight();

                int height = 500;
                int width = Math.round(height * aspectRatio);

                bitmap = Bitmap.createScaledBitmap(
                        bitmap, width, height, false);

                final Handler handler = new Handler();
                final Bitmap finalBitmap = bitmap;
                Runnable runnable = new Runnable() {
                    public void run() {

                        URL img_value = null;
                        Bitmap mIcon1 = null;

                        try {
                            handler.post(new Runnable() {
                                public void run() {
                                    ivItemPhoto.setImageBitmap(finalBitmap);
                                }
                            });

                        } catch (Exception e) {
                            Log.e("IOException", "onActivityResult");
                            e.printStackTrace();
                        }


                    }
                };
                new Thread(runnable).start();


                FirebaseStorage storage = FirebaseStorage.getInstance();
                // Create a storage reference from our app
                StorageReference storageRef = storage.getReference();
                // Create a reference to profile picture
                final StorageReference photoRef = storageRef.child("images/menu_item_image/" + currentItem.id + "/original.jpeg");

                // Upload photo to firebase
                UploadTask uploadTask = photoRef.putBytes(pictureData);
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        Log.e("onActivityResult", "Upload Faliure: " + exception.toString());
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.

                        photoRef.getDownloadUrl()
                                .addOnSuccessListener(new OnSuccessListener<Uri>() {
                                                          @Override
                                                          public void onSuccess(Uri uri) {
                                                              currentItem.itemPhotoUrl = uri.toString();
                                                              MenuService.getInstance(getContext()).updateMenuItem(currentItem, new UpdateMenuItemInterface() {
                                                                  @Override
                                                                  public void onUpdated() {

                                                                  }

                                                                  @Override
                                                                  public void onError() {
                                                                      Log.e(TAG, "Error");
                                                                  }
                                                              });
                                                          }
                                                      }
                                );


                        Log.d("onActivityResult", "Upload SUCCESS: ");

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


    private void takePhotoOrChoose(){



        new AlertDialog.Builder(getActivity())
                .setMessage("Select Destination")
                .setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // Request to take a photo
                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, 1888);
                    }
                })
                .setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto , 1889);
                    }
                })
                .create()
                .show();


    }


    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(getActivity(),
                new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                12345);
    }



    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(getActivity())
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }
}
