package com.food.quibilrestaurant.ui.feature.qrcodemanager.dialogs


import com.food.quibilrestaurant.models.QRCode

interface UpdateQRCodeInterface {
    fun userEnteredTableId(qrCode: QRCode)
    fun userWantsToDeleteQRCode(qrCode: QRCode)
}
