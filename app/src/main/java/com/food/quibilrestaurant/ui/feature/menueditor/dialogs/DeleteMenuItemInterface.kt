package com.food.quibilrestaurant.ui.feature.menueditor.dialogs


import com.food.quibilrestaurant.models.QRCode

interface DeleteMenuItemInterface {
    fun userWantsToDeleteMenuItem()
}
