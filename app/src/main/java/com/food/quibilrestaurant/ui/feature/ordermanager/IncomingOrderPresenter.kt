package com.food.quibilrestaurant.ui.feature.ordermanager

import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.util.Log
import android.widget.Toast
import com.food.quibilrestaurant.data_layer.printer.PrinterManager
import com.food.quibilrestaurant.data_layer.printer.PrinterManagerInterface
import com.food.quibilrestaurant.domain.ordermanager.IndividualOrderedItem
import com.food.quibilrestaurant.domain.ordermanager.OrderManagerUseCase
import com.food.quibilrestaurant.interfaces.*
import com.food.quibilrestaurant.models.BraintreeCustomer
import com.food.quibilrestaurant.models.Restaurant
import com.food.quibilrestaurant.models.Session
import com.food.quibilrestaurant.network_layer.OrderPrintedInterface
import com.food.quibilrestaurant.network_layer.RestaurantService
import com.food.quibilrestaurant.network_layer.UserService
import com.food.quibilrestaurant.ui.feature.ordermanager.lists.ClientsWhoHaveNotOrderedYetRecyclerAdapter
import com.google.firebase.functions.FirebaseFunctionsException

class IncomingOrderPresenter(var incomingOrderView: IncomingOrderContract.View) : IncomingOrderContract.Presenter {
    val TAG = "IncomingOrderPresenter"
    var orderManagerUseCase = OrderManagerUseCase()

    override fun onStart(printerManager: PrinterManager) {
        // Naive way of knowing if we are connected to a printer. It will mostly assume yes
        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        val pairedDevices = mBluetoothAdapter.bondedDevices
        var notConnected = true
        for (bluetoothDevice in pairedDevices) {
            if (printerManager.getPrinterName() == bluetoothDevice.name) {
                incomingOrderView.setConnectedToPrinter(printerManager.getPrinterName())
                notConnected = false
            }
        }

        if (notConnected) {
            incomingOrderView.setNotConnectedToPrinter()
        }

//        printerManager.isConnected(object : PrinterConnectionnterface {
//            override fun cannotFindPrinterInListOfBluetoothDevices() {
//                incomingOrderView.setNotConnectedToPrinter()
//            }
//
//            override fun isConnected() {
//                incomingOrderView.setConnectedToPrinter(printerManager.getPrinterName())
//            }
//
//            override fun isDisconnected() {
//                incomingOrderView.setNotConnectedToPrinter()
//            }
//
//
//        })
    }

    override fun checkoutClient(context: Context, session: Session, currentRestaurant: Restaurant, clientsWhoHaveNotOrderedYetRecyclerAdapter: ClientsWhoHaveNotOrderedYetRecyclerAdapter, onCompletionInterface: OnCompletionInterface) {
        incomingOrderView.showDialog("Checking Out User")
        RestaurantService.getInstance(context).checkoutUser(session)
                .addOnCompleteListener { task ->
                    incomingOrderView.dismissDialog()

                    if (!task.isSuccessful) {
                        val e = task.exception
                        if (e is FirebaseFunctionsException) {
                            val ffe = e as FirebaseFunctionsException?
                            val code = ffe!!.code
                            val details = ffe.details
                            Log.e(TAG, "Code: $code\nException: $details", e)

                            onCompletionInterface.onError()

                            incomingOrderView.showToast("API Error")
                        } else {

                            clientsWhoHaveNotOrderedYetRecyclerAdapter.clear()


                            onCompletionInterface.onComplete()

                            // Fetch all categories and populate list
                            RestaurantService.getInstance(context).getCheckedInUsers(currentRestaurant.id, object : FetchCheckedInUsersInterface {
                                override fun onDataFetched(checkedInUser: Session) {
                                    clientsWhoHaveNotOrderedYetRecyclerAdapter.addRow(checkedInUser)
                                }

                                override fun clearCurrentUsers() {
                                    clientsWhoHaveNotOrderedYetRecyclerAdapter.clear()
                                }

                                override fun onError() {

                                }

                                override fun onNoOneCheckedIn() {

                                }
                            })

                            incomingOrderView.showToast("Client Checked Out")
                        }
                    }

                    // ...
                }
    }

    override fun chargeClient(context: Context, session: Session, onCompletionInterface: OnCompletionInterface) {
        incomingOrderView.showDialog("Charging.")

        // Find client braintree id
        UserService.getInstance(context)
                .fetchBraintreeCustomerId(session.clientId, object : FetchBraintreeCustomerIdInterface {

                    var totalPaymentAmount = (session.totalPaymentAmount / 100.0).toString()

                    override fun onFetched(braintreeCustomer: BraintreeCustomer) {

                        incomingOrderView.setDialogTitle("Charging..")
                        // Find client payment token
                        UserService.getInstance(context).testBTFindCustomer(braintreeCustomer.id, object : FindBraintreeCustomerInterface {
                            override fun onFetched(paymentToken: String) {
                                Log.d(TAG, paymentToken)

                                incomingOrderView.setDialogTitle("Charging...")
                                UserService.getInstance(context).getPaymentToken(paymentToken, object : OnPaymentTokenCreatedInterface {
                                    override fun onNoneReceived(paymentNonce: String) {

                                        incomingOrderView.setDialogTitle("Charging.")
                                        UserService.getInstance(context).chargeClient(paymentNonce, totalPaymentAmount, object : OnClientChargedInterface {
                                            override fun onSuccess() {
                                                incomingOrderView.setDialogTitle("Charging..")
                                                incomingOrderView.dismissDialog()
                                                onCompletionInterface.onComplete()
                                            }

                                            override fun onError() {
                                                incomingOrderView.dismissDialog()
                                                incomingOrderView.showToast("Charge Error")
                                                onCompletionInterface.onError()
                                            }
                                        })
                                    }

                                    override fun onError() {
                                        incomingOrderView.dismissDialog()
                                        incomingOrderView.showToast("Fetch Payment Token Error")
                                        onCompletionInterface.onError()
                                    }
                                })
                            }

                            override fun onError() {
                                incomingOrderView.dismissDialog()
                                incomingOrderView.showToast("Find Customer Error")
                                onCompletionInterface.onError()
                            }
                        })
                    }

                    override fun onError() {
                        incomingOrderView.dismissDialog()
                        incomingOrderView.showToast("Fetch Customer ID Error")
                        onCompletionInterface.onError()
                    }
                })

    }

    override fun printReceipt(context: Context, session: Session, onCompletionInterface: OnCompletionInterface) {
        incomingOrderView.showDialog("Printing Receipt")

        val printerManager = PrinterManager()
        printerManager.init(context)
        printerManager.printReceipt(printerManager.getPrinterName()!!, session, object : PrinterManagerInterface {
            override fun cannotFindPrinterInListOfBluetoothDevices() {
                incomingOrderView.dismissDialog()
                incomingOrderView.setNotConnectedToPrinter()
                incomingOrderView.showToast("Printer Issue. Try Connecting in Printer manager")
                onCompletionInterface.onError()
            }

            override fun onPrintSuccess() {
                incomingOrderView.dismissDialog()
                onCompletionInterface.onComplete()
            }

            override fun onPrintError() {
                Log.e(TAG, "onRestaurantWantsToChecksUserOut:onPrintError")
                incomingOrderView.showToast("Printer Error 197")
                incomingOrderView.dismissDialog()
                onCompletionInterface.onError()
            }
        })
    }


    override fun autoPrintItemIfNecessary(context: Context?, session: Session) {
        for (item in session.orders) {
            var individualOrderedItem = orderManagerUseCase.bindIndividualOrderedItem(item, session)
            // Print one item and then break the loop. Because the db changing will call autoPrintItemIfNecessary
            // again. So even though we are printing this only once, all items will be printed eventually
            if(individualOrderedItem.isPrinted == false){
                printIndividualItem(context, individualOrderedItem)
                return
            }

        }

    }

    private fun printIndividualItem(context: Context?, individualOrderedItem: IndividualOrderedItem){
        val printerManager = PrinterManager()
        printerManager.init(context)
        printerManager.printOrder(individualOrderedItem, object : PrinterManagerInterface {
            override fun cannotFindPrinterInListOfBluetoothDevices() {
                Log.d(TAG, "printOrder:cannotFindPrinterInListOfBluetoothDevices")
            }

            override fun onPrintSuccess() {
                Log.d(TAG, "printOrder:onPrintSuccess")
                RestaurantService.getInstance(context)
                        .setMenuItemAsPrinted(individualOrderedItem.item, individualOrderedItem.sessionId, object : OrderPrintedInterface {
                            override fun onStatuSuccesfullyChanged() {
                                Log.d(TAG, "setMenuItemAsPrinted:onStatuSuccesfullyChanged")
                            }

                            override fun onError() {
                                Log.e(TAG, "setMenuItemAsPrinted:onError")
                            }
                        })
            }

            override fun onPrintError() {
                Log.e(TAG, "printOrder:onPrintError")
            }
        })
    }

}