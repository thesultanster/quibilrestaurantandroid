package com.food.quibilrestaurant.ui.feature.ordermanager;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.food.quibilrestaurant.R;
import com.food.quibilrestaurant.interfaces.OnCompletionInterface;
import com.food.quibilrestaurant.ui.feature.ordermanager.lists.ClientsWhoHaveNotOrderedYetRecyclerAdapter;
import com.food.quibilrestaurant.ui.feature.ordermanager.lists.ItemsDeliveredRecyclerAdapter;
import com.food.quibilrestaurant.ui.feature.ordermanager.dialogs.ChargeClientDialog;
import com.food.quibilrestaurant.ui.feature.ordermanager.dialogs.ChargeClientInterface;
import com.food.quibilrestaurant.ui.feature.ordermanager.lists.IndividualItemsRecyclerAdapter;
import com.food.quibilrestaurant.data_layer.CurrentRestaurant;
import com.food.quibilrestaurant.data_layer.printer.PrinterManager;
import com.food.quibilrestaurant.data_layer.printer.PrinterManagerInterface;
import com.food.quibilrestaurant.models.Session;
import com.food.quibilrestaurant.network_layer.OrderPrintedInterface;
import com.food.quibilrestaurant.ui.feature.ordermanager.dialogs.CheckoutClientDialog;
import com.food.quibilrestaurant.interfaces.FetchCheckedInUsersInterface;
import com.food.quibilrestaurant.interfaces.FetchUsersOrderDeliverednterface;
import com.food.quibilrestaurant.interfaces.FetchUsersWaitingForOrderInterface;
import com.food.quibilrestaurant.interfaces.OnCheckedInUserSelectedInterface;
import com.food.quibilrestaurant.interfaces.OnOrderDeliveredSelectedInterface;
import com.food.quibilrestaurant.interfaces.OnWaitingForOrderSelectedInterface;
import com.food.quibilrestaurant.domain.ordermanager.IndividualOrderedItem;
import com.food.quibilrestaurant.models.Restaurant;
import com.food.quibilrestaurant.network_layer.RestaurantService;
import com.food.quibilrestaurant.ui.feature.ordermanager.dialogs.CheckoutClientInterface;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;


public class IncomingOrderFragment extends Fragment implements IncomingOrderContract.View {
    String TAG = "IncomingOrderFragment";

    RecyclerView rvClientsWhoHaveNotOrderedYet;
    RecyclerView rvWaitingForOrder;
    RecyclerView rvDeliveredOrders;
    ClientsWhoHaveNotOrderedYetRecyclerAdapter clientsWhoHaveNotOrderedYetRecyclerAdapter;
    IndividualItemsRecyclerAdapter individualItemsRecyclerAdapter;
    ItemsDeliveredRecyclerAdapter itemsDeliveredRecyclerAdapter;

    ProgressDialog progressDialog;

    ImageView civPrinterStatus;
    TextView tvPrinterStatus;

    Typeface openFace;
    Restaurant currentRestaurant;

    PrinterManager printerManager;

    IncomingOrderContract.Presenter incomingOrderPresenter;


    public IncomingOrderFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        incomingOrderPresenter = new IncomingOrderPresenter(this);

        openFace = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Light.ttf");
        clientsWhoHaveNotOrderedYetRecyclerAdapter = new ClientsWhoHaveNotOrderedYetRecyclerAdapter(getContext(), new ArrayList<Session>(), new OnCheckedInUserSelectedInterface() {
            @Override
            public void onActionSelected(int position, final Session session) {
                DialogFragment newFragment = CheckoutClientDialog.Companion.newInstance(session, new CheckoutClientInterface() {

                    @Override
                    public void checkoutUser() {
                        incomingOrderPresenter.checkoutClient(getContext(), session, currentRestaurant, clientsWhoHaveNotOrderedYetRecyclerAdapter, new OnCompletionInterface() {
                            @Override
                            public void onComplete() {

                            }

                            @Override
                            public void onError() {

                            }
                        });
                    }
                });
                newFragment.show(getFragmentManager(), "CheckoutClientDialog");
            }

            @Override
            public void onSelected(int position, Session session) {

            }
        });

        individualItemsRecyclerAdapter = new IndividualItemsRecyclerAdapter(getContext(), new ArrayList<IndividualOrderedItem>(), new OnWaitingForOrderSelectedInterface() {

            @Override
            public void onSelected(int position, IndividualOrderedItem individualOrderedItem) {

            }

            @Override
            public void onOrderNeedsToBePrinted(int position, final IndividualOrderedItem individualOrderedItem) {
                showDialog("Printing Order");

                PrinterManager printerManager = new PrinterManager();
                printerManager.init(getContext());
                printerManager.printOrder(individualOrderedItem, new
                        PrinterManagerInterface() {
                            @Override
                            public void cannotFindPrinterInListOfBluetoothDevices() {
                                dismissDialog();
                                setNotConnectedToPrinter();
                                Toast.makeText(getActivity(), "Printer Issue. Try Connecting in Printer manager", Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onPrintSuccess() {
                                dismissDialog();

                                RestaurantService.getInstance(getContext())
                                        .setMenuItemAsPrinted(individualOrderedItem.item
                                                , individualOrderedItem.sessionId
                                                , new OrderPrintedInterface() {
                                                    @Override
                                                    public void onStatuSuccesfullyChanged() {
                                                        Log.e(TAG, "onPrintSuccess:onStatuSuccesfullyChanged");
                                                    }

                                                    @Override
                                                    public void onError() {
                                                        Log.e(TAG, "onPrintSuccess:onError");
                                                    }
                                                });
                            }

                            @Override
                            public void onPrintError() {
                                Log.e(TAG, "onOrderNeedsToBePrinted:onPrintError");
                                Toast.makeText(getActivity(), "Printer Error 197", Toast.LENGTH_LONG).show();
                                dismissDialog();
                            }
                        });

            }
        });

        itemsDeliveredRecyclerAdapter = new ItemsDeliveredRecyclerAdapter(getContext(), new ArrayList<Session>(), new OnOrderDeliveredSelectedInterface() {
            @Override
            public void onSelected(int position, Session session) {

            }

            @Override
            public void onRestaurantWantsToChecksUserOut(int position, final Session session) {


                DialogFragment newFragment = ChargeClientDialog.Companion.newInstance(session, new ChargeClientInterface() {

                    @Override
                    public void printAndCharge() {
                        incomingOrderPresenter.printReceipt(getContext(), session, new OnCompletionInterface() {
                            @Override
                            public void onComplete() {
                                incomingOrderPresenter.chargeClient(getContext(), session, new OnCompletionInterface() {
                                    @Override
                                    public void onComplete() {

                                        incomingOrderPresenter.checkoutClient(getContext(), session, currentRestaurant, clientsWhoHaveNotOrderedYetRecyclerAdapter, new OnCompletionInterface() {
                                            @Override
                                            public void onComplete() {

                                            }

                                            @Override
                                            public void onError() {

                                            }
                                        });
                                    }

                                    @Override
                                    public void onError() {

                                    }
                                });
                            }

                            @Override
                            public void onError() {

                            }
                        });
                    }

                    @Override
                    public void printReceipt() {
                        incomingOrderPresenter.printReceipt(getContext(), session, new OnCompletionInterface() {
                            @Override
                            public void onComplete() {

                            }

                            @Override
                            public void onError() {

                            }
                        });
                    }

                    @Override
                    public void chargeClient() {

                        incomingOrderPresenter.chargeClient(getContext(), session, new OnCompletionInterface() {
                            @Override
                            public void onComplete() {

                                incomingOrderPresenter.checkoutClient(getContext(), session, currentRestaurant, clientsWhoHaveNotOrderedYetRecyclerAdapter, new OnCompletionInterface() {
                                    @Override
                                    public void onComplete() {

                                    }

                                    @Override
                                    public void onError() {

                                    }
                                });
                            }

                            @Override
                            public void onError() {

                            }
                        });
                    }
                });
                newFragment.show(getFragmentManager(), "ChargeClientDialog");

            }
        });


        currentRestaurant = CurrentRestaurant.getInstance(getContext()).getCache();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_incoming_order, container, false);

        rvClientsWhoHaveNotOrderedYet = view.findViewById(R.id.rvClientsWhoHaveNotOrderedYet);
        rvWaitingForOrder = view.findViewById(R.id.rvWaitingForOrder);
        rvDeliveredOrders = view.findViewById(R.id.rvDeliveredOrders);
        civPrinterStatus = view.findViewById(R.id.civPrinterStatus);
        tvPrinterStatus = view.findViewById(R.id.tvPrinterStatus);

        rvClientsWhoHaveNotOrderedYet.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvClientsWhoHaveNotOrderedYet.setAdapter(clientsWhoHaveNotOrderedYetRecyclerAdapter);

        rvWaitingForOrder.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvWaitingForOrder.setAdapter(individualItemsRecyclerAdapter);

        rvDeliveredOrders.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvDeliveredOrders.setAdapter(itemsDeliveredRecyclerAdapter);

        rvClientsWhoHaveNotOrderedYet.setNestedScrollingEnabled(false);
        rvWaitingForOrder.setNestedScrollingEnabled(false);
        rvDeliveredOrders.setNestedScrollingEnabled(false);





        return view;
    }


    @Override
    public void showDialog(String title) {
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage(title);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void setDialogTitle(String title) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.setTitle(title);
        }
    }

    @Override
    public void dismissDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void showToast(@NotNull String title) {
        Toast.makeText(getActivity(), title, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStart() {
        super.onStart();
        printerManager = new PrinterManager();
        printerManager.init(getContext());
        incomingOrderPresenter.onStart(printerManager);

        // Fetch all categories and populate list
        RestaurantService.getInstance(getContext()).getCheckedInUsers(currentRestaurant.id, new FetchCheckedInUsersInterface() {
            @Override
            public void onDataFetched(Session checkedInUser) {
                clientsWhoHaveNotOrderedYetRecyclerAdapter.addRow(checkedInUser);
            }

            @Override
            public void clearCurrentUsers() {
                clientsWhoHaveNotOrderedYetRecyclerAdapter.clear();
            }

            @Override
            public void onError() {

            }

            @Override
            public void onNoOneCheckedIn() {

            }
        });

        // Fetch all individual ordered items
        RestaurantService.getInstance(getContext()).getUsersWaitingForOrderUsers(currentRestaurant.id, new FetchUsersWaitingForOrderInterface() {
            @Override
            public void onDataFetched(Session session) {
                individualItemsRecyclerAdapter.addRow(session);
                incomingOrderPresenter.autoPrintItemIfNecessary(getContext(), session);
            }

            @Override
            public void clearCurrentOrders() {
                individualItemsRecyclerAdapter.clear();
            }

            @Override
            public void onError() {

            }

            @Override
            public void noPendingOrders() {

            }
        });

        // Fetch all categories and populate list
        RestaurantService.getInstance(getContext()).getUsersOrderedDelivered(currentRestaurant.id, new FetchUsersOrderDeliverednterface() {
            @Override
            public void onDataFetched(Session session) {
                itemsDeliveredRecyclerAdapter.addRow(session);
            }

            @Override
            public void clearDeliveredOrders() {
                itemsDeliveredRecyclerAdapter.clear();
            }

            @Override
            public void onError() {

            }

            @Override
            public void noPendingOrders() {

            }
        });

    }

    @Override
    public void onStop() {
        super.onStop();
        printerManager.blockOperation();
        RestaurantService.getInstance(getContext()).removeListeners();
    }

    @Override
    public void setNotConnectedToPrinter() {
        tvPrinterStatus.setText("Not Connected to any Printer. Go to Printer Manager");
        civPrinterStatus.setVisibility(View.VISIBLE);
    }

    @Override
    public void setConnectedToPrinter(@NotNull String name) {
        tvPrinterStatus.setText("Connected to " + name);
        civPrinterStatus.setVisibility(View.GONE);
    }



}
