package com.food.quibilrestaurant.ui.feature.menueditor.dialogs


import com.food.quibilrestaurant.models.QRCode

interface DeleteStaffInterface {
    fun userWantsToDeleteStaff()
}
