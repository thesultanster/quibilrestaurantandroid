package com.food.quibilrestaurant.ui.feature.ordermanager.lists;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.food.quibilrestaurant.R;
import com.food.quibilrestaurant.interfaces.OnCheckedInUserSelectedInterface;
import com.food.quibilrestaurant.models.Session;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class ClientsWhoHaveNotOrderedYetRecyclerAdapter extends RecyclerView.Adapter<ClientsWhoHaveNotOrderedYetRecyclerAdapter.MyViewHolder> {

    // emptyList takes care of null pointer exception
    List<Session> data = Collections.emptyList();
    LayoutInflater inflator;
    Context context;
    OnCheckedInUserSelectedInterface onCheckedInUserSelectedInterface;
    // Start with first item selected
    private int focusedItem = -1;


    public ClientsWhoHaveNotOrderedYetRecyclerAdapter(Context context, List<Session> data, OnCheckedInUserSelectedInterface onCheckedInUserSelectedInterface) {
        this.context = context;
        inflator = LayoutInflater.from(context);
        this.data = data;
        this.onCheckedInUserSelectedInterface = onCheckedInUserSelectedInterface;

        //onMenuCategorySelectedInterface.onCategorySelected(0,menuCategories.get(0));
    }

    public void addRow(Session checkedInUser) {
        data.add(checkedInUser);
        notifyItemInserted(getItemCount() - 1);
    }

    public void clear(){
        data = new ArrayList<>();
        notifyDataSetChanged();
    }


    // Called when the recycler view needs to create a new row
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        final View view = inflator.inflate(R.layout.row_session, parent, false);
        final MyViewHolder holder = new MyViewHolder(view, context, new MyViewHolder.MyViewHolderClicks() {

            @Override
            public void actionClick(View caller, int position) {
                onCheckedInUserSelectedInterface.onActionSelected(position, data.get(position));
            }

            public void rowClick(View caller, int position) {
                Log.d("rowClick", "rowClicks");
                // Set selected position
                focusedItem = position;

                // Pass selected category down the interface
                onCheckedInUserSelectedInterface.onSelected(position, data.get(position));


            }


        });
        return holder;
    }

    // Setting up the data for each row
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Session current = data.get(position);


        holder.tvName.setText(current.userFullName);
        holder.tvState.setText("NOT ORDERED YET");
        holder.tvTable.setText(current.tableId);

        DateFormat formatter = new SimpleDateFormat("hh:mm a", Locale.US);
        formatter.setTimeZone(TimeZone.getDefault());
        holder.tvTime.setText(formatter.format(current.createdAt));

        if(current.userProfilePicURL != null && !current.userProfilePicURL.equals("")) {
            holder.ivProfilePicture.setVisibility(View.VISIBLE);
            Picasso.with(context).load(current.userProfilePicURL).into(holder.ivProfilePicture);
        } else {
            holder.ivProfilePicture.setVisibility(View.GONE);
        }
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    // Created my custom view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView ivProfilePicture;
        TextView tvName;
        TextView tvTable;
        TextView tvTime;
        TextView tvItemsOrdered;
        TextView tvPrice;
        TextView tvState;
        Button btnAction;
        Context context;


        public MyViewHolderClicks mListener;

        // itemView will be my own custom layout View of the row
        public MyViewHolder(View itemView, Context context, MyViewHolderClicks listener) {
            super(itemView);
            mListener = listener;
            this.context = context;

            ivProfilePicture = itemView.findViewById(R.id.ivProfilePicture);
            tvName = itemView.findViewById(R.id.tvName);
            tvTable = itemView.findViewById(R.id.tvTable);
            tvTime = itemView.findViewById(R.id.tvTime);
            tvItemsOrdered = itemView.findViewById(R.id.tvItemsOrdered);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            tvState = itemView.findViewById(R.id.tvState);
            btnAction = itemView.findViewById(R.id.btnAction);


            tvPrice.setVisibility(View.GONE);
            tvItemsOrdered.setVisibility(View.GONE);

            btnAction.setText("Check Out");

            btnAction.setOnClickListener(this);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btnAction:
                    mListener.actionClick(v, getAdapterPosition());
                    break;
                default:
                    mListener.rowClick(v, getAdapterPosition());
                    break;
            }
        }

        public interface MyViewHolderClicks {
            void actionClick(View caller, int position);
            void rowClick(View caller, int position);
        }
    }


}