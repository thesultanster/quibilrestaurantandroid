package com.food.quibilrestaurant.ui.feature.qrcodemanager

import android.util.Log
import com.food.quibilrestaurant.activities.RequestPermissionsInterface
import com.food.quibilrestaurant.models.QRCode
import com.food.quibilrestaurant.ui.feature.qrcodemanager.lists.OnQRCodeListSelectedInterface
import com.google.firebase.firestore.FirebaseFirestore

class QRCodeTableManagerPresenter(
        var qrCodeTableView: QRCodeTableContract.View)
    : QRCodeTableContract.Presenter, RequestPermissionsInterface, OnQRCodeListSelectedInterface {

    private val TAG = "QRTableManagerPresenter"

    override fun onCreate() {
    }

    override fun onStart() {

    }

    override fun fetchQRCodes(restaurantId: String) {
        FirebaseFirestore.getInstance()
                .collection("restaurants").document(restaurantId)
                .collection("QRCodes").get()
                .addOnSuccessListener { queryDocumentSnapshots ->
                    Log.d(TAG, "got restaurant's qr codes")
                    qrCodeTableView.clearQRList()
                    for(qrSnap in queryDocumentSnapshots){
                        val qrCode = qrSnap.toObject(QRCode::class.java) as QRCode
                        qrCodeTableView.addAQRCodeToList(qrCode)
                    }

                }
                .addOnFailureListener( { exception ->
                    Log.d(TAG, "fetch restauarant's qr codes fail")
                    qrCodeTableView.dismissDialog()
                })
    }



    override fun onCameraPermissionsGranted() {
        qrCodeTableView.startCamera()
    }

    override fun onCameraPermissionsRejected() {
    }

    override fun onSelected(position: Int, qrCode: QRCode) {
    }

    override fun onEditQrCode(position: Int, qrCode: QRCode) {
        qrCodeTableView.showUpdateQrCodeDialog(qrCode)
    }




}