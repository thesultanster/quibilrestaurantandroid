package com.food.quibilrestaurant.ui.feature.ordermanager.dialogs


interface CheckoutClientInterface {
    fun checkoutUser()
}
