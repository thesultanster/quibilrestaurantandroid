package com.food.quibilrestaurant.ui.feature.printermanager.lists;


import android.bluetooth.BluetoothDevice;

import com.food.quibilrestaurant.models.Printer;

public interface OnBluetoothDeviceSelectedInterface {
    void onSelected(int position, BluetoothDevice printer);
}
