package com.food.quibilrestaurant.ui.feature.ordermanager.lists;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.food.quibilrestaurant.R;
import com.food.quibilrestaurant.domain.ordermanager.OrderManagerUseCase;
import com.food.quibilrestaurant.interfaces.OnWaitingForOrderSelectedInterface;
import com.food.quibilrestaurant.domain.ordermanager.IndividualOrderedItem;
import com.food.quibilrestaurant.models.OrderedItem;
import com.food.quibilrestaurant.models.Session;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class IndividualItemsRecyclerAdapter extends RecyclerView.Adapter<IndividualItemsRecyclerAdapter.MyViewHolder> {

    // emptyList takes care of null pointer exception
    List<IndividualOrderedItem> data = Collections.emptyList();
    LayoutInflater inflator;
    Context context;
    OnWaitingForOrderSelectedInterface onWaitingForOrderSelectedInterface;
    // Start with first item selected
    private int focusedItem = -1;


    public IndividualItemsRecyclerAdapter(Context context, List<IndividualOrderedItem> data, OnWaitingForOrderSelectedInterface onWaitingForOrderSelectedInterface) {
        this.context = context;
        inflator = LayoutInflater.from(context);
        this.data = data;
        this.onWaitingForOrderSelectedInterface = onWaitingForOrderSelectedInterface;
    }

    public void addRow(Session session) {

        if (session.orders != null) {
            OrderManagerUseCase orderManagerUseCase = new OrderManagerUseCase();
            for (OrderedItem item : session.orders) {
                IndividualOrderedItem individualOrderedItem = orderManagerUseCase.bindIndividualOrderedItem(item, session);
                data.add(individualOrderedItem);
                notifyDataSetChanged();
            }
        }

    }

    public void clear() {
        data = new ArrayList<>();
        notifyDataSetChanged();
    }

    // Called when the recycler view needs to create a new row
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        final View view = inflator.inflate(R.layout.row_individual_ordered_item, parent, false);
        final MyViewHolder holder = new MyViewHolder(view, context, new MyViewHolder.MyViewHolderClicks() {

            public void rowClick(View caller, int position) {
                Log.d("rowClick", "rowClicks");
                // Set selected postition
                focusedItem = position;

                // Pass selected item down the interface
                onWaitingForOrderSelectedInterface.onSelected(position, data.get(position));


            }

            @Override
            public void printOrder(View caller, int position) {
                IndividualOrderedItem individualOrderedItem = data.get(position);
                onWaitingForOrderSelectedInterface.onOrderNeedsToBePrinted(position, individualOrderedItem);
                ((TextView) caller).setText("PRINT AGAIN");
            }
        });
        return holder;
    }

    // Setting up the data for each row
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        IndividualOrderedItem current = data.get(position);

        if(current.isPrinted){
            holder.vNotificationBar.setVisibility(View.GONE);
        } else {
            holder.vNotificationBar.setVisibility(View.VISIBLE);
        }

        holder.tvName.setText(current.userFullName);
        holder.tvState.setText(current.item.state);
        holder.tvTable.setText(current.tableId);

        DateFormat formatter = new SimpleDateFormat("hh:mm a", Locale.US);
        formatter.setTimeZone(TimeZone.getDefault());
        holder.tvTime.setText(formatter.format(current.orderedAt));

        holder.tvItemsOrdered.setText(current.item.name);

        NumberFormat n = NumberFormat.getCurrencyInstance(Locale.US);
        String s = n.format(current.item.price / 100.0);
        holder.tvPrice.setText(s);

    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    // Created my custom view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvName;
        TextView tvTable;
        TextView tvTime;
        TextView tvItemsOrdered;
        TextView tvPrice;
        TextView tvState;
        Button btnPrintOrder;
        Context context;
        View vNotificationBar;


        public MyViewHolderClicks mListener;

        // itemView will be my own custom layout View of the row
        public MyViewHolder(View itemView, Context context, MyViewHolderClicks listener) {
            super(itemView);
            mListener = listener;
            this.context = context;

            vNotificationBar = itemView.findViewById(R.id.vNotificationBar);
            tvName = itemView.findViewById(R.id.tvName);
            tvTable = itemView.findViewById(R.id.tvTable);
            tvTime = itemView.findViewById(R.id.tvTime);
            tvState = itemView.findViewById(R.id.tvState);
            tvItemsOrdered = itemView.findViewById(R.id.tvItemsOrdered);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            btnPrintOrder = itemView.findViewById(R.id.btnPrintOrder);

            itemView.setOnClickListener(this);
            btnPrintOrder.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btnPrintOrder:
                    mListener.printOrder(v, getAdapterPosition());
                    break;
                default:
                    mListener.rowClick(v, getAdapterPosition());
                    break;
            }
        }

        public interface MyViewHolderClicks {
            void rowClick(View caller, int position);
            void printOrder(View caller, int position);
        }
    }


}