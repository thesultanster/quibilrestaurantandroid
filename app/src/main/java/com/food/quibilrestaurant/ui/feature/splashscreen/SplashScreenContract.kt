package com.food.quibilrestaurant.ui.feature.splashscreen

import android.content.Context

interface SplashScreenContract {
    interface View {
        fun goToSignUpScreen()
        fun goToLockScreen()
        fun goToMainActivity()
    }

    interface Presenter {
        fun checkAuthStatus(context: Context)
    }
}