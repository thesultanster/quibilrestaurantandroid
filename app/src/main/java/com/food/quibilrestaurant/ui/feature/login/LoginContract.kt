package com.food.quibilrestaurant.ui.feature.login

import android.content.Context

interface LoginContract {
    interface View {
        fun goToLockScreen()
        fun goToSplashScreen()
        fun goToSignUpScreen()
        fun showProgressDialog(title: String, body: String)
        fun dismissProgressDialog()
        fun consumeError(errorCode: String)
    }

    interface Presenter {
        fun signIn(email: String, password: String, context: Context, activity: LoginActivity)
    }
}