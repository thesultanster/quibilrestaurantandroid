package com.food.quibilrestaurant.ui.feature.ordermanager

import android.content.Context
import com.food.quibilrestaurant.data_layer.printer.PrinterManager
import com.food.quibilrestaurant.interfaces.OnCompletionInterface
import com.food.quibilrestaurant.models.Restaurant
import com.food.quibilrestaurant.models.Session
import com.food.quibilrestaurant.ui.feature.ordermanager.lists.ClientsWhoHaveNotOrderedYetRecyclerAdapter

interface IncomingOrderContract {

    interface View {
        fun setNotConnectedToPrinter()
        fun setConnectedToPrinter(name: String?)
        fun showToast(title: String)
        fun setDialogTitle(title: String)
        fun showDialog(title: String)
        fun dismissDialog()
    }

    interface Presenter {
        fun onStart(printerManager: PrinterManager)
        fun checkoutClient(context: Context, session: Session, currentRestaurant: Restaurant,clientsWhoHaveNotOrderedYetRecyclerAdapter: ClientsWhoHaveNotOrderedYetRecyclerAdapter, onCompletionInterface: OnCompletionInterface)
        fun chargeClient(context: Context, session: Session, onCompletionInterface: OnCompletionInterface)
        fun printReceipt(context: Context, session: Session, onCompletionInterface: OnCompletionInterface)
        fun autoPrintItemIfNecessary(context: Context?, session: Session)
    }
}
