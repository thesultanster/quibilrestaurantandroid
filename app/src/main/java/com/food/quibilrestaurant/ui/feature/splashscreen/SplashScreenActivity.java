package com.food.quibilrestaurant.ui.feature.splashscreen;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.food.quibilrestaurant.R;
import com.food.quibilrestaurant.ui.feature.lockscreen.LockScreenActivity;
import com.food.quibilrestaurant.activities.LoginSignUpActivity;
import com.food.quibilrestaurant.activities.MainActivity;
import com.food.quibilrestaurant.utils.App;

public class SplashScreenActivity extends AppCompatActivity implements SplashScreenContract.View {

    SplashScreenPresenter splashScreenPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        App.getmFirebaseAnalytics().setCurrentScreen(this, "SplashScreenActivity", null /* class override */);
        splashScreenPresenter = new SplashScreenPresenter(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        splashScreenPresenter.checkAuthStatus(getApplicationContext());
    }

    @Override
    public void goToSignUpScreen() {
        Intent intent = new Intent(getApplicationContext(), LoginSignUpActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void goToMainActivity() {
        Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
        intent.putExtra("previousActivity","SplashScreenActivity");
        startActivity(intent);
        finish();
    }

    @Override
    public void goToLockScreen() {
        Intent intent = new Intent(SplashScreenActivity.this, LockScreenActivity.class);
        startActivity(intent);
        finish();
    }
}
