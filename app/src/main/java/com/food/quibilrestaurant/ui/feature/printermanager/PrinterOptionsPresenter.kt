package com.food.quibilrestaurant.ui.feature.printermanager

import android.bluetooth.BluetoothAdapter
import com.food.quibilrestaurant.data_layer.printer.PrinterManager
import android.content.Intent
import android.content.BroadcastReceiver
import android.content.Context
import android.widget.CompoundButton
import android.bluetooth.BluetoothDevice
import android.util.Log
import com.food.quibilrestaurant.data_layer.printer.PrinterConnectionnterface
import com.food.quibilrestaurant.ui.feature.printermanager.lists.OnBluetoothDeviceSelectedInterface


internal class PrinterOptionsPresenter(
        var printerManagerView: PrinterOptionsContract.View)
    : PrinterOptionsContract.Presenter,
        OnBluetoothDeviceSelectedInterface,
        CompoundButton.OnCheckedChangeListener,
        BroadcastReceiver() {


    var blockOperation = false


    /******** Contract Functions **********/
    override fun onCreate() {


    }

    override fun onStart(printerManager: PrinterManager) {
        blockOperation = false

        // Check if we are currently connected to our saved printer

//        printerManager.isConnected(object : PrinterConnectionnterface {
//            override fun cannotFindPrinterInListOfBluetoothDevices() {
//                if (!blockOperation) {
//                    printerManagerView.onDeviceDisconnected()
//                }
//            }
//
//            override fun isConnected() {
//                if (!blockOperation) {
//                    printerManagerView.onDeviceConnected(printerManager.getConnectedPrinter())
//                }
//            }
//
//            override fun isDisconnected() {
//                if (!blockOperation) {
//                    printerManagerView.onDeviceDisconnected()
//                }
//            }
//
//
//        })


    }

    override fun onViewCreated() {
        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
            printerManagerView.onBluetoothDisabled()

        } else {
            // Bluetooth disabled
            if (!mBluetoothAdapter.isEnabled) {
                printerManagerView.onBluetoothDisabled()
            } else {
                populateDeviceList()
                printerManagerView.onBluetoothEnabled()
            }
        }
    }

    override fun onDestroy(printerManager: PrinterManager) {
        blockOperation = true;
    }

    // Callback for when a user selects a bluetooth device from the list
    override fun onSelected(position: Int, printer: BluetoothDevice) {

        printerManagerView.selectPrinterAsDefault(printer)
    }

    /******** Callback Functions **********/
// Callback for bluetooth swtich
    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
        setBluetooth(isChecked)
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        val action = intent!!.action
        Log.d("FUCK", action)

        // Bluetooth Setting State
        if (action == BluetoothAdapter.ACTION_STATE_CHANGED) {
            val state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                    BluetoothAdapter.ERROR)
            when (state) {
                BluetoothAdapter.STATE_OFF -> {
                    printerManagerView.onBluetoothDisabled()
                }
                BluetoothAdapter.STATE_TURNING_OFF -> {
                }
                BluetoothAdapter.STATE_ON -> {
                    populateDeviceList()
                    printerManagerView.onBluetoothEnabled()
                }
                BluetoothAdapter.STATE_TURNING_ON -> {
                }
            }


        }


        // Scanning For Bluetooth Devices
        when (action) {
            BluetoothDevice.ACTION_FOUND -> {
                // Discovery has found a device. Get the BluetoothDevice
                // object and its info from the Intent.
                val device: BluetoothDevice = intent.extras[BluetoothDevice.EXTRA_DEVICE] as BluetoothDevice
                printerManagerView.addAvailableBluetoothDevice(device)
            }
            BluetoothDevice.ACTION_ACL_CONNECTED -> {
                val device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE) as BluetoothDevice
                //Device is now connected
                device.createBond()
                printerManagerView.onDeviceConnected(device)
            }
            BluetoothAdapter.ACTION_DISCOVERY_FINISHED -> {
                //Done searching
                printerManagerView.onScanComplete()
            }

            BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED -> {
                //Device is about to disconnect
                printerManagerView.onDeviceMaybeConnecting()
            }
            BluetoothDevice.ACTION_ACL_DISCONNECTED -> {
                printerManagerView.onDeviceDisconnected()
            }
        }
    }

    override fun populateDeviceList() {
        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        val pairedDevices = mBluetoothAdapter.bondedDevices

        for (bluetoothDevice in pairedDevices) {
            printerManagerView.addAvailableBluetoothDevice(bluetoothDevice)
        }
    }

    /******** Helper Functions **********/
    fun setBluetooth(enable: Boolean): Boolean {
        val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        val isEnabled = bluetoothAdapter.isEnabled
        if (enable && !isEnabled) {
            return bluetoothAdapter.enable()
        } else if (!enable && isEnabled) {
            return bluetoothAdapter.disable()
        }
        // No need to change bluetooth state
        return true
    }


}