package com.food.quibilrestaurant.ui.feature.printermanager

import android.annotation.SuppressLint
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.IntentFilter
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.food.quibilrestaurant.R
import com.food.quibilrestaurant.activities.BaseFragment
import com.food.quibilrestaurant.data_layer.printer.PrinterManager
import com.food.quibilrestaurant.data_layer.printer.PrinterManagerInterface
import com.food.quibilrestaurant.ui.feature.printermanager.lists.PrinterOptionsDevicesRecyclerAdapter
import kotlinx.android.synthetic.main.fragment_printer_manager.*


internal class PrinterOptionsFragment : BaseFragment(), PrinterOptionsContract.View {

    private val REQUEST_BT_PAIRING = 1230
    private val TAG = "PrinterOptionsFragment"

    lateinit var printerManagerPresenter: PrinterOptionsPresenter
    lateinit var rvBluetoothAdapter: PrinterOptionsDevicesRecyclerAdapter
    lateinit var printerManager: PrinterManager

    /******** Lifecycle Functions **********/
    fun newInstance(): PrinterOptionsFragment {
        val fragment = PrinterOptionsFragment()
        val arguments = Bundle()
        fragment.arguments = arguments

        return fragment
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        printerManagerPresenter = PrinterOptionsPresenter(this)
        printerManagerPresenter.onCreate()


        rvBluetoothAdapter = PrinterOptionsDevicesRecyclerAdapter(activity, ArrayList<BluetoothDevice>(), printerManagerPresenter)

        printerManager = PrinterManager()
        printerManager.init(context)

    }

    override fun onStart() {
        super.onStart()
        printerManagerPresenter.onStart(printerManager)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_printer_manager, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Setup adapter and propagate click events to presenter
        rvBluetoothDevices.layoutManager = LinearLayoutManager(activity)
        rvBluetoothDevices.adapter = rvBluetoothAdapter
        rvBluetoothDevices.isNestedScrollingEnabled = false

        swToggleBluetooth.setOnCheckedChangeListener(printerManagerPresenter)
        btnRescan.setOnClickListener { rescanBluetooth() }

        var printerManager = PrinterManager()
        printerManager.init(context)
        var printer = printerManager.getConnectedPrinter()
        if (printer == null) {
            onDeviceDisconnected()
        } else {
            onDeviceConnected(printer)
        }

        btnDemoPrint.setOnClickListener {

            printerManager.printTestData(object : PrinterManagerInterface {
                override fun cannotFindPrinterInListOfBluetoothDevices() {
                    onDeviceDisconnected()
                }

                override fun onPrintSuccess() {
                }

                override fun onPrintError() {
                    onDeviceDisconnected()
                }

            })

        }

        printerManagerPresenter.onViewCreated()

    }

    override fun onDestroy() {
        super.onDestroy()
        printerManagerPresenter.onDestroy(printerManager)

    }


    /******** View Contract Functions **********/
    @SuppressLint("ResourceAsColor")
    override fun onBluetoothDisabled() {
        val color = ColorDrawable(resources.getColor(R.color.md_red_400))
        civBluetoothStatus.setImageDrawable(color)
        tvBluetoothStatus.text = "Bluetooth Turned Off"
        swToggleBluetooth.isChecked = false

        btnRescan.visibility = View.GONE
        tvListTitle.visibility = View.GONE
        pbScanning.visibility = View.GONE

        // Clear List of Devices
        rvBluetoothAdapter.clear()
    }

    @SuppressLint("ResourceAsColor")
    override fun onBluetoothEnabled() {
        val color = ColorDrawable(resources.getColor(R.color.md_green_400))
        civBluetoothStatus.setImageDrawable(color)
        tvBluetoothStatus.text = "Bluetooth Turned On"
        swToggleBluetooth.isChecked = true

        btnRescan.visibility = View.VISIBLE

    }

    override fun addAvailableBluetoothDevice(bluetoothDevice: BluetoothDevice) {
        tvListTitle.visibility = View.VISIBLE
        if (bluetoothDevice.name != null) {
            rvBluetoothAdapter.addRow(bluetoothDevice)
        }
    }

    override fun selectPrinterAsDefault(device: BluetoothDevice) {
        // Cancel discovery scan
        var mBluetoothAdapter: BluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        if (mBluetoothAdapter.isDiscovering) {
            mBluetoothAdapter.cancelDiscovery()
        }


        printerManager.cachePrinterAddress(device.address)
        printerManager.cachePrinterName(device.name)

        onDeviceConnected(device)

    }

    override fun rescanBluetooth() {
        var mBluetoothAdapter: BluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

        if (mBluetoothAdapter.isDiscovering) {
            mBluetoothAdapter.cancelDiscovery()
        }

        mBluetoothAdapter.startDiscovery()

        // Clear List of Devices
        rvBluetoothAdapter.clear()
        printerManagerPresenter.populateDeviceList()

        pbScanning.visibility = View.VISIBLE
    }

    override fun onScanComplete() {
        pbScanning.visibility = View.GONE
    }


    override fun onDeviceConnected(device: BluetoothDevice?) {
        val color = ColorDrawable(resources.getColor(R.color.md_green_400))
        civPrinterStatus.setImageDrawable(color)
        tvPrinterStatus.text = "Connected: " + device!!.name
        btnDemoPrint.visibility = View.VISIBLE
    }

    override fun onDeviceDisconnected() {
        val color = ColorDrawable(resources.getColor(R.color.md_red_400))
        civPrinterStatus.setImageDrawable(color)
        tvPrinterStatus.text = "No Printer Connected"
        btnDemoPrint.visibility = View.GONE
    }

    override fun onDeviceMaybeConnecting() {
        val color = ColorDrawable(resources.getColor(R.color.md_orange_400))
        civPrinterStatus.setImageDrawable(color)
    }


}
