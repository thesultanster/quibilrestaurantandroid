package com.food.quibilrestaurant.ui.feature.ordermanager.dialogs

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.widget.Button

import com.food.quibilrestaurant.R

import com.food.quibilrestaurant.models.Session
import kotlinx.android.synthetic.main.dialog_charge_client.*
import kotlinx.android.synthetic.main.dialog_charge_client.view.*

class ChargeClientDialog : DialogFragment() {

    lateinit var session: Session

    lateinit var chargeClientInterface: ChargeClientInterface

    // This is a workaround for the strange behavior of onCreateView (which doesn't show dialog's layout)
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialogBuilder = AlertDialog.Builder(context!!)
        val inflater = LayoutInflater.from(context)
        val dialogView = inflater.inflate(R.layout.dialog_charge_client, null)
        initViews(dialogView)
        dialogBuilder.setView(dialogView)

        session = arguments!!.getSerializable("session") as Session

        // Set dialog to not be cancelable
        isCancelable = false

        return dialogBuilder.create()
    }


    private fun initViews(view: View) {

        view.btnChargeOnly.setOnClickListener {
            chargeClientInterface.chargeClient()
            dialog.dismiss()
        }

        view.btnPrintOnly.setOnClickListener {
            chargeClientInterface.printReceipt()
            dialog.dismiss()
        }

        view.btnPrintAndCharge.setOnClickListener {
            chargeClientInterface.printAndCharge()
            dialog.dismiss()
        }

        view.tvCancel.setOnClickListener { dialog.dismiss() }

    }


    companion object {


        fun newInstance(session: Session, chargeClientInterface: ChargeClientInterface): ChargeClientDialog {
            val frag = ChargeClientDialog()
            frag.chargeClientInterface = chargeClientInterface
            val args = Bundle()
            args.putSerializable("session", session)
            frag.arguments = args
            return frag
        }
    }
}
