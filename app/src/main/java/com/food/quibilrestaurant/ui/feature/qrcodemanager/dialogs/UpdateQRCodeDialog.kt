package com.food.quibilrestaurant.ui.feature.qrcodemanager.dialogs

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import com.food.quibilrestaurant.R
import com.food.quibilrestaurant.models.QRCode
import kotlinx.android.synthetic.main.dialog_update_qr_code.view.*

/**
 * Created by sultankhan on 7/28/18.
 */

class UpdateQRCodeDialog : DialogFragment() {

    private lateinit var updateQrCodeDialogInterface: UpdateQRCodeInterface
    private lateinit var qrCode: QRCode

    // This is a workaround for the strange behavior of onCreateView (which doesn't show dialog's layout)
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialogBuilder = AlertDialog.Builder(context!!)
        val inflater = LayoutInflater.from(context)
        val dialogView = inflater.inflate(R.layout.dialog_update_qr_code, null)
        dialogBuilder.setView(dialogView)

        qrCode = arguments!!.getSerializable("qrCode") as QRCode

        initSubViews(dialogView)

        populateSubViews()

        isCancelable = false

        return dialogBuilder.create()
    }


    private fun initSubViews(rootView: View) {


        rootView.edtTableId.setText(qrCode.tableId)

        rootView.btnUpdateCode.setOnClickListener {

            qrCode.tableId = rootView.edtTableId.text.toString()

            updateQrCodeDialogInterface.userEnteredTableId(qrCode)
            dismiss()
        }

        rootView.btnCancel.setOnClickListener { dialog.dismiss() }

        rootView.tvDelete.setOnClickListener {
            updateQrCodeDialogInterface.userWantsToDeleteQRCode(qrCode)
            dialog.dismiss()
        }


    }

    private fun populateSubViews() {

    }

    companion object {


        fun newInstance(qrCode: QRCode, updateQrCodeDialogInterface: UpdateQRCodeInterface): UpdateQRCodeDialog {


            val frag = UpdateQRCodeDialog()
            var arguments = Bundle()
            frag.updateQrCodeDialogInterface = updateQrCodeDialogInterface
            arguments!!.putSerializable("qrCode", qrCode)
            frag.arguments = arguments
            return frag
        }
    }
}
