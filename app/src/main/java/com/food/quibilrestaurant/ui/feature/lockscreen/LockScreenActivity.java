package com.food.quibilrestaurant.ui.feature.lockscreen;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.davidmiguel.numberkeyboard.NumberKeyboard;
import com.davidmiguel.numberkeyboard.NumberKeyboardListener;
import com.food.quibilrestaurant.R;
import com.food.quibilrestaurant.activities.MainActivity;
import com.food.quibilrestaurant.data_layer.AppState;
import com.food.quibilrestaurant.data_layer.CurrentRestaurant;
import com.food.quibilrestaurant.models.LockscreenCredential;
import com.food.quibilrestaurant.models.Restaurant;
import com.food.quibilrestaurant.ui.feature.login.LoginActivity;
import com.food.quibilrestaurant.ui.feature.splashscreen.SplashScreenActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.locks.Lock;

public class LockScreenActivity extends AppCompatActivity implements LockScreenContract.View {

    NumberKeyboard nkKeypad;
    EditText edtPin;
    ProgressBar plProgress;

    LockScreenPresenter lockScreenPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lock_screen);
        lockScreenPresenter = new LockScreenPresenter(this);

        final Restaurant currentRestaurant = CurrentRestaurant.getInstance(getApplicationContext()).getCache();

        edtPin = findViewById(R.id.edtPin);
        nkKeypad = findViewById(R.id.nkKeypad);
        plProgress = findViewById(R.id.plProgress);
        plProgress.setIndeterminate(false);

        // To stop keyboard form popping up
        edtPin.setFocusable(false);


        nkKeypad.setListener(new NumberKeyboardListener() {
            @Override
            public void onNumberClicked(int number) {

                // If less than 4 add number
                if (edtPin.getText().length() != 4) {
                    edtPin.setText(edtPin.getText().toString() + number);
                    edtPin.setSelection(edtPin.getText().length());
                }

                String pin = edtPin.getText().toString();

                // After adding number, if we have reached 4 numbers
                if (pin.length() == 4) {
                    plProgress.setVisibility(View.VISIBLE);
                    // Fetch lockscreen credentials
                    lockScreenPresenter.getCredentials(currentRestaurant.id, pin, getApplicationContext());
                }

            }

            @Override
            public void onLeftAuxButtonClicked() {

            }

            @Override
            public void onRightAuxButtonClicked() {
                if (edtPin.getText().length() > 0) {
                    edtPin.setText(edtPin.getText().toString().substring(0, edtPin.getText().length() - 1));
                    edtPin.setSelection(edtPin.getText().length());
                }
            }
        });
    }

    @Override
    public void hideProgressBar() {
        plProgress.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showSnackbar(@NotNull String title) {
        Snackbar.make(findViewById(android.R.id.content), title, Snackbar.LENGTH_LONG)
                .show();
    }

    @Override
    public void goToMainActivity() {
        Intent intent = new Intent(LockScreenActivity.this, MainActivity.class);
        intent.putExtra("previousActivity", "LockScreenActivity");
        startActivity(intent);
        finish();
    }
}
