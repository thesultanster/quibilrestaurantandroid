package com.food.quibilrestaurant.fragments;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.food.quibilrestaurant.R;
import com.food.quibilrestaurant.activities.BaseFragment;
import com.food.quibilrestaurant.data_layer.CurrentRestaurant;
import com.food.quibilrestaurant.interfaces.UpdateRestaurantInterface;
import com.food.quibilrestaurant.models.Restaurant;
import com.food.quibilrestaurant.models.StaffWorker;
import com.food.quibilrestaurant.models.User;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.rey.material.widget.EditText;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.net.URL;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.MODE_PRIVATE;


public class ProfileViewerAndEditorFragment extends BaseFragment {

    TextView tvName;
    TextView tvPhoneNumber;
    TextView tvAddress;
    TextView tvChangeProfilePhoto;
    EditText edtName;
    EditText edtPhoneNumber;
    EditText edtAddress;
    TextView tvEdit;
    CircleImageView civProfilePicture;

    boolean isEditOn = false;

    Typeface openFace;
    User currentUser;

    public ProfileViewerAndEditorFragment() {
        // Required empty public constructor
    }

    public static ProfileViewerAndEditorFragment newInstance(User currentUser) {
        ProfileViewerAndEditorFragment frag = new ProfileViewerAndEditorFragment();
        Bundle args = new Bundle();
        args.putSerializable("currentUser", currentUser);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        openFace = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Light.ttf");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_profile_viewer_editor, container, false);
        Restaurant restaurant = CurrentRestaurant.getInstance(getContext()).getCache();
        //currentUser = (User) getArguments().getSerializable("currentUser");

        tvName = (TextView) view.findViewById(R.id.tvName);
        tvPhoneNumber = (TextView) view.findViewById(R.id.tvPhoneNumber);
        tvEdit = (TextView) view.findViewById(R.id.tvEdit);
        tvAddress = (TextView) view.findViewById(R.id.tvAddress);
        tvChangeProfilePhoto = (TextView) view.findViewById(R.id.tvChangeProfilePhoto);
        edtName = (EditText) view.findViewById(R.id.edtFirstName);
        edtPhoneNumber = (EditText) view.findViewById(R.id.edtPhoneNumber);
        edtAddress = (EditText) view.findViewById(R.id.edtAddress);
        civProfilePicture = (CircleImageView) view.findViewById(R.id.civProfilePicture);

        tvChangeProfilePhoto.setVisibility(View.GONE);

       tvChangeProfilePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkCameraPermission()) {
                    takePhotoOrChoose();
                } else {
                    requestPermission();
                }
            }
        });
        Picasso.with(getContext()).load(restaurant.profilePictureUrl).into(civProfilePicture);


        tvName.setText(restaurant.name);
        tvPhoneNumber.setText(restaurant.phoneNumber);
        tvAddress.setText(restaurant.address);
        edtName.setText(restaurant.name);
        edtPhoneNumber.setText(restaurant.phoneNumber);
        edtAddress.setText(restaurant.address);

        tvEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isEditOn) {
                    isEditOn = false;
                    tvEdit.setText("EDIT");


                    tvName.setText(edtName.getText().toString());
                    tvPhoneNumber.setText(edtPhoneNumber.getText().toString());
                    tvAddress.setText(edtAddress.getText().toString());

                    Restaurant restaurant = CurrentRestaurant.getInstance(getContext()).getCache();
                    restaurant.name = edtName.getText().toString();
                    restaurant.phoneNumber = edtPhoneNumber.getText().toString();
                    restaurant.address = edtAddress.getText().toString();
                    CurrentRestaurant.getInstance(getContext()).cacheRestaurant(restaurant);

                    SharedPreferences.Editor editor = getContext().getSharedPreferences("user", MODE_PRIVATE).edit();
                    editor.putBoolean("hasFilledRestaurant", true);
                    editor.commit();

                    tvName.setVisibility(View.VISIBLE);
                    tvPhoneNumber.setVisibility(View.VISIBLE);
                    tvAddress.setVisibility(View.VISIBLE);

                    edtName.setVisibility(View.GONE);
                    edtPhoneNumber.setVisibility(View.GONE);
                    edtAddress.setVisibility(View.GONE);
                    tvChangeProfilePhoto.setVisibility(View.GONE);

                } else {
                    isEditOn = true;
                    tvEdit.setText("SAVE");

                    tvName.setVisibility(View.GONE);
                    tvPhoneNumber.setVisibility(View.GONE);
                    tvAddress.setVisibility(View.GONE);

                    edtName.setVisibility(View.VISIBLE);
                    edtPhoneNumber.setVisibility(View.VISIBLE);
                    edtAddress.setVisibility(View.VISIBLE);
                    tvChangeProfilePhoto.setVisibility(View.VISIBLE);

                }


            }
        });


        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        // Detects request codes
        if ((requestCode == 1888 || requestCode == 1889) && resultCode == Activity.RESULT_OK) {

            // Set image to bitmap
            try {

                // Get image bitmap and set to images
                Bitmap bitmap;
                if (requestCode == 1889) {
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                } else {
                    bitmap = (Bitmap) data.getExtras().get("data");
                }

                civProfilePicture.setImageBitmap(bitmap);

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 80, baos);
                byte[] pictureData = baos.toByteArray();

                float aspectRatio = bitmap.getWidth() / (float) bitmap.getHeight();

                int height = 500;
                int width = Math.round(height * aspectRatio);

                bitmap = Bitmap.createScaledBitmap(
                        bitmap, width, height, false);

                final Handler handler = new Handler();
                final Bitmap finalBitmap = bitmap;
                Runnable runnable = new Runnable() {
                    public void run() {

                        URL img_value = null;
                        Bitmap mIcon1 = null;

                        try {
                            handler.post(new Runnable() {
                                public void run() {
                                    tvChangeProfilePhoto.setVisibility(View.GONE);
                                    civProfilePicture.setImageBitmap(finalBitmap);
                                }
                            });

                        } catch (Exception e) {
                            Log.e("IOException", "onActivityResult");
                            e.printStackTrace();
                        }


                    }
                };
                new Thread(runnable).start();


                FirebaseStorage storage = FirebaseStorage.getInstance();
                // Create a storage reference from our app
                StorageReference storageRef = storage.getReference();
                // Create a reference to profile picture
                final StorageReference photoRef = storageRef.child("images/user_profile_image/" +  FirebaseAuth.getInstance().getCurrentUser().getUid() + "/profile_image.jpeg");

                // Upload photo to firebase
                UploadTask uploadTask = photoRef.putBytes(pictureData);
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        Log.e("onActivityResult", "Upload Faliure: " + exception.toString());
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.

                        photoRef.getDownloadUrl()
                                .addOnSuccessListener(new OnSuccessListener<Uri>() {
                                                          @Override
                                                          public void onSuccess(Uri uri) {
                                                              Restaurant restaurant = CurrentRestaurant.getInstance(getContext()).getCache();
                                                              restaurant.profilePictureUrl = uri.toString();
                                                              CurrentRestaurant.getInstance(getContext()).cacheRestaurant(restaurant);
                                                              CurrentRestaurant.getInstance(getContext()).updateRestaurantToDatabase(restaurant, new UpdateRestaurantInterface() {
                                                                  @Override
                                                                  public void onUpdated() {

                                                                  }

                                                                  @Override
                                                                  public void onError() {

                                                                  }
                                                              });

                                                              Log.d("onActivityResult", "Upload SUCCESS: ");
                                                          }
                                                      }
                                );


                        Log.d("onActivityResult", "Upload SUCCESS: ");

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


}
