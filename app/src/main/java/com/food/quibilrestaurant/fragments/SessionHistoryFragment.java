package com.food.quibilrestaurant.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.food.quibilrestaurant.R;
import com.food.quibilrestaurant.adapters.AddOnItemsRecyclerAdapter;
import com.food.quibilrestaurant.adapters.ReceipttemsRecyclerAdapter;
import com.food.quibilrestaurant.data_layer.CurrentRestaurant;
import com.food.quibilrestaurant.dialogs.AddAddonItemDialog;
import com.food.quibilrestaurant.dialogs.UpdateAddonItemDialog;
import com.food.quibilrestaurant.interfaces.CreateAddonItemInterface;
import com.food.quibilrestaurant.interfaces.FetchRestaurantItemAddOnsInterface;
import com.food.quibilrestaurant.interfaces.OnAddOnItemSelectedInterface;
import com.food.quibilrestaurant.interfaces.UpdateAddonItemInterface;
import com.food.quibilrestaurant.interfaces.UpdateMenuItemInterface;
import com.food.quibilrestaurant.interfaces.UserEnteredNewAddonItemInterface;
import com.food.quibilrestaurant.models.AddOnItem;
import com.food.quibilrestaurant.models.MenuItem;
import com.food.quibilrestaurant.models.Restaurant;
import com.food.quibilrestaurant.models.Session;
import com.food.quibilrestaurant.network_layer.MenuService;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.rey.material.widget.EditText;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.net.URL;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;


public class SessionHistoryFragment extends Fragment {
    String TAG = "SessionHistoryFragment";


    TextView tvName;
    TextView tvDate;
    TextView tvPrice;
    CircleImageView civProfilePicture;

    Typeface openFace;
    Session session;
    RecyclerView rvMenuItems;
    ReceipttemsRecyclerAdapter receipttemsRecyclerAdapter;


    public SessionHistoryFragment() {
        // Required empty public constructor
    }

    public static SessionHistoryFragment newInstance(Session session) {
        SessionHistoryFragment frag = new SessionHistoryFragment();
        Bundle args = new Bundle();
        args.putSerializable("session", session);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        openFace = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Light.ttf");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_session_history, container, false);
        session = (Session) getArguments().getSerializable("session");

        tvPrice = view.findViewById(R.id.tvPrice);
        tvDate = view.findViewById(R.id.tvDate);
        tvName = view.findViewById(R.id.tvName);
        civProfilePicture = view.findViewById(R.id.civProfilePicture);
        rvMenuItems = view.findViewById(R.id.rvMenuItems);

        receipttemsRecyclerAdapter = new ReceipttemsRecyclerAdapter(getContext());
        rvMenuItems.setLayoutManager(new LinearLayoutManager(getContext()));
        rvMenuItems.setAdapter(receipttemsRecyclerAdapter);
        receipttemsRecyclerAdapter.setMenuItems(session.orders);

        // Format and set total cart price
        NumberFormat n = NumberFormat.getCurrencyInstance(Locale.US);
        String s = n.format(session.totalPaymentAmount / 100.0);
        tvPrice.setText(s);

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String dateString = formatter.format(new Date(session.createdAt));
        tvDate.setText(dateString);

        tvName.setText(session.userFullName);

        if(session.userProfilePicURL != null && !session.userProfilePicURL.equals("")){
            Picasso.with(getContext()).load(session.userProfilePicURL).into(civProfilePicture);
        }

        return view;
    }



}
