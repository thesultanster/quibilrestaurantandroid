package com.food.quibilrestaurant.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.food.quibilrestaurant.R;
import com.food.quibilrestaurant.adapters.SessionHistoryItemsRecyclerAdapter;
import com.food.quibilrestaurant.interfaces.FetchPastSessionsInterface;
import com.food.quibilrestaurant.interfaces.OnSessionHistorySelectedInterface;
import com.food.quibilrestaurant.models.MenuCategory;
import com.food.quibilrestaurant.models.Session;
import com.food.quibilrestaurant.network_layer.UserService;

import java.util.ArrayList;


public class SessionHistoryListFragment extends Fragment {


    Typeface openFace;

    RecyclerView rvPastSessions;
    SessionHistoryItemsRecyclerAdapter sessionHistoryItemsRecyclerAdapter;


    public SessionHistoryListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        openFace = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Light.ttf");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_session_history_list, container, false);

        rvPastSessions = view.findViewById(R.id.rvPastSessions);

        sessionHistoryItemsRecyclerAdapter = new SessionHistoryItemsRecyclerAdapter(getContext(), new ArrayList<Session>(), new OnSessionHistorySelectedInterface() {
            @Override
            public void onHistorySelected(int position, Session session) {

                // Create a new fragment and specify the fragment to show based on nav item clicked
                SessionHistoryFragment fragment = SessionHistoryFragment.newInstance(session);
                // Insert the fragment by replacing any existing fragment
                FragmentManager fragmentManager = getChildFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.flSessionHistory, fragment).commit();

            }
        });

        rvPastSessions.setLayoutManager(new LinearLayoutManager(getContext()));
        rvPastSessions.setAdapter(sessionHistoryItemsRecyclerAdapter);


        UserService.getInstance(getContext()).getOlderSessions(new FetchPastSessionsInterface() {
            @Override
            public void onDataFetched(Session session) {
                sessionHistoryItemsRecyclerAdapter.addRow(session);
            }

            @Override
            public void onClearList() {
                sessionHistoryItemsRecyclerAdapter.clearRows();
            }

            @Override
            public void onError() {

            }
        });

        return view;
    }


}
