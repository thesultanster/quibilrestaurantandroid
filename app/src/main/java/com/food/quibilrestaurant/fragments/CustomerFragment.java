package com.food.quibilrestaurant.fragments;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.TextView;

import com.food.quibilrestaurant.R;


public class CustomerFragment extends Fragment {

    RecyclerView recyclerView;
    //PostRecyclerAdapter adapter;
    View currentFocusedLayout, oldFocusedLayout;

    private FloatingActionButton fabAddCourse;

    TextView tvTitle;
    TextView tvLearn;
    TextView tvShare;

    Typeface face;
    Typeface openFace;


    public CustomerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        openFace = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Light.ttf");
        //adapter = new PostRecyclerAdapter(getContext(), new ArrayList<Post>());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_customer, container, false);

        tvTitle = (TextView) view.findViewById(R.id.tvTitle);
        tvShare = (TextView) view.findViewById(R.id.tvShare);
        tvLearn = (TextView) view.findViewById(R.id.tvLearn);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        fabAddCourse = (FloatingActionButton) view.findViewById(R.id.fabAddCourse);


        tvTitle.setTypeface(face);
        tvShare.setTypeface(openFace);
        tvLearn.setTypeface(openFace);


        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        //recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView,
                                             int newState) {
                super.onScrollStateChanged(recyclerView, newState);


                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
                    //get the recyclerview position which is completely visible and first
                    int positionView = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
                    Log.i("POST VISISBLE", positionView + "");
                    if (positionView >= 0) {
                        if (oldFocusedLayout != null) {
                            //Stop the previous video playback after new scroll
                            //PostView postView = (PostView) oldFocusedLayout.findViewById(R.id.postView);
                            //postView.pauseVideo();
                        }


                        currentFocusedLayout = ((LinearLayoutManager) recyclerView.getLayoutManager()).findViewByPosition(positionView);
                        //PostView currentPost = (PostView) currentFocusedLayout.findViewById(R.id.postView);
                        //currentPost.startVideo();
                        oldFocusedLayout = currentFocusedLayout;
                    }
                }

            }

        });

//        fabAddCourse.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getContext(), GalleryActivity.class);
//                startActivity(intent);
//            }
//        });




        tvShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Navigate to Share page
                Intent navigateVideoIntent = new Intent("NAVIGATE");
                navigateVideoIntent.putExtra("position", 0);
                LocalBroadcastManager.getInstance(getContext()).sendBroadcast(navigateVideoIntent);
            }
        });

        tvLearn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Navigate to Courses page
                Intent navigateVideoIntent = new Intent("NAVIGATE");
                navigateVideoIntent.putExtra("position", 2);
                LocalBroadcastManager.getInstance(getContext()).sendBroadcast(navigateVideoIntent);
            }
        });

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();

        //adapter.clearData();
//
//        PostService.getInstance(getActivity()).fetchAllPosts("", "", "", "",
//                0, new PostFetchListener() {
//            @Override
//            public void onPostFetched(Post object) {
//                adapter.addRowFront(object);
//            }
//
//            @Override
//            public void onPostFetchedCompleteListener() {
//
//            }
//        });

    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        // Make sure that we are currently visible
        if (this.isVisible()) {
            // If we are becoming invisible, then let the recyclerview know pause a played video
            if (!isVisibleToUser) {

            }
        }
    }
}
