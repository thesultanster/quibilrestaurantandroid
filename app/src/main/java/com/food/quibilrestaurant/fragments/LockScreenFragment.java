package com.food.quibilrestaurant.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.davidmiguel.numberkeyboard.NumberKeyboard;
import com.davidmiguel.numberkeyboard.NumberKeyboardListener;
import com.food.quibilrestaurant.R;
import com.food.quibilrestaurant.data_layer.AppState;
import com.food.quibilrestaurant.data_layer.CurrentRestaurant;
import com.food.quibilrestaurant.interfaces.LockScreenSecurityInterface;
import com.food.quibilrestaurant.models.LockscreenCredential;
import com.food.quibilrestaurant.models.Restaurant;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class LockScreenFragment extends Fragment {
    String TAG = "LockScreenFragment";

    RelativeLayout rlRoot;
    NumberKeyboard nkKeypad;
    EditText edtPin;
    ProgressBar plProgress;
    LockScreenSecurityInterface lockScreenSecurityInterface;

    public LockScreenFragment() {
        // Required empty public constructor
    }

    public static LockScreenFragment newInstance(LockScreenSecurityInterface lockScreenSecurityInterface) {
        LockScreenFragment frag = new LockScreenFragment();
        frag.lockScreenSecurityInterface = lockScreenSecurityInterface;
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //openFace = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Light.ttf");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_lock_screen, container, false);
        final Restaurant currentRestaurant = CurrentRestaurant.getInstance(getContext()).getCache();

        rlRoot = view.findViewById(R.id.rlRoot);
        edtPin = view.findViewById(R.id.edtPin);
        nkKeypad = view.findViewById(R.id.nkKeypad);
        plProgress = view.findViewById(R.id.plProgress);

        // To stop keyboard form popping up
        edtPin.setFocusable(false);


        nkKeypad.setListener(new NumberKeyboardListener() {
            @Override
            public void onNumberClicked(int number) {

                // If less than 4 add number
                if (edtPin.getText().length() != 4) {
                    edtPin.setText(edtPin.getText().toString() + number);
                    edtPin.setSelection(edtPin.getText().length());
                }


                String pin = edtPin.getText().toString();
                // After adding number, if we have reached 4 numbers
                if (pin.length() == 4) {

                    plProgress.setIndeterminate(false);
                    plProgress.setVisibility(View.VISIBLE);

                    // Fetch lockscreen credentials
                    //TODO: Move over to user service
                    FirebaseDatabase.getInstance().getReference()
                            .child("restaurant_pin_user_relation")
                            .child(currentRestaurant.id)
                            .child(pin).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            LockscreenCredential credential = dataSnapshot.getValue(LockscreenCredential.class);
                            plProgress.setVisibility(View.INVISIBLE);

                            // If no id found
                            if(credential == null){
                                Toast.makeText(getActivity(), "No user found for pin code", Toast.LENGTH_SHORT).show();
                            }
                            // Else if id found
                            else {
                                Log.d(TAG, "User ID: " + credential.id +"");

                                // Cache mode and staff id
                                if(credential.profileType.equals("STAFF")) {
                                    AppState.getInstance(getContext()).cacheAppMode(credential.profileType);
                                    AppState.getInstance(getContext()).cacheLockscreenCredential(credential);
                                } else {
                                    AppState.getInstance(getContext()).cacheAppMode(credential.profileType);
                                    AppState.getInstance(getContext()).cacheLockscreenCredential(credential);
                                }


                                lockScreenSecurityInterface.unlockScreen(credential.profileType, credential.name);

                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            Log.e(TAG, databaseError.getMessage());
                            Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_SHORT).show();

                        }
                    });
                }

            }

            @Override
            public void onLeftAuxButtonClicked() {

            }

            @Override
            public void onRightAuxButtonClicked() {
                if (edtPin.getText().length() > 0) {
                    edtPin.setText(edtPin.getText().toString().substring(0, edtPin.getText().length() - 1));
                    edtPin.setSelection(edtPin.getText().length());
                }
            }
        });

        rlRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


        return view;
    }



}
