package com.food.quibilrestaurant.fragments;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.food.quibilrestaurant.R;
import com.food.quibilrestaurant.activities.BaseFragment;
import com.food.quibilrestaurant.data_layer.CurrentRestaurant;
import com.food.quibilrestaurant.interfaces.UpdateMenuItemInterface;
import com.food.quibilrestaurant.interfaces.UpdateRestaurantInterface;
import com.food.quibilrestaurant.models.Restaurant;
import com.food.quibilrestaurant.network_layer.MenuService;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.rey.material.widget.EditText;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.net.URL;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.MODE_PRIVATE;


public class RestaurantViewerAndEditorFragment extends BaseFragment {

    TextView tvName;
    TextView tvPhoneNumber;
    TextView tvAddress;
    TextView tvChangeProfilePhoto;
    EditText edtName;
    EditText edtPhoneNumber;
    EditText edtAddress;
    TextView tvEdit;
    ImageView civProfilePicture;
    ImageView ivAddPhoto;

    boolean isEditOn = false;

    Typeface openFace;

    Restaurant restaurant;


    public RestaurantViewerAndEditorFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        openFace = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Light.ttf");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_restaurant_viewer_editor, container, false);
        restaurant = CurrentRestaurant.getInstance(getContext()).getCache();

        tvName = (TextView) view.findViewById(R.id.tvFirstName);
        tvPhoneNumber = (TextView) view.findViewById(R.id.tvPhoneNumber);
        tvEdit = (TextView) view.findViewById(R.id.tvEdit);
        tvAddress = (TextView) view.findViewById(R.id.tvAddress);
        tvChangeProfilePhoto = (TextView) view.findViewById(R.id.tvChangeProfilePhoto);
        edtName = (EditText) view.findViewById(R.id.edtFirstName);
        edtPhoneNumber = (EditText) view.findViewById(R.id.edtPhoneNumber);
        edtAddress = view.findViewById(R.id.edtAddress);
        civProfilePicture = view.findViewById(R.id.civProfilePicture);
        ivAddPhoto = view.findViewById(R.id.ivAddPhoto);

        tvName.setTypeface(openFace);
        tvPhoneNumber.setTypeface(openFace);
        tvAddress.setTypeface(openFace);


        tvChangeProfilePhoto.setVisibility(View.GONE);

        // Setup Profile Picture
        tvChangeProfilePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkCameraPermission()) {
                    takePhotoOrChoose();
                } else {
                    requestPermission();
                }
            }
        });

        if (restaurant.profilePictureUrl != null && !restaurant.profilePictureUrl.equals("")) {
            Picasso.with(getContext()).load(restaurant.profilePictureUrl).into(civProfilePicture);
            ivAddPhoto.setVisibility(View.GONE);
        }


        // Set the text for our user fields, make text textview "hint" light if empty
        if (restaurant.name != null && !restaurant.name.equals("")) {
            tvName.setText(restaurant.name);
            edtName.setText(restaurant.name);
        } else {
            tvName.setTextColor(ContextCompat.getColor(getContext(), R.color.md_grey_500));
        }

        if (restaurant.phoneNumber != null && !restaurant.phoneNumber.equals("")) {
            tvPhoneNumber.setText(restaurant.phoneNumber);
            edtPhoneNumber.setText(restaurant.phoneNumber);
        } else {
            tvPhoneNumber.setTextColor(ContextCompat.getColor(getContext(), R.color.md_grey_500));
        }
        if (restaurant.address != null && !restaurant.address.equals("")) {
            tvAddress.setText(restaurant.address);
            edtAddress.setText(restaurant.address);
        } else {
            tvAddress.setTextColor(ContextCompat.getColor(getContext(), R.color.md_grey_500));
        }

        tvEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isEditOn) {
                    isEditOn = false;
                    tvEdit.setText("EDIT");


                    tvName.setText(edtName.getText().toString());
                    tvPhoneNumber.setText(edtPhoneNumber.getText().toString());
                    tvAddress.setText(edtAddress.getText().toString());

                    Restaurant restaurant = CurrentRestaurant.getInstance(getContext()).getCache();
                    restaurant.name = edtName.getText().toString();
                    restaurant.phoneNumber = edtPhoneNumber.getText().toString();
                    restaurant.address = edtAddress.getText().toString();
                    CurrentRestaurant.getInstance(getContext()).cacheRestaurant(restaurant);
                    CurrentRestaurant.getInstance(getContext()).updateRestaurantToDatabase(restaurant, new UpdateRestaurantInterface() {
                        @Override
                        public void onUpdated() {

                        }

                        @Override
                        public void onError() {

                        }
                    });

                    SharedPreferences.Editor editor = getContext().getSharedPreferences("user", MODE_PRIVATE).edit();
                    editor.putBoolean("hasFilledRestaurant", true);
                    editor.commit();

                    tvName.setVisibility(View.VISIBLE);
                    tvPhoneNumber.setVisibility(View.VISIBLE);
                    tvAddress.setVisibility(View.VISIBLE);

                    edtName.setVisibility(View.GONE);
                    edtPhoneNumber.setVisibility(View.GONE);
                    edtAddress.setVisibility(View.GONE);
                    tvChangeProfilePhoto.setVisibility(View.GONE);

                } else {
                    isEditOn = true;
                    tvEdit.setText("SAVE");

                    tvName.setVisibility(View.GONE);
                    tvPhoneNumber.setVisibility(View.GONE);
                    tvAddress.setVisibility(View.GONE);

                    edtName.setVisibility(View.VISIBLE);
                    edtPhoneNumber.setVisibility(View.VISIBLE);
                    edtAddress.setVisibility(View.VISIBLE);
                    tvChangeProfilePhoto.setVisibility(View.VISIBLE);

                }


            }
        });


        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        // Detects request codes
        if ((requestCode == 1888 || requestCode == 1889) && resultCode == Activity.RESULT_OK) {

            // Set image to bitmap
            try {

                // Get image bitmap and set to images
                Bitmap bitmap;
                if (requestCode == 1889) {
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                } else {
                    bitmap = (Bitmap) data.getExtras().get("data");
                }

                civProfilePicture.setImageBitmap(bitmap);

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 80, baos);
                byte[] pictureData = baos.toByteArray();

                float aspectRatio = bitmap.getWidth() / (float) bitmap.getHeight();

                int height = 500;
                int width = Math.round(height * aspectRatio);

                bitmap = Bitmap.createScaledBitmap(
                        bitmap, width, height, false);

                final Handler handler = new Handler();
                final Bitmap finalBitmap = bitmap;
                Runnable runnable = new Runnable() {
                    public void run() {

                        URL img_value = null;
                        Bitmap mIcon1 = null;

                        try {
                            handler.post(new Runnable() {
                                public void run() {
                                    ivAddPhoto.setVisibility(View.GONE);
                                    civProfilePicture.setImageBitmap(finalBitmap);
                                }
                            });

                        } catch (Exception e) {
                            Log.e("IOException", "onActivityResult");
                            e.printStackTrace();
                        }


                    }
                };
                new Thread(runnable).start();


                FirebaseStorage storage = FirebaseStorage.getInstance();
                // Create a storage reference from our app
                StorageReference storageRef = storage.getReference();
                // Create a reference to profile picture
                final StorageReference photoRef = storageRef.child("images/restaurant_profile_image/" + restaurant.id + "/profile_image.jpeg");

                // Upload photo to firebase
                UploadTask uploadTask = photoRef.putBytes(pictureData);
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        Log.e("onActivityResult", "Upload Faliure: " + exception.toString());
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.

                        photoRef.getDownloadUrl()
                                .addOnSuccessListener(new OnSuccessListener<Uri>() {
                                                          @Override
                                                          public void onSuccess(Uri uri) {
                                                              Restaurant restaurant = CurrentRestaurant.getInstance(getContext()).getCache();
                                                              restaurant.profilePictureUrl = uri.toString();
                                                              CurrentRestaurant.getInstance(getContext()).cacheRestaurant(restaurant);
                                                              CurrentRestaurant.getInstance(getContext()).updateRestaurantToDatabase(restaurant, new UpdateRestaurantInterface() {
                                                                  @Override
                                                                  public void onUpdated() {

                                                                  }

                                                                  @Override
                                                                  public void onError() {

                                                                  }
                                                              });

                                                              Log.d("onActivityResult", "Upload SUCCESS: ");
                                                          }
                                                      }
                                );


                        Log.d("onActivityResult", "Upload SUCCESS: ");

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }


}
