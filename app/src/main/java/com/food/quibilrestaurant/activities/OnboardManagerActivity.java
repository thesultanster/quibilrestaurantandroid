package com.food.quibilrestaurant.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.food.quibilrestaurant.R;
import com.food.quibilrestaurant.data_layer.AppState;
import com.food.quibilrestaurant.data_layer.CurrentRestaurant;
import com.food.quibilrestaurant.interfaces.CanPinBeMadeInterface;
import com.food.quibilrestaurant.interfaces.CreateStaffWorkerInterface;
import com.food.quibilrestaurant.interfaces.UpdateStaffWorkerInterface;
import com.food.quibilrestaurant.models.LockscreenCredential;
import com.food.quibilrestaurant.models.Restaurant;
import com.food.quibilrestaurant.models.StaffWorker;
import com.food.quibilrestaurant.network_layer.StaffService;
import com.food.quibilrestaurant.network_layer.UserService;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class OnboardManagerActivity extends AppCompatActivity {
    String TAG = "OnboardManagerActivity";

    Button btnCancel;
    Button btnAddWorker;
    EditText etName;
    EditText edtPin;

    ProgressDialog progressDialog;

    Restaurant currentRestaurant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboard_manager);

        currentRestaurant = CurrentRestaurant.getInstance(getApplicationContext()).getCache();

        btnAddWorker = findViewById(R.id.btnAddWorker);
        btnCancel = findViewById(R.id.btnCancel);
        edtPin = findViewById(R.id.edtPin);
        etName = findViewById(R.id.etName);

        btnAddWorker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                progressDialog = new ProgressDialog(OnboardManagerActivity.this);
                progressDialog.setMessage("Saving Profile...");
                progressDialog.setCancelable(false);
                progressDialog.show();

                final StaffWorker staffWorker = new StaffWorker();
                staffWorker.name = etName.getText().toString();
                staffWorker.pin = edtPin.getText().toString();
                staffWorker.profileType = "MANAGER";

                if (AppState.getInstance(getApplicationContext()).getIsManagerStaffAccountCreated()) {
                    StaffService.getInstance(getApplicationContext()).updateStaffWorker(currentRestaurant.id, staffWorker, new UpdateStaffWorkerInterface() {
                        @Override
                        public void onUpdated() {
                            Log.d(TAG, "addNewStaffWorker:onUpdated when getIsManagerStaffAccountCreated = true");


                            // Fetch lockscreen credentials
                            //TODO: Move over to user service
                            FirebaseDatabase.getInstance().getReference()
                                    .child("restaurant_pin_user_relation")
                                    .child(currentRestaurant.id)
                                    .child(staffWorker.pin).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    LockscreenCredential credential = dataSnapshot.getValue(LockscreenCredential.class);

                                    progressDialog.dismiss();

                                    Log.d(TAG, "User ID: " + credential.id + "");

                                    // Cache credentials
                                    AppState.getInstance(getApplicationContext()).cacheAppMode(credential.profileType);
                                    AppState.getInstance(getApplicationContext()).cacheLockscreenCredential(credential);

                                    Intent intent = new Intent(OnboardManagerActivity.this, MainActivity.class);
                                    intent.putExtra("previousActivity", "OnboardManagerActivity");
                                    startActivity(intent);
                                    finish();
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                    Log.e(TAG, databaseError.getMessage());
                                    Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();

                                }
                            });

                        }

                        @Override
                        public void onError() {
                            Log.e(TAG, "addNewStaffWorker:onError when getIsManagerStaffAccountCreated = true");
                            progressDialog.dismiss();
                        }
                    });
                } else {
                    StaffService.getInstance(getApplicationContext()).addNewStaffWorker(currentRestaurant.id, staffWorker, new CreateStaffWorkerInterface() {
                        @Override
                        public void onCreated() {
                            Log.d(TAG, "addNewStaffWorker:onUpdated when getIsManagerStaffAccountCreated = false");
                            AppState.getInstance(getApplicationContext()).cacheIsManagerStaffAccountCreated(true);

                            // Fetch lockscreen credentials
                            //TODO: Move over to user service
                            FirebaseDatabase.getInstance().getReference()
                                    .child("restaurant_pin_user_relation")
                                    .child(currentRestaurant.id)
                                    .child(staffWorker.pin).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    LockscreenCredential credential = dataSnapshot.getValue(LockscreenCredential.class);


                                    progressDialog.dismiss();

                                    Log.d(TAG, "User ID: " + credential.id + "");

                                    // Cache credentials
                                    AppState.getInstance(getApplicationContext()).cacheAppMode(credential.profileType);
                                    AppState.getInstance(getApplicationContext()).cacheLockscreenCredential(credential);


                                    Intent intent = new Intent(OnboardManagerActivity.this, MainActivity.class);
                                    intent.putExtra("previousActivity", "OnboardManagerActivity");
                                    startActivity(intent);
                                    finish();
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                    Log.e(TAG, databaseError.getMessage());
                                    Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();

                                }
                            });

                        }

                        @Override
                        public void onError() {
                            Log.e(TAG, "addNewStaffWorker:onError when getIsManagerStaffAccountCreated = false");
                            progressDialog.dismiss();
                        }
                    });
                }


            }
        });


        edtPin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 4) {
                    UserService.getInstance(getApplicationContext()).checkIfPinHasNotBeenChosenByPreviousUsers(currentRestaurant.id, editable.toString(), new CanPinBeMadeInterface() {
                        @Override
                        public void pinAvailable() {
                            Log.d(TAG, "Pin available");
                            btnAddWorker.setEnabled(true);
                        }

                        @Override
                        public void pinTaken() {
                            edtPin.setError("Pin taken by another user");
                            btnAddWorker.setEnabled(false);
                        }
                    });
                }
            }
        });

    }
}
