package com.food.quibilrestaurant.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.food.quibilrestaurant.R;
import com.food.quibilrestaurant.data_layer.AppState;
import com.food.quibilrestaurant.data_layer.CurrentRestaurant;
import com.food.quibilrestaurant.data_layer.CurrentUser;
import com.food.quibilrestaurant.ui.feature.ordermanager.IncomingOrderFragment;
import com.food.quibilrestaurant.fragments.LockScreenFragment;
import com.food.quibilrestaurant.ui.feature.menueditor.MenuEditorFragment;
import com.food.quibilrestaurant.fragments.RestaurantViewerAndEditorFragment;
import com.food.quibilrestaurant.fragments.SessionHistoryListFragment;
import com.food.quibilrestaurant.ui.feature.splashscreen.SplashScreenActivity;
import com.food.quibilrestaurant.ui.feature.staffeditor.StaffListFragment;
import com.food.quibilrestaurant.interfaces.LockScreenSecurityInterface;
import com.food.quibilrestaurant.models.LockscreenCredential;
import com.food.quibilrestaurant.models.Restaurant;
import com.food.quibilrestaurant.models.User;
import com.food.quibilrestaurant.ui.feature.printermanager.PrinterOptionsFragment;
import com.food.quibilrestaurant.ui.feature.qrcodemanager.QRCodeTableManagerFragment;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity implements LockScreenSecurityInterface {

    LinearLayout llProfileType;
    TextView tvProfileType;
    DrawerLayout mDrawerLayout;
    NavigationView navigationView;
    Toolbar toolbar;
    ImageView ivLock;

    Restaurant currentRestaurant;
    User currentUser;
    String appMode;

    Class currentClass = IncomingOrderFragment.class;
    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Get current restaurant and user from cache
        currentRestaurant = CurrentRestaurant.getInstance(getApplicationContext()).getCache();
        currentUser = CurrentUser.getInstance(getApplicationContext()).getCache();
        appMode = AppState.getInstance(getApplicationContext()).getAppMode();

        llProfileType = findViewById(R.id.llProfileType);
        tvProfileType = findViewById(R.id.tvProfileType);
        toolbar = findViewById(R.id.toolbar);
        navigationView = findViewById(R.id.nav_view);
        mDrawerLayout = findViewById(R.id.mDrawerLayout);
        ivLock = findViewById(R.id.ivLock);


        // Find out what the previous activity
        Intent myIntent = getIntent();
        String previousActivity = myIntent.getStringExtra("previousActivity");

        setupDefaultToolbarAndNavigation();

        ivLock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lockScreen();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

        LockscreenCredential credential = AppState.getInstance(getApplicationContext()).getLockscreenCredential();
        tvProfileType.setText(credential.profileType + " : " + credential.name);
        setUIState(credential.profileType);
    }

    private void setupDefaultToolbarAndNavigation() {


        setSupportActionBar(toolbar);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        // set item as selected to persistAndCache highlight
                        menuItem.setChecked(true);
                        // close drawer when item is tapped
                        mDrawerLayout.closeDrawers();

                        // Create a new fragment and specify the fragment to show based on nav item clicked
                        Fragment fragment = null;
                        Class fragmentClass = currentClass;
                        switch (menuItem.getItemId()) {
                            case R.id.restaurant_editor:
                                fragmentClass = RestaurantViewerAndEditorFragment.class;
                                break;
                            case R.id.incoming_orders:
                                fragmentClass = IncomingOrderFragment.class;
                                break;
                            case R.id.staff_list:
                                fragmentClass = StaffListFragment.class;
                                break;
                            case R.id.menu_editor:
                                fragmentClass = MenuEditorFragment.class;
                                break;
                            case R.id.table_scanner:
                                fragmentClass = QRCodeTableManagerFragment.class;
                                break;
                            case R.id.printer_manager:
                                fragmentClass = PrinterOptionsFragment.class;
                                break;
                            case R.id.session_history:
                                fragmentClass = SessionHistoryListFragment.class;
                                break;
                            case R.id.logout:
                                // Session is expired, show dialog
                                new AlertDialog.Builder(MainActivity.this)
                                        .setMessage("Are you sure you want to log out?")
                                        .setPositiveButton("Log Out", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {

                                                AppState.getInstance(getApplicationContext()).cacheVipAuthStatus("");
                                                FirebaseAuth.getInstance().signOut();
                                                Intent intent = new Intent(getApplicationContext(), SplashScreenActivity.class);
                                                startActivity(intent);
                                                finish();
                                            }
                                        })
                                        .setNegativeButton("Nevermind", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                            }
                                        })
                                        .setCancelable(false)
                                        .create()
                                        .show();
                                break;
                        }

                        try {
                            fragment = (Fragment) fragmentClass.newInstance();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        // Insert the fragment by replacing any existing fragment
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
                        currentClass = fragmentClass;

                        // Highlight the selected item has been done by NavigationView
                        menuItem.setChecked(true);
                        // Set action bar title
                        setTitle(menuItem.getTitle());
                        // Close the navigation drawer
                        mDrawerLayout.closeDrawers();

                        return true;
                    }
                });


        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu_24dp);


        toolbar.setVisibility(View.VISIBLE);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (progressDialog != null && !progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void unlockScreen(String profileType, String fullName) {
        tvProfileType.setText(profileType + " : " + fullName);

        setUIState(profileType);

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().remove(fragmentManager.findFragmentById(R.id.flLockScreen)).commit();

        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    private void setUIState(String profileType) {
        if (profileType.equals("STAFF")) {
            llProfileType.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.md_green_700));
            navigationView.getMenu().findItem(R.id.staff_list).setVisible(false);
            navigationView.getMenu().findItem(R.id.menu_editor).setVisible(false);
        } else {
            llProfileType.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.md_blue_700));
            navigationView.getMenu().findItem(R.id.staff_list).setVisible(true);
            navigationView.getMenu().findItem(R.id.menu_editor).setVisible(true);
        }
    }


    @Override
    public void lockScreen() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flLockScreen, LockScreenFragment.newInstance(this)).commit();

        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);  // OPEN DRAWER
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
