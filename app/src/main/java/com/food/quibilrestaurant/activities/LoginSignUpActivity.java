package com.food.quibilrestaurant.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.food.quibilrestaurant.R;
import com.food.quibilrestaurant.data_layer.AppState;
import com.food.quibilrestaurant.data_layer.CurrentRestaurant;
import com.food.quibilrestaurant.data_layer.CurrentUser;
import com.food.quibilrestaurant.interfaces.CreateRestaurantInterface;
import com.food.quibilrestaurant.interfaces.UpdateUserInterface;
import com.food.quibilrestaurant.models.Restaurant;
import com.food.quibilrestaurant.models.User;
import com.food.quibilrestaurant.network_layer.RestaurantService;
import com.food.quibilrestaurant.ui.feature.login.LoginActivity;
import com.food.quibilrestaurant.utils.App;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.rey.material.widget.Button;
import com.rey.material.widget.EditText;

public class LoginSignUpActivity extends AppCompatActivity {
    private String TAG = "LoginSignUpActivity";

    TextView tvOrText;
    TextView tvLoginText;
    EditText etEmail;
    EditText etPassword;
    EditText etPassword2;
    Button btnSignUp;

    ProgressDialog progressDialog;

    private FirebaseAuth mAuth;

    final String[] items = new String[3];


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.activity_login_sign_up);
        final Typeface face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Light.ttf");
        App.getmFirebaseAnalytics().setCurrentScreen(this, "LoginSignupActivity", null /* class override */);

        if(AppState.getInstance(getApplicationContext()).getVipAuthStatus().equals("")){
            Intent intent = new Intent(getApplicationContext(), VipLoginActivity.class);
            startActivity(intent);
            finish();
        }

        mAuth = FirebaseAuth.getInstance();

        btnSignUp = (Button) findViewById(R.id.btnSignUp);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etPassword2 = (EditText) findViewById(R.id.etPassword2);
        tvLoginText = (TextView) findViewById(R.id.tvLoginText);
        tvOrText = (TextView) findViewById(R.id.tvOrText);


        tvOrText.setTypeface(face);
        btnSignUp.setTypeface(face);
        tvLoginText.setTypeface(face);
        etPassword.setTypeface(face);
        etPassword2.setTypeface(face);
        etEmail.setTypeface(face);

        tvLoginText.setText(Html.fromHtml("Already have an account? <b>Login</b>"));

        btnSignUp.setVisibility(View.GONE);


        etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (!editable.toString().equals("") && !etPassword.getText().toString().equals("") && !etPassword2.getText().toString().equals("")) {
                    btnSignUp.setVisibility(View.VISIBLE);
                    btnSignUp.animate().alpha(1.0f).setDuration(500).start();
                } else {
                    btnSignUp.animate().alpha(0.0f).setDuration(500).start();
                }
            }
        });

        etPassword2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (!editable.toString().equals("") && !etPassword.getText().toString().equals("") && !etEmail.getText().toString().equals("")) {
                    btnSignUp.setVisibility(View.VISIBLE);
                    btnSignUp.animate().alpha(1.0f).setDuration(500).start();
                } else {
                    btnSignUp.animate().alpha(0.0f).setDuration(500).start();
                }
            }
        });

        etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (!editable.toString().equals("") && !etEmail.getText().toString().equals("") && !etPassword2.getText().toString().equals("")) {
                    btnSignUp.setVisibility(View.VISIBLE);
                    btnSignUp.animate().alpha(1.0f).setDuration(500).start();
                } else {
                    btnSignUp.animate().alpha(0.0f).setDuration(500).start();
                }
            }
        });

        tvLoginText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });


        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Validate fields
                if (etPassword.getText().length() == 0) {
                    Toast.makeText(LoginSignUpActivity.this, "Password should not be empty", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (etPassword2.getText().length() == 0) {
                    Toast.makeText(LoginSignUpActivity.this, "Password should not be empty", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (etEmail.getText().toString().isEmpty()) {
                    Toast.makeText(LoginSignUpActivity.this, "Email should not be empty", Toast.LENGTH_SHORT).show();
                    return;
                }


                if (!etPassword.getText().toString().equals(etPassword2.getText().toString())) {
                    Toast.makeText(LoginSignUpActivity.this, "Passwords don't match",
                            Toast.LENGTH_SHORT).show();
                    return;
                }

                progressDialog = new ProgressDialog(LoginSignUpActivity.this);
                progressDialog.setMessage("Signing up");
                progressDialog.show();

                etEmail.setText(etEmail.getText().toString().replaceAll(" ", ""));

                mAuth.createUserWithEmailAndPassword(etEmail.getText().toString(), etPassword.getText().toString())
                        .addOnCompleteListener(LoginSignUpActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                progressDialog.setMessage("Signing up.");

                                if (task.isSuccessful()) {

                                    // Sign in success, update UI with the signed-in user's information
                                    Log.d("LoginSignUpActivity", "createUserWithEmail:success");
                                    final FirebaseUser user = mAuth.getCurrentUser();


                                    // Cache user data, we will sync to database later on
                                    User currentUser = new User();
                                    currentUser.id = user.getUid();
                                    currentUser.email = user.getEmail();
                                    CurrentUser.getInstance(getApplicationContext()).cacheUser(currentUser);

                                    // Create and save User document on database
                                    FirebaseFirestore.getInstance().collection("users").document(user.getUid()).set(currentUser).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            progressDialog.setMessage("Signing up..");

                                            if (task.isSuccessful()) {

                                                // Create brand new restaurant
                                                RestaurantService.getInstance(getApplicationContext()).createRetaurantFromDatabase(new CreateRestaurantInterface() {
                                                    @Override
                                                    public void onCreated(String restaurantId) {
                                                        progressDialog.setMessage("Signing up...");

                                                        // Cache new restaurant as current
                                                        Restaurant currentRestaurant = new Restaurant();
                                                        currentRestaurant.id = restaurantId;
                                                        CurrentRestaurant.getInstance(getApplicationContext()).cacheRestaurant(currentRestaurant);

                                                        // Set user and restaurant relation and cache and save to database
                                                        User currentUser = CurrentUser.getInstance(getApplicationContext()).getCache();
                                                        currentUser.ownsRestaurant = restaurantId;
                                                        CurrentUser.getInstance(getApplicationContext()).persistAndCache(currentUser, new UpdateUserInterface() {
                                                            @Override
                                                            public void onUpdated() {
                                                                progressDialog.dismiss();
                                                                Toast.makeText(LoginSignUpActivity.this, "Saved User Restaurant Relation", Toast.LENGTH_SHORT).show();

                                                                Intent intent = new Intent(getApplicationContext(), OnboardManagerActivity.class);
                                                                intent.putExtra("previousActivity", "LoginSignUpActivity");
                                                                startActivity(intent);
                                                                finish();
                                                            }

                                                            @Override
                                                            public void onError() {
                                                                progressDialog.dismiss();
                                                                Toast.makeText(LoginSignUpActivity.this, "Error Saving profile", Toast.LENGTH_SHORT).show();
                                                            }
                                                        });

                                                    }

                                                    @Override
                                                    public void onError() {
                                                        progressDialog.dismiss();
                                                        Toast.makeText(LoginSignUpActivity.this, "Error Saving restaurant", Toast.LENGTH_SHORT).show();
                                                    }
                                                });




                                            } else {
                                                Log.d(TAG, "get failed with ", task.getException());
                                            }
                                        }
                                    });


                                } else {
                                    // If sign in fails, display a message to the user.
                                    Log.w("LoginSignUpActivity", "createUserWithEmail:failure", task.getException());
                                    Toast.makeText(LoginSignUpActivity.this, task.getException().getMessage(),
                                            Toast.LENGTH_SHORT).show();
                                    progressDialog.dismiss();
                                }

                            }
                        });

            }
        });
    }
}
