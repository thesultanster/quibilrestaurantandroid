package com.food.quibilrestaurant.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.food.quibilrestaurant.R;
import com.food.quibilrestaurant.data_layer.AppState;
import com.rey.material.widget.Button;
import com.rey.material.widget.EditText;

public class VipLoginActivity extends AppCompatActivity {

    EditText etVip;
    Button btnVipLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vip_login);

        etVip = findViewById(R.id.etVip);
        btnVipLogin = findViewById(R.id.btnVipLogin);
        btnVipLogin.setVisibility(View.GONE);

        etVip.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (!editable.toString().equals("") ) {
                    btnVipLogin.setVisibility(View.VISIBLE);
                    btnVipLogin.animate().alpha(1.0f).setDuration(500).start();
                } else {
                    btnVipLogin.animate().alpha(0.0f).setDuration(500).start();
                }
            }
        });

        btnVipLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(etVip.getText().toString().equals("quibil2018")){
                    AppState.getInstance(getApplicationContext()).cacheVipAuthStatus("ACCESS");
                    Intent intent = new Intent(getApplicationContext(), LoginSignUpActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });

    }
}
