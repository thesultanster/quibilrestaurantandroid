package com.food.quibilrestaurant.activities

interface RequestPermissionsInterface {

    fun onCameraPermissionsGranted()
    fun onCameraPermissionsRejected();
}