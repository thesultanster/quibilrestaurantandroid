package com.food.quibilrestaurant.utils

import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import javax.xml.datatype.DatatypeConstants.HOURS


class DateUtils {
    companion object {
        fun convertMillisecondsToTimeStamp(millis: Long): String {
            val formatter = SimpleDateFormat("hh:mm a", Locale.US)
            formatter.timeZone = TimeZone.getDefault()
            return formatter.format(millis)
        }


    }
}
