package com.food.quibilrestaurant.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import static android.content.Context.MODE_PRIVATE;


public class AppState {
    private String TAG = "AppState";

    private DatabaseReference mDatabase;
    private Context context;
    private SharedPreferences prefs;
    private static AppState instance;

    public static AppState getInstance(Context context) {

        if (instance == null) {
            instance = new AppState(context);
        }
        return instance;
    }

    private AppState(Context context) {
        this.mDatabase = FirebaseDatabase.getInstance().getReference();
        this.context = context;
        this.prefs = context.getSharedPreferences("current_user", MODE_PRIVATE);
    }




}
