package com.food.quibilrestaurant.domain.ordermanager;

import com.food.quibilrestaurant.models.OrderedItem;

import java.util.ArrayList;

/**
 * Created by sultankhan on 8/22/18.
 */

public class IndividualOrderedItem {

    public String sessionId;
    public String restaurantId;
    public String tableId;
    public String userFullName;
    public String userProfilePicURL;
    public String state;
    public OrderedItem item;
    public long orderedAt;
    public boolean isPrinted;

    public IndividualOrderedItem() {
    }
}
