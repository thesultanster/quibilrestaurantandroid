package com.food.quibilrestaurant.domain.ordermanager

import com.food.quibilrestaurant.models.OrderedItem
import com.food.quibilrestaurant.models.Session

class OrderManagerUseCase(){

    fun bindIndividualOrderedItem(orderedItem: OrderedItem, session: Session): IndividualOrderedItem {
        val individualOrderedItem = IndividualOrderedItem()
        individualOrderedItem.sessionId = session.id
        individualOrderedItem.item = orderedItem
        individualOrderedItem.orderedAt = orderedItem.orderedAt
        individualOrderedItem.restaurantId = session.restaurantId
        individualOrderedItem.tableId = session.tableId
        individualOrderedItem.userFullName = session.userFullName
        individualOrderedItem.userProfilePicURL = session.userProfilePicURL
        individualOrderedItem.isPrinted = orderedItem.isPrinted
        return individualOrderedItem
    }
}