package com.food.quibilrestaurant.adapters;

import android.content.Context;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.food.quibilrestaurant.R;
import com.food.quibilrestaurant.interfaces.ItemTouchHelperAdapter;
import com.food.quibilrestaurant.interfaces.OnMenuCategorySelectedInterface;
import com.food.quibilrestaurant.interfaces.OnStartDragListener;
import com.food.quibilrestaurant.interfaces.UpdateMenuCategoryInterface;
import com.food.quibilrestaurant.models.MenuCategory;
import com.food.quibilrestaurant.network_layer.MenuService;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MenuCategoriesRecyclerAdapter extends RecyclerView.Adapter<MenuCategoriesRecyclerAdapter.MyViewHolder>  implements ItemTouchHelperAdapter {

    // emptyList takes care of null pointer exception
    List<MenuCategory> menuCategories = Collections.emptyList();
    LayoutInflater inflator;
    Context context;
    OnMenuCategorySelectedInterface onMenuCategorySelectedInterface;
    final OnStartDragListener mDragStartListener;
    // Start with first item selected
    private int focusedItem = -1;


    public MenuCategoriesRecyclerAdapter(Context context, List<MenuCategory> menuCategories, OnStartDragListener dragStartListener, OnMenuCategorySelectedInterface onMenuCategorySelectedInterface) {
        this.context = context;
        this.mDragStartListener = dragStartListener;
        this.inflator = LayoutInflater.from(context);
        this.menuCategories = menuCategories;
        this.onMenuCategorySelectedInterface = onMenuCategorySelectedInterface;
    }

    public void addRow(MenuCategory menuCategory) {
        menuCategories.add(menuCategory);

            boolean swapped = true;
            while (swapped) {
                swapped = false;
                for(int i=1; i< menuCategories.size(); i++) {
                    if(menuCategories.get(i-1).index > menuCategories.get(i).index) {

                        MenuCategory temp = menuCategories.get(i-1);
                        menuCategories.set(i-1, menuCategories.get(i));
                        menuCategories.set(i, temp);
                        swapped = true;
                    }
                }
            }


        notifyDataSetChanged();
    }

    public void updateRow(MenuCategory menuCategory, int position){
        menuCategories.set(position, menuCategory);
        notifyItemChanged(position);
    }
    // Called when the recycler view needs to create a new row
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        final View view = inflator.inflate(R.layout.row_menu_category, parent, false);
        final MyViewHolder holder = new MyViewHolder(view, context, new MyViewHolder.MyViewHolderClicks() {

            public void rowClick(View caller, int position) {
                Log.d("rowClick", "rowClicks");
                // Set selected postition
                focusedItem = position;

                // Pass selected category down the interface
                onMenuCategorySelectedInterface.onCategorySelected(position, menuCategories.get(position));

                // Update rows
                Handler handler = new Handler();
                final Runnable r = new Runnable() {
                    public void run() {
                        notifyDataSetChanged(); // notify adapter
                    }
                };
                handler.post(r);

            }

            @Override
            public void edit(View caller, int position) {
                onMenuCategorySelectedInterface.onEdit(position, menuCategories.get(position));
            }

            @Override
            public void dragRow(View caller, int position) {


            }


        });
        return holder;
    }

    // Setting up the data for each row
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        if(focusedItem == position){
            holder.rlRow.setBackgroundColor(ContextCompat.getColor(context, R.color.md_grey_200));
        } else {
            holder.rlRow.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
        }

        MenuCategory current = menuCategories.get(position);
        holder.tvCategoryTitle.setText(current.name);

        holder.ivHandle.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEventCompat.getActionMasked(event) ==
                        MotionEvent.ACTION_DOWN) {
                    mDragStartListener.onStartDrag(holder);
                }
                return false;
            }
        });

    }


    @Override
    public int getItemCount() {
        return menuCategories.size();
    }

    // Created my custom view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvCategoryTitle;
        ImageView ivEdit;
        ImageView ivHandle;
        RelativeLayout rlRow;
        Context context;


        public MyViewHolderClicks mListener;

        // itemView will be my own custom layout View of the row
        public MyViewHolder(View itemView, Context context, MyViewHolderClicks listener) {
            super(itemView);
            mListener = listener;
            this.context = context;

            ivHandle = itemView.findViewById(R.id.ivHandle);
            ivEdit = itemView.findViewById(R.id.ivEdit);
            tvCategoryTitle = (TextView) itemView.findViewById(R.id.tvCategoryTitle);
            rlRow = (RelativeLayout) itemView.findViewById(R.id.rlRow);

            itemView.setOnClickListener(this);
            ivEdit.setOnClickListener(this);
            ivHandle.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivHandle:
                    mListener.dragRow(v, getAdapterPosition());
                    break;
                case R.id.ivEdit:
                    mListener.edit(v, getAdapterPosition());
                    break;
                default:
                    rlRow.setBackgroundColor(ContextCompat.getColor(context, R.color.md_grey_200));
                    mListener.rowClick(v, getAdapterPosition());
                    break;
            }
        }

        public interface MyViewHolderClicks {
            void rowClick(View caller, int position);
            void edit(View caller, int position);
            void dragRow(View caller, int position);
        }
    }


    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(menuCategories, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(menuCategories, i, i - 1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);

        // Update positions in adapter and database
        for(int i = 0; i < getItemCount(); i++){
            MenuCategory menuCategory = menuCategories.get(i);
            menuCategory.index=i;
            menuCategories.set(i, menuCategory);
            MenuService.getInstance(context).updateMenuCategory(menuCategory, new UpdateMenuCategoryInterface() {
                @Override
                public void onUpdated() {

                }

                @Override
                public void onError() {

                }
            });
        }
    }

    @Override
    public void onItemDismiss(int position) {

    }


}