package com.food.quibilrestaurant.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.food.quibilrestaurant.R;
import com.food.quibilrestaurant.interfaces.OnMenuCategorySelectedInterface;
import com.food.quibilrestaurant.interfaces.OnSessionHistorySelectedInterface;
import com.food.quibilrestaurant.models.Session;

import org.ocpsoft.prettytime.PrettyTime;

import java.text.NumberFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class SessionHistoryItemsRecyclerAdapter extends RecyclerView.Adapter<SessionHistoryItemsRecyclerAdapter.MyViewHolder> {

    // emptyList takes care of null pointer exception
    List<Session> data = Collections.emptyList();
    LayoutInflater inflator;
    Context context;

    OnSessionHistorySelectedInterface onSessionHistorySelectedInterface;

    public SessionHistoryItemsRecyclerAdapter(Context context, List<Session> data, OnSessionHistorySelectedInterface onSessionHistorySelectedInterface) {
        this.context = context;
        this.inflator = LayoutInflater.from(context);
        this.data = data;
        this.onSessionHistorySelectedInterface = onSessionHistorySelectedInterface;
    }

    public void addRow(Session menuItem) {
        data.add(menuItem);
        notifyItemInserted(getItemCount() - 1);
    }

    public void clearRows() {
        data.clear();
        notifyDataSetChanged();
    }

    // Called when the recycler view needs to create a new row
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        final View view = inflator.inflate(R.layout.row_session_history, parent, false);
        final MyViewHolder holder = new MyViewHolder(view, context, new MyViewHolder.MyViewHolderClicks() {

            public void rowClick(View caller, int position) {
                Log.d("rowClick", "rowClicks");

                onSessionHistorySelectedInterface.onHistorySelected(position, data.get(position));
            }


        });
        return holder;
    }

    // Setting up the data for each row
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Session current = data.get(position);

        PrettyTime p = new PrettyTime();
        holder.tvTime.setText(p.format(new Date(current.updatedAt)));

        // Format and set price
        NumberFormat n = NumberFormat.getCurrencyInstance(Locale.US);
        String s = n.format(current.totalPaymentAmount  / 100.0);
        holder.tvTotalAmount.setText(s);

        holder.tvTitle.setText(current.id);
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    // Created my custom view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvTotalAmount;
        TextView tvTitle;
        TextView tvTime;
        LinearLayout rlRow;
        Context context;

        public MyViewHolderClicks mListener;

        // itemView will be my own custom layout View of the row
        public MyViewHolder(View itemView, Context context, MyViewHolderClicks listener) {
            super(itemView);
            this.mListener = listener;
            this.context = context;

            tvTotalAmount = itemView.findViewById(R.id.tvTotalAmount);
            tvTime = itemView.findViewById(R.id.tvTime);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            rlRow = itemView.findViewById(R.id.rlRow);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                default:
                    mListener.rowClick(v, getAdapterPosition());
                    break;
            }
        }

        public interface MyViewHolderClicks {
            void rowClick(View caller, int position);
        }
    }


}