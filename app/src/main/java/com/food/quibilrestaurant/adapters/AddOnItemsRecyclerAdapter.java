package com.food.quibilrestaurant.adapters;

import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.food.quibilrestaurant.R;
import com.food.quibilrestaurant.dialogs.AddAddonItemDialog;
import com.food.quibilrestaurant.interfaces.CreateAddonItemInterface;
import com.food.quibilrestaurant.interfaces.OnAddOnItemSelectedInterface;
import com.food.quibilrestaurant.interfaces.UserEnteredNewAddonItemInterface;
import com.food.quibilrestaurant.models.AddOnItem;
import com.food.quibilrestaurant.network_layer.MenuService;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class AddOnItemsRecyclerAdapter extends RecyclerView.Adapter<AddOnItemsRecyclerAdapter.MyViewHolder> {

    // emptyList takes care of null pointer exception
    List<AddOnItem> addOnItems = Collections.emptyList();
    List<Integer> itemCount = Collections.emptyList();
    LayoutInflater inflator;
    Context context;

    OnAddOnItemSelectedInterface addOnItemSelectedInterface;


    public AddOnItemsRecyclerAdapter(Context context, List<AddOnItem> addOnItems, OnAddOnItemSelectedInterface addOnItemSelectedInterface) {
        this.context = context;
        this.addOnItemSelectedInterface = addOnItemSelectedInterface;
        this.addOnItems = addOnItems;
        this.itemCount = new ArrayList<>();
        inflator = LayoutInflater.from(context);
    }

    public void addRow(AddOnItem addOnItem) {
        addOnItems.add(addOnItem);
        itemCount.add(0);
        notifyItemInserted(getItemCount() - 1);
    }

    public void clearRows() {
        addOnItems.clear();
        notifyDataSetChanged();
    }

    public void updateRow(AddOnItem addOnItem, int position){
        addOnItems.set(position, addOnItem);
        notifyItemChanged(position);
    }

    // Called when the recycler view needs to create a new row
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        final View view = inflator.inflate(R.layout.row_addon_item, parent, false);
        final MyViewHolder holder = new MyViewHolder(view, context, new MyViewHolder.MyViewHolderClicks() {

            public void rowClick(View caller, int position) {
                Log.d("rowClick", "rowClicks");

                // Pass selected category down the interface
                // onMenuItemSelectedInterface.onItemSelected(position, addOnItems.get(position));


            }

            @Override
            public void edit(final View caller, int position) {
                final AddOnItem currentItem = addOnItems.get(position);
                addOnItemSelectedInterface.onEditItem(position, currentItem );
            }


        });
        return holder;
    }

    // Setting up the data for each row
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        AddOnItem current = addOnItems.get(position);

        NumberFormat n = NumberFormat.getCurrencyInstance(Locale.US);
        String s = n.format(current.price / 100.0);

        holder.tvPrice.setText("+"+s);
        holder.tvCategoryTitle.setText(current.name);
    }


    @Override
    public int getItemCount() {
        return addOnItems.size();
    }

    // Created my custom view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvCategoryTitle;
        ImageView ivEdit;
        TextView tvPrice;
        RelativeLayout rlRow;
        Context context;

        public MyViewHolderClicks mListener;

        // itemView will be my own custom layout View of the row
        public MyViewHolder(View itemView, Context context, MyViewHolderClicks listener) {
            super(itemView);
            this.mListener = listener;
            this.context = context;

            ivEdit = itemView.findViewById(R.id.ivEdit);
            tvCategoryTitle =  itemView.findViewById(R.id.tvCategoryTitle);
            rlRow = itemView.findViewById(R.id.rlRow);
            tvPrice = itemView.findViewById(R.id.itemPrice);

            itemView.setOnClickListener(this);
            ivEdit.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivEdit:
                    mListener.edit(v, getAdapterPosition());
                    break;
                default:
                    mListener.rowClick(v, getAdapterPosition());
                    break;
            }
        }

        public interface MyViewHolderClicks {
            void rowClick(View caller, int position);
            void edit(View caller, int position);
        }
    }


}