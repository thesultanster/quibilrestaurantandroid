package com.food.quibilrestaurant.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.food.quibilrestaurant.R;
import com.food.quibilrestaurant.interfaces.UserEnteredNewMenuCategoryInterface;
import com.food.quibilrestaurant.interfaces.UserUpdatedMenuCategoryInterface;
import com.food.quibilrestaurant.models.AddOnItem;
import com.food.quibilrestaurant.models.MenuCategory;

/**
 * Created by sultankhan on 7/28/18.
 */

public  class UpdateMenuCategoryDialog extends DialogFragment {


    Button btnCancel;
    Button btnAddCategory;
    EditText edtMenuCateogry;

    MenuCategory menuCategory;

    private UserUpdatedMenuCategoryInterface userUpdatedMenuCategoryInterface;


    public static UpdateMenuCategoryDialog newInstance(MenuCategory menuCategory, UserUpdatedMenuCategoryInterface userUpdatedMenuCategoryInterface) {
        UpdateMenuCategoryDialog frag = new UpdateMenuCategoryDialog();
        frag.userUpdatedMenuCategoryInterface = userUpdatedMenuCategoryInterface;
        Bundle args = new Bundle();
        args.putSerializable("menuCategory", menuCategory);
        frag.setArguments(args);
        return frag;
    }

    // This is a workaround for the strange behavior of onCreateView (which doesn't show dialog's layout)
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View dialogView = inflater.inflate(R.layout.dialog_add_menu_category, null);
        dialogBuilder.setView(dialogView);


        menuCategory = (MenuCategory) getArguments().getSerializable("menuCategory");

        initSubViews(dialogView);

        populateSubViews();

        setCancelable(false);

        return dialogBuilder.create();
    }


    private void initSubViews(View rootView) {
        btnAddCategory = rootView.findViewById(R.id.btnAddCategory);
        btnCancel = rootView.findViewById(R.id.btnCancel);
        edtMenuCateogry = rootView.findViewById(R.id.edtMenuCateogry);

        btnAddCategory.setText("Update Category");
        edtMenuCateogry.setText(menuCategory.name);

        btnAddCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuCategory.name = edtMenuCateogry.getText().toString();
                userUpdatedMenuCategoryInterface.userUpdatedMenuCategory(menuCategory);
                dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
            }
        });


    }

    private void populateSubViews() {

    }
}
