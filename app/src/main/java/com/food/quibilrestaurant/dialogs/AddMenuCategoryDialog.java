package com.food.quibilrestaurant.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.food.quibilrestaurant.R;
import com.food.quibilrestaurant.activities.MainActivity;
import com.food.quibilrestaurant.interfaces.UserEnteredNewMenuCategoryInterface;

/**
 * Created by sultankhan on 7/28/18.
 */

public  class AddMenuCategoryDialog extends DialogFragment {


    Button btnCancel;
    Button btnAddCategory;
    EditText edtMenuCateogry;

    private UserEnteredNewMenuCategoryInterface userEnteredNewMenuCategoryInterface;


    public static AddMenuCategoryDialog newInstance(String title, UserEnteredNewMenuCategoryInterface userEnteredNewMenuCategoryInterface) {
        AddMenuCategoryDialog frag = new AddMenuCategoryDialog();
        frag.userEnteredNewMenuCategoryInterface = userEnteredNewMenuCategoryInterface;
        Bundle args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);
        return frag;
    }

    // This is a workaround for the strange behavior of onCreateView (which doesn't show dialog's layout)
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View dialogView = inflater.inflate(R.layout.dialog_add_menu_category, null);
        dialogBuilder.setView(dialogView);

        initSubViews(dialogView);

        populateSubViews();

        setCancelable(false);

        return dialogBuilder.create();
    }


    private void initSubViews(View rootView) {
        btnAddCategory = rootView.findViewById(R.id.btnAddCategory);
        btnCancel = rootView.findViewById(R.id.btnCancel);
        edtMenuCateogry = rootView.findViewById(R.id.edtMenuCateogry);

        btnAddCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userEnteredNewMenuCategoryInterface.userEnteredNewMenuCategory(edtMenuCateogry.getText().toString());
                dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
            }
        });


    }

    private void populateSubViews() {

    }
}
