package com.food.quibilrestaurant.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.food.quibilrestaurant.R;
import com.food.quibilrestaurant.interfaces.UserEnteredNewAddonItemInterface;
import com.food.quibilrestaurant.interfaces.UserEnteredNewMenuItemInterface;
import com.food.quibilrestaurant.models.AddOnItem;
import com.food.quibilrestaurant.models.Menu;
import com.food.quibilrestaurant.models.MenuCategory;
import com.food.quibilrestaurant.models.MenuItem;

public  class AddAddonItemDialog extends DialogFragment {


    Button btnAddItem;
    Button btnCancel;
    EditText edtMenuItem;
    EditText edtMenuPrice;

    MenuItem menuItem;

    private UserEnteredNewAddonItemInterface userEnteredNewAddonItemInterface;


    public static AddAddonItemDialog newInstance( MenuItem menuItem, UserEnteredNewAddonItemInterface userEnteredNewAddonItemInterface) {
        AddAddonItemDialog frag = new AddAddonItemDialog();
        frag.userEnteredNewAddonItemInterface = userEnteredNewAddonItemInterface;
        Bundle args = new Bundle();
        args.putSerializable("menuItem", menuItem);
        frag.setArguments(args);
        return frag;
    }

    // This is a workaround for the strange behavior of onCreateView (which doesn't show dialog's layout)
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View dialogView = inflater.inflate(R.layout.dialog_add_addon_item, null);
        dialogBuilder.setView(dialogView);

        menuItem = (MenuItem) getArguments().getSerializable("menuItem");

        initSubViews(dialogView);

        populateSubViews();

        setCancelable(false);

        return dialogBuilder.create();
    }


    private void initSubViews(View rootView) {
        btnAddItem = rootView.findViewById(R.id.btnAddItem);
        btnCancel = rootView.findViewById(R.id.btnCancel);
        edtMenuItem = rootView.findViewById(R.id.edtMenuItem);
        edtMenuPrice = rootView.findViewById(R.id.edtMenuPrice);

        btnAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AddOnItem addOnItem = new AddOnItem();
                addOnItem.name = edtMenuItem.getText().toString();
                addOnItem.menuItemId = menuItem.id;
                addOnItem.price = Integer.parseInt(edtMenuPrice.getText().toString());

                userEnteredNewAddonItemInterface.userEnteredNewAddonItem(addOnItem);
                dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
            }
        });


    }

    private void populateSubViews() {

    }
}
