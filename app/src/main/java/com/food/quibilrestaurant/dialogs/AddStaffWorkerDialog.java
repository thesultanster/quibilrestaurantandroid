package com.food.quibilrestaurant.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.food.quibilrestaurant.R;
import com.food.quibilrestaurant.data_layer.CurrentRestaurant;
import com.food.quibilrestaurant.interfaces.CanPinBeMadeInterface;
import com.food.quibilrestaurant.interfaces.UserEnteredNewMenuCategoryInterface;
import com.food.quibilrestaurant.interfaces.UserEnteredNewStaffWorkerInterface;
import com.food.quibilrestaurant.models.StaffWorker;
import com.food.quibilrestaurant.network_layer.RestaurantService;
import com.food.quibilrestaurant.network_layer.UserService;

/**
 * Created by sultankhan on 7/28/18.
 */

public class AddStaffWorkerDialog extends DialogFragment {
    String TAG = "AddStaffWorkerDialog";

    Button btnCancel;
    Button btnAddWorker;
    EditText edtStaffName;
    EditText edtPin;

    String restaurantId;

    private UserEnteredNewStaffWorkerInterface userEnteredNewStaffWorkerInterface;


    public static AddStaffWorkerDialog newInstance(String restaurantId, UserEnteredNewStaffWorkerInterface userEnteredNewStaffWorkerInterface) {
        AddStaffWorkerDialog frag = new AddStaffWorkerDialog();
        frag.userEnteredNewStaffWorkerInterface = userEnteredNewStaffWorkerInterface;
        Bundle args = new Bundle();
        args.putString("restaurantId", restaurantId);
        frag.setArguments(args);
        return frag;
    }

    // This is a workaround for the strange behavior of onCreateView (which doesn't show dialog's layout)
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View dialogView = inflater.inflate(R.layout.dialog_add_staff_worker, null);
        dialogBuilder.setView(dialogView);

        restaurantId = getArguments().getString("restaurantId");

        initSubViews(dialogView);

        populateSubViews();

        setCancelable(false);

        return dialogBuilder.create();
    }


    private void initSubViews(View rootView) {
        btnAddWorker = rootView.findViewById(R.id.btnAddWorker);
        btnCancel = rootView.findViewById(R.id.btnCancel);
        edtPin = rootView.findViewById(R.id.edtPin);
        edtStaffName = rootView.findViewById(R.id.edtStaffName);

        btnAddWorker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StaffWorker staffWorker = new StaffWorker();
                staffWorker.name = edtStaffName.getText().toString();
                staffWorker.pin = edtPin.getText().toString();
                staffWorker.worksAtRestaurantId = CurrentRestaurant.getInstance(getContext()).getCache().id;

                userEnteredNewStaffWorkerInterface.userEnteredNewStaffWorkerCategory(staffWorker);
                dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
            }
        });



        edtPin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length() == 4){
                    UserService.getInstance(getContext()).checkIfPinHasNotBeenChosenByPreviousUsers(restaurantId, editable.toString(), new CanPinBeMadeInterface() {
                        @Override
                        public void pinAvailable() {
                            Log.d(TAG,"Pin available");
                            btnAddWorker.setEnabled(true);
                        }

                        @Override
                        public void pinTaken() {
                            edtPin.setError("Pin taken by another user");
                            btnAddWorker.setEnabled(false);
                        }
                    });
                }
            }
        });
    }

    private void populateSubViews() {

    }
}
