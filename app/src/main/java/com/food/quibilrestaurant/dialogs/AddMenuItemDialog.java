package com.food.quibilrestaurant.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.food.quibilrestaurant.R;
import com.food.quibilrestaurant.interfaces.UserEnteredNewMenuItemInterface;
import com.food.quibilrestaurant.models.MenuCategory;
import com.food.quibilrestaurant.models.MenuItem;
import com.food.quibilrestaurant.utils.NumberTextWatcher;

/**
 * Created by sultankhan on 7/28/18.
 */

public  class AddMenuItemDialog extends DialogFragment {


    Button btnAddItem;
    Button btnCancel;
    EditText edtMenuItem;
    EditText edtMenuPrice;

    MenuCategory menuCategory;

    private UserEnteredNewMenuItemInterface userEnteredNewMenuItemInterface;


    public static AddMenuItemDialog newInstance(MenuCategory menuCategory, UserEnteredNewMenuItemInterface userEnteredNewMenuItemInterface) {
        AddMenuItemDialog frag = new AddMenuItemDialog();
        frag.userEnteredNewMenuItemInterface = userEnteredNewMenuItemInterface;
        Bundle args = new Bundle();
        args.putSerializable("menuCategory", menuCategory);
        frag.setArguments(args);
        return frag;
    }

    // This is a workaround for the strange behavior of onCreateView (which doesn't show dialog's layout)
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View dialogView = inflater.inflate(R.layout.dialog_add_menu_item, null);
        dialogBuilder.setView(dialogView);

        menuCategory = (MenuCategory) getArguments().getSerializable("menuCategory");

        initSubViews(dialogView);

        populateSubViews();

        setCancelable(false);

        return dialogBuilder.create();
    }


    private void initSubViews(View rootView) {
        btnAddItem = rootView.findViewById(R.id.btnAddItem);
        btnCancel = rootView.findViewById(R.id.btnCancel);
        edtMenuItem = rootView.findViewById(R.id.edtMenuItem);
        edtMenuPrice = rootView.findViewById(R.id.edtMenuPrice);

        edtMenuPrice.addTextChangedListener(new NumberTextWatcher(edtMenuPrice));

        btnAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MenuItem menuItem = new MenuItem();
                menuItem.name = edtMenuItem.getText().toString();
                menuItem.categoryId = menuCategory.id;
                menuItem.restaurantId = menuCategory.restaurantId;
                menuItem.price = (int)(((Double.parseDouble(edtMenuPrice.getText().toString().replace("$", "")))) * 100);

                userEnteredNewMenuItemInterface.userEnteredNewMenuItem(menuItem);
                dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
            }
        });


    }

    private void populateSubViews() {

    }
}
