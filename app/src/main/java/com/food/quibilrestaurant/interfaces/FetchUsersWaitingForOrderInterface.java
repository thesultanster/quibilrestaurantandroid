package com.food.quibilrestaurant.interfaces;


import com.food.quibilrestaurant.models.CheckedInUser;
import com.food.quibilrestaurant.models.Session;
import com.food.quibilrestaurant.models.WaitingForOrder;

/**
 * Created by sultankhan on 8/1/17.
 */

public interface FetchUsersWaitingForOrderInterface {
    void onDataFetched(Session session);
    void clearCurrentOrders();
    void onError();
    void noPendingOrders();
}
