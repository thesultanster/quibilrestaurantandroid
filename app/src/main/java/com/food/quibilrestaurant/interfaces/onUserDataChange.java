package com.food.quibilrestaurant.interfaces;


import com.food.quibilrestaurant.models.User;

/**
 * Created by sultankhan on 7/22/17.
 */

public interface onUserDataChange {
    void onDataSaved(User user);
    void onError();
}
