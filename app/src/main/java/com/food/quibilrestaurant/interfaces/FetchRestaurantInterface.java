package com.food.quibilrestaurant.interfaces;


import com.food.quibilrestaurant.models.Restaurant;
import com.food.quibilrestaurant.models.User;

/**
 * Created by sultankhan on 8/1/17.
 */

public interface FetchRestaurantInterface {
    void onDataFetched(Restaurant restaurant);
    void onError();

}
