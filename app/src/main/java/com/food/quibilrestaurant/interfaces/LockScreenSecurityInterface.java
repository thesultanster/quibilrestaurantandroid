package com.food.quibilrestaurant.interfaces;


public interface LockScreenSecurityInterface {
    void unlockScreen(String profileType, String fullName);
    void lockScreen();
}
