package com.food.quibilrestaurant.interfaces;


import com.food.quibilrestaurant.models.AddOnItem;

public interface OnAddOnItemSelectedInterface {
    void onEditItem(int position, AddOnItem addOnItem);
}
