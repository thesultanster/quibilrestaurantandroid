package com.food.quibilrestaurant.interfaces;


import com.food.quibilrestaurant.models.MenuCategory;
import com.food.quibilrestaurant.models.Session;

/**
 * Created by sultankhan on 8/1/17.
 */

public interface OnSessionHistorySelectedInterface {
    void onHistorySelected(int position, Session session);
}
