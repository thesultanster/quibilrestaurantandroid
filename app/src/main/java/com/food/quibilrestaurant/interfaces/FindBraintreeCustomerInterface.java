package com.food.quibilrestaurant.interfaces;


/**
 * Created by sultankhan on 8/1/17.
 */

public interface FindBraintreeCustomerInterface {
    void onFetched(String paymentToken);
    void onError();
}
