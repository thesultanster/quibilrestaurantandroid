package com.food.quibilrestaurant.interfaces;


import com.food.quibilrestaurant.models.MenuCategory;

/**
 * Created by sultankhan on 8/1/17.
 */

public interface OnMenuCategorySelectedInterface {
    void onCategorySelected(int position, MenuCategory menuCategory);
    void onEdit(int position, MenuCategory menuCategory);
}
