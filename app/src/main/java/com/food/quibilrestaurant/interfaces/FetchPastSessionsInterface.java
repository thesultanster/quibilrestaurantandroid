package com.food.quibilrestaurant.interfaces;


import com.food.quibilrestaurant.models.Session;

public interface FetchPastSessionsInterface {
    void onDataFetched(Session session);
    void onClearList();
    void onError();

}
