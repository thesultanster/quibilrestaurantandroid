package com.food.quibilrestaurant.interfaces;


import com.food.quibilrestaurant.models.User;

/**
 * Created by sultankhan on 8/1/17.
 */

public interface FetchUserInterface {
    void onDataFetched(User user);
    void onUserDataNotFound();

}
