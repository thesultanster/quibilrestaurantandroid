package com.food.quibilrestaurant.interfaces;


/**
 * Created by sultankhan on 8/1/17.
 */

public interface DeleteStaffWorkerInterface {
    void onDeleted();
    void onError();
}
