package com.food.quibilrestaurant.interfaces;

public interface UpdateMenuCategoryInterface {
    void onUpdated();
    void onError();
}
