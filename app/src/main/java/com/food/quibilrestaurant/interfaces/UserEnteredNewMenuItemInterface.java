package com.food.quibilrestaurant.interfaces;


import com.food.quibilrestaurant.models.MenuItem;

/**
 * Created by sultankhan on 8/1/17.
 */

public interface UserEnteredNewMenuItemInterface {
    void userEnteredNewMenuItem(MenuItem menuItem);
}
