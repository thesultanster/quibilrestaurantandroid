package com.food.quibilrestaurant.interfaces;


/**
 * Created by sultankhan on 8/1/17.
 */

public interface CreateRestaurantInterface {
    void onCreated(String restaurantId);
    void onError();

}
