package com.food.quibilrestaurant.interfaces;

public interface CreateAddonItemInterface {
    void onCreated();
    void onError();
}
