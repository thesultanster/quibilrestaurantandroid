package com.food.quibilrestaurant.interfaces;


import com.food.quibilrestaurant.models.MenuCategory;
import com.food.quibilrestaurant.models.Restaurant;

/**
 * Created by sultankhan on 8/1/17.
 */

public interface FetchRestaurantMenuCategoriesInterface {
    void onDataFetched(MenuCategory menuCategory);
    void onError();

}
