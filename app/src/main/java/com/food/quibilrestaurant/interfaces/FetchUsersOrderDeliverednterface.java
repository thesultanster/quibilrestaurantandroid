package com.food.quibilrestaurant.interfaces;


import com.food.quibilrestaurant.models.OrderDelivered;
import com.food.quibilrestaurant.models.Session;
import com.food.quibilrestaurant.models.WaitingForOrder;

/**
 * Created by sultankhan on 8/1/17.
 */

public interface FetchUsersOrderDeliverednterface {
    void onDataFetched(Session session);
    void clearDeliveredOrders();
    void onError();
    void noPendingOrders();
}
