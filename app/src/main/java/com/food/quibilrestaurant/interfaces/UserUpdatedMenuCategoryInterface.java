package com.food.quibilrestaurant.interfaces;


import com.food.quibilrestaurant.models.MenuCategory;

/**
 * Created by sultankhan on 8/1/17.
 */

public interface UserUpdatedMenuCategoryInterface {
    void userUpdatedMenuCategory(MenuCategory menuCategory);
}
