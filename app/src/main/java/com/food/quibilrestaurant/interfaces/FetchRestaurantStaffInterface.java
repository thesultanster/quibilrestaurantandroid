package com.food.quibilrestaurant.interfaces;


import com.food.quibilrestaurant.models.MenuCategory;
import com.food.quibilrestaurant.models.StaffWorker;

/**
 * Created by sultankhan on 8/1/17.
 */

public interface FetchRestaurantStaffInterface {
    void onDataFetched(StaffWorker staffWorker);
    void onError();

}
