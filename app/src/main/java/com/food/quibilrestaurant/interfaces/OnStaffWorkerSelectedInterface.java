package com.food.quibilrestaurant.interfaces;


import com.food.quibilrestaurant.models.MenuCategory;
import com.food.quibilrestaurant.models.StaffWorker;

/**
 * Created by sultankhan on 8/1/17.
 */

public interface OnStaffWorkerSelectedInterface {
    void onCategorySelected(int position, StaffWorker staffWorker);
    void onUserWantsToDeleteStaff(int position, StaffWorker staffWorker);
}
