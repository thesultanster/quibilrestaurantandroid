package com.food.quibilrestaurant.interfaces;


import com.food.quibilrestaurant.models.CheckedInUser;
import com.food.quibilrestaurant.models.Session;
import com.food.quibilrestaurant.models.User;

/**
 * Created by sultankhan on 8/1/17.
 */

public interface FetchCheckedInUsersInterface {
    void onDataFetched(Session checkedInUser);
    void clearCurrentUsers();
    void onError();
    void onNoOneCheckedIn();

}
