package com.food.quibilrestaurant.interfaces;


import com.food.quibilrestaurant.models.Menu;
import com.food.quibilrestaurant.models.User;

/**
 * Created by sultankhan on 8/1/17.
 */

public interface RestaurantMenuReceivedInterface {
    void onDataFetched(Menu menu);
    void onMenuNotFound();

}
