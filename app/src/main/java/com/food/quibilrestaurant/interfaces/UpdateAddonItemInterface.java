package com.food.quibilrestaurant.interfaces;

public interface UpdateAddonItemInterface {
    void onUpdated();
    void onError();
}
