package com.food.quibilrestaurant.interfaces;

import android.support.v7.widget.RecyclerView;

/**
 * Created by sultankhan on 9/3/18.
 */
public interface OnStartDragListener {

    /**
     * Called when a view is requesting a start of a drag.
     *
     * @param viewHolder The holder of the view to drag.
     */
    void onStartDrag(RecyclerView.ViewHolder viewHolder);
}