package com.food.quibilrestaurant.interfaces;


import com.food.quibilrestaurant.domain.ordermanager.IndividualOrderedItem;

/**
 * Created by sultankhan on 8/1/17.
 */

public interface OnWaitingForOrderSelectedInterface {
    void onSelected(int position, IndividualOrderedItem individualOrderedItem);
    void onOrderNeedsToBePrinted(int position, IndividualOrderedItem individualOrderedItem);
}
