package com.food.quibilrestaurant.interfaces;


import com.food.quibilrestaurant.models.BraintreeCustomer;

/**
 * Created by sultankhan on 8/1/17.
 */

public interface FetchBraintreeCustomerIdInterface {
    void onFetched(BraintreeCustomer braintreeCustomer);
    void onError();
}
