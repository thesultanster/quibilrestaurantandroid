package com.food.quibilrestaurant.interfaces;


import com.food.quibilrestaurant.models.AddOnItem;
import com.food.quibilrestaurant.models.MenuItem;


public interface UserEnteredNewAddonItemInterface {
    void userEnteredNewAddonItem(AddOnItem addOnItem);
}
