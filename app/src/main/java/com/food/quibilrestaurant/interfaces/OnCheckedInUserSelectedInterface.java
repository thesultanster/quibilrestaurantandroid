package com.food.quibilrestaurant.interfaces;


import com.food.quibilrestaurant.models.CheckedInUser;
import com.food.quibilrestaurant.models.Session;
import com.food.quibilrestaurant.models.StaffWorker;

/**
 * Created by sultankhan on 8/1/17.
 */

public interface OnCheckedInUserSelectedInterface {
    void onSelected(int position, Session session);
    void onActionSelected(int position, Session session);
}
