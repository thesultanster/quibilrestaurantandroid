package com.food.quibilrestaurant.interfaces;


/**
 * Created by sultankhan on 8/1/17.
 */

public interface UpdateLockScreenInterface {
    void onUpdated();
    void onError();

}
