package com.food.quibilrestaurant.interfaces;


import com.food.quibilrestaurant.models.Menu;

/**
 * Created by sultankhan on 8/1/17.
 */

public interface RestaurantMenuAddCategoryInterface {
    void onCategoryAdded();
    void onMenuNotFound();

}
