package com.food.quibilrestaurant.interfaces;

import com.food.quibilrestaurant.models.AddOnItem;

public interface FetchRestaurantItemAddOnsInterface {
    void onDataFetched(AddOnItem addOnItem);
    void onError();

}
