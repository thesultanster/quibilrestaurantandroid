package com.food.quibilrestaurant.interfaces;


import com.food.quibilrestaurant.models.Restaurant;

/**
 * Created by sultankhan on 8/1/17.
 */

public interface UpdateRestaurantInterface {
    void onUpdated();
    void onError();

}
