package com.food.quibilrestaurant.interfaces;


import com.food.quibilrestaurant.models.StaffWorker;

/**
 * Created by sultankhan on 8/1/17.
 */

public interface UserEnteredNewStaffWorkerInterface {
    void userEnteredNewStaffWorkerCategory(StaffWorker staffWorker);
}
