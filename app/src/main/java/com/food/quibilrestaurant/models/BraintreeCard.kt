package com.food.quibilrestaurant.models


import java.io.Serializable

class BraintreeCard : Serializable {
    var token: String = ""
    var maskedNumber: String = ""
}
