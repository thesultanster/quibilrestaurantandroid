package com.food.quibilrestaurant.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by sultankhan on 7/24/18.
 */

public class Restaurant implements Serializable{

    public String id="";
    public String name="";
    public String address="";
    public String phoneNumber="";
    public String profilePictureUrl="";

    @Override
    public String toString() {
        String fields = this.getClass().getSimpleName() + ":\n{ \n"
                + "\tid = " + id + ",\n "
                + "\tname = " + name + ",\n "
                + "\taddress = " + address + ",\n "
                + "\tphoneNumber = " + phoneNumber + ",\n "
                + "\tprofilePictureUrl = " + profilePictureUrl + ",\n "
                + "}";

        return fields;
    }
}
