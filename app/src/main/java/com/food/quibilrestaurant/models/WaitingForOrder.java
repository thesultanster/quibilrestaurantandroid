package com.food.quibilrestaurant.models;

import java.util.ArrayList;

/**
 * Created by sultankhan on 8/22/18.
 */

public class WaitingForOrder {

    public String id;
    public String clientId;
    public String restaurantId;
    public String tableId;
    public String userFullName;
    public String userProfilePicURL;
    public ArrayList<MenuItem> orders;
    public long updatedAt;
    public long createdAt;
    public long totalPaymentAmount;

    public WaitingForOrder(){}

    public long getCreatedAt() {
        return createdAt;
    }

    public String getId() {
        return id;
    }

    public String getClientId() {
        return clientId;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public String getTableId() {
        return tableId;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public String getUserProfilePicURL() {
        return userProfilePicURL;
    }

    public long getUpdatedAt() {
        return updatedAt;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public void setUserProfilePicURL(String userProfilePicURL) {
        this.userProfilePicURL = userProfilePicURL;
    }

    public void setUpdatedAt(long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }
}
