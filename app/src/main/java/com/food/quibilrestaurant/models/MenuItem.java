package com.food.quibilrestaurant.models;

import com.food.quibilrestaurant.interfaces.RoundItemRowType;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by sultankhan on 7/24/18.
 */

public class MenuItem implements Serializable, RoundItemRowType {

    public String id;
    public String name;
    public String description;
    public String restaurantId;
    public String categoryId;
    public String itemPhotoUrl;
    public String state;
    public int price;
    public int index;
    public double taxRate;
    public ArrayList<AddOnItem> addOnItems = new ArrayList<>();
}
