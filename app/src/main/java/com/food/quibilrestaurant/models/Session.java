package com.food.quibilrestaurant.models;

import java.io.Serializable;
import java.util.ArrayList;

public class Session implements Serializable {

    public String id;
    public String clientId;
    public String restaurantId;
    public String restaurantName;
    public String restaurantAddress;
    public String restaurantProfilePicURL;
    public String tableId;
    public String userFullName;
    public String userProfilePicURL;
    public String state;
    public ArrayList<OrderedItem> orders;
    public long updatedAt;
    public long createdAt;
    public long totalPaymentAmount;
}
