package com.food.quibilrestaurant.models;


import com.food.quibilrestaurant.interfaces.RoundItemRowType;

import java.io.Serializable;

/**
 * Created by sultankhan on 8/10/18.
 */

public class AddOnItem implements Serializable, RoundItemRowType {
    public String id;
    public long price;
    public String name;
    public String menuItemId;
}
