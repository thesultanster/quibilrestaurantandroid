package com.food.quibilrestaurant.models;

import android.graphics.Bitmap;
import android.util.Log;

import java.io.Serializable;

/**
 * Created by sultankhan on 7/22/17.
 */

public class User implements Serializable {

    public String id = "";
    public String name = "";
    public String email = "";
    public String profilePictureURL = "";
    public String profileType = "MANAGER";
    public String profileTypeTitle = "Restaurant Owner";
    public String ownsRestaurant = "";

    public User() {

    }

    @Override
    public String toString() {
        String fields = this.getClass().getSimpleName() + ":\n{ \n"
                + "\tid = " + id + ",\n "
                + "\tname = " + name + ",\n "
                + "\temail = " + email + ",\n "
                + "\tprofilePictureURL = " + profilePictureURL + ",\n "
                + "\tprofileType = " + profileType + ",\n "
                + "\tprofileTypeTitle = " + profileTypeTitle + ",\n "
                + "\townsRestaurant = " + ownsRestaurant + ",\n "
                + "}";

        return fields;
    }
}
