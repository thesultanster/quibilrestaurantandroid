package com.food.quibilrestaurant.models;

import com.food.quibilrestaurant.interfaces.RoundItemRowType;

import java.io.Serializable;
import java.util.ArrayList;

// Inside session. This is an individual item under /orders/
public class OrderedItem implements Serializable, RoundItemRowType {

    public String id;
    public String name;
    public String description;
    public String restaurantId;
    public String categoryId;
    public String itemPhotoUrl;
    public String state;
    public int index;
    public int price;
    public double taxRate;
    public long orderedAt;
    public long createdAt;
    public boolean isPrinted;
    public ArrayList<AddOnItem> addOnItems = new ArrayList<>();

    public OrderedItem(){

    }

}
