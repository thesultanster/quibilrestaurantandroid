package com.food.quibilrestaurant.models


import java.io.Serializable

class QRCode : Serializable {
    var id: String = ""
    var tableId = ""
    var restaurantId = ""
}
