package com.food.quibilrestaurant.models;

import java.io.Serializable;

/**
 * Created by sultankhan on 7/24/18.
 */

public class MenuCategory implements Serializable{
    public String id;
    public String name;
    public String restaurantId;
    public int index;
}
