package com.food.quibilrestaurant.models;

import android.graphics.Bitmap;

import java.io.Serializable;

/**
 * Created by sultankhan on 7/22/17.
 */

public class StaffWorker implements Serializable {

    public String id = "";
    public String name = "";
    public String pin="";
    public String profilePictureURL = "";
    public String profileType = "STAFF";
    public String profileTypeTitle = "Staff Worker";
    public String worksAtRestaurantId = "";
    public Bitmap profilePictureBitmap = null;

    public StaffWorker() {

    }

}
